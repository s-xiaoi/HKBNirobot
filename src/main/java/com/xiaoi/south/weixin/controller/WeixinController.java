/*
 * Copyright [2018] [5a79074276d60953a4dc2ab95d762b2f]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");you may not
 * use this file except in compliance with the License.You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.See the
 * License for the specific language governing permissions andlimitations under
 * the License.
 */
package com.xiaoi.south.weixin.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.xiaoi.south.app.servlet.controller.BaseController;
import com.xiaoi.south.weixin.models.ReturnModel;
import com.xiaoi.south.weixin.service.CoreService;

/**
 * <pre>
 * 微信入口
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
@Controller
public class WeixinController extends BaseController{

    @Autowired
    protected WxMpConfigStorage configStorage;

    @Autowired
    protected WxMpService wxMpService;

    @Autowired
    protected CoreService coreService;

    /**
     * 微信公众号webservice主服务接口，提供与微信服务器的信息交互
     * 
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping( value = "wxCore" )
    public void wxCore( HttpServletRequest request, HttpServletResponse response ) throws Exception{

        // context容器
        final Map<String, Object> context = new HashMap<String, Object>();

        String signature = request.getParameter( "signature" );
        String nonce = request.getParameter( "nonce" );
        String timestamp = request.getParameter( "timestamp" );
        String echoStr = request.getParameter( "echostr" );
        // 开发模式
        String debug = request.getParameter( "debug" );

        this.logger.info( "signature={},nonce={},timestamp={},echostr={},debug={}", signature, nonce, timestamp, echoStr, debug );

        if( !this.wxMpService.checkSignature( timestamp, nonce, signature ) && !StringUtils.equals( debug, "true" ) ){
            // 消息签名不正确，说明不是公众平台发过来的消息
            this.writeString( response, "reject the request!" );
            return;
        }

        this.logger.info( "signature={},nonce={},timestamp={},echostr={},debug={}", signature, nonce, timestamp, echoStr, debug );

        if( StringUtils.isNotBlank( echoStr ) ){
            // 说明是一个仅仅用来验证的请求，回显echostr
            this.writeString( response, echoStr );
            return;
        }

        // 来源渠道,默认微信
        String platform = request.getParameter( "p" );
        if( StringUtils.isBlank( platform ) ){
            context.put( "platform", platform );
        }else{
            context.put( "platform", "weixin" );
        }

        String encryptType = StringUtils.isBlank( request.getParameter( "encrypt_type" ) ) ? "raw" : request.getParameter( "encrypt_type" );
        this.logger.info( "encryptType={}", encryptType );

        // 响应公众平台
        WxMpXmlOutMessage outMsg = null;

        if( "raw".equals( encryptType ) ){
            // 明文传输的消息
            WxMpXmlMessage inMsg = WxMpXmlMessage.fromXml( request.getInputStream() );
            outMsg = this.coreService.route( inMsg, context );
            if( outMsg != null ){
                this.writeXml( response, outMsg.toXml() );
            }else{
                this.writeString( response, "success" );
            }
            return;
        }else if( "aes".equals( encryptType ) ){
            // 是aes加密的消息
            String msgSignature = request.getParameter( "msg_signature" );
            WxMpXmlMessage inMsg = WxMpXmlMessage.fromEncryptedXml( request.getInputStream(), this.configStorage, timestamp, nonce, msgSignature );
            this.logger.debug( "\n消息解密后内容为：\n{} ", inMsg.toString() );
            outMsg = this.coreService.route( inMsg, context );
            if( outMsg != null ){
                this.writeXml( response, outMsg.toEncryptedXml( this.configStorage ) );
            }else{
                this.writeString( response, "success" );
            }
            return;
        }

        this.writeString( response, "Unrecognized request" );
        return;
    }

    /**
     * 通过openid获得基本用户信息
     * 详情请见:
     * http://mp.weixin.qq.com/wiki/14/bb5031008f1494a59c6f71fa0f319c66.html
     * 
     * @param response
     * @param openid openid
     * @param lang zh_CN, zh_TW, en
     */
    @RequestMapping( value = "getUserInfo" )
    public WxMpUser getUserInfo( HttpServletResponse response, @RequestParam( value = "openid" ) String openid, @RequestParam( value = "lang" ) String lang ){
        ReturnModel returnModel = new ReturnModel();
        WxMpUser wxMpUser = null;
        try{
            wxMpUser = this.wxMpService.getUserService().userInfo( openid, lang );
            returnModel.setResult( true );
            returnModel.setDatum( wxMpUser );
            this.writeJson( response, returnModel );
        }catch( WxErrorException e ){
            returnModel.setResult( false );
            returnModel.setReason( e.getError().toString() );
            this.writeJson( response, returnModel );
            this.logger.error( e.getError().toString() );
        }
        return wxMpUser;
    }

    /**
     * 通过code获得基本用户信息
     * 详情请见:
     * http://mp.weixin.qq.com/wiki/14/bb5031008f1494a59c6f71fa0f319c66.html
     * 
     * @param response
     * @param code code
     * @param lang zh_CN, zh_TW, en
     */
    @RequestMapping( value = "getOAuth2UserInfo" )
    public void getOAuth2UserInfo( HttpServletResponse response, @RequestParam( value = "code" ) String code, @RequestParam( value = "lang" ) String lang ){
        ReturnModel returnModel = new ReturnModel();
        WxMpOAuth2AccessToken accessToken;
        WxMpUser wxMpUser;
        try{
            accessToken = this.wxMpService.oauth2getAccessToken( code );
            wxMpUser = this.wxMpService.getUserService().userInfo( accessToken.getOpenId(), lang );
            returnModel.setResult( true );
            returnModel.setDatum( wxMpUser );
            this.writeJson( response, returnModel );
        }catch( WxErrorException e ){
            returnModel.setResult( false );
            returnModel.setReason( e.getError().toString() );
            this.writeJson( response, returnModel );
            this.logger.error( e.getError().toString() );
        }
    }

    /**
     * 用code换取oauth2的openid
     * 详情请见:
     * http://mp.weixin.qq.com/wiki/1/8a5ce6257f1d3b2afb20f83e72b72ce9.html
     * 
     * @param response
     * @param code code
     */
    @RequestMapping( value = "getOpenid" )
    public void getOpenid( HttpServletResponse response, @RequestParam( value = "code" ) String code ){
        ReturnModel returnModel = new ReturnModel();
        WxMpOAuth2AccessToken accessToken;
        try{
            accessToken = this.wxMpService.oauth2getAccessToken( code );
            returnModel.setResult( true );
            returnModel.setDatum( accessToken.getOpenId() );
            this.writeJson( response, returnModel );
        }catch( WxErrorException e ){
            returnModel.setResult( false );
            returnModel.setReason( e.getError().toString() );
            this.writeJson( response, returnModel );
            this.logger.error( e.getError().toString() );
        }
    }

}
