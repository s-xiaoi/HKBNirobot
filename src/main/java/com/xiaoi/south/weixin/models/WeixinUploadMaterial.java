/*
 * Copyright [2018] [5a79074276d60953a4dc2ab95d762b2f]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xiaoi.south.weixin.models;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import jodd.cache.LRUCache;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xiaoi.south.app.modules.utils.StringTookits;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.result.WxMediaUploadResult;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.kefu.WxMpKefuMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * <pre>
 * 上传素材获取mediaId
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class WeixinUploadMaterial{

    protected Logger logger = LoggerFactory.getLogger( WeixinUploadMaterial.class );

    private WxMpService wxMpService;

    private WxMpXmlMessage wxMpMessage;

    // 2.5天过期
    private static LRUCache<String, String> cache = new LRUCache<String, String>( 10000, 216000000 );

    /**
     * 创建一个新的实例 WeixinUploadMaterial.
     * 
     */
    public WeixinUploadMaterial( WxMpService wxMpService, WxMpXmlMessage wxMpMessage ){
        this.wxMpService = wxMpService;
        this.wxMpMessage = wxMpMessage;
    }

    /**
     * <pre>
     * <b>图片，视频回复响应</b>
     * </pre>
     * 
     * @param url
     * @param mediaType void
     * @since 1.0
     */
    public void response( String url, String mediaType ){
        String mediaId = this.upload( url, mediaType );
        WxMpKefuMessage message = null;
        if( StringUtils.equals( WxConsts.MediaFileType.IMAGE, mediaType ) ){
            message = WxMpKefuMessage.IMAGE().toUser( wxMpMessage.getFromUser() ).mediaId( mediaId ).build();

        }else if( StringUtils.equals( WxConsts.MediaFileType.VIDEO, mediaType ) ){
            message = WxMpKefuMessage.VIDEO().toUser( wxMpMessage.getFromUser() ).mediaId( mediaId ).build();
        }
        try{
            this.wxMpService.getKefuService().sendKefuMessage( message );
        }catch( WxErrorException e ){
            logger.error( "", e );
        }
    }

    /**
     * <pre>
     * <b>上传产品的图片以及视频的素材,返回mediaId</b>
     * </pre>
     * 
     * @param url 产品资源的url
     * @param mediaType 公众平台mediaType
     * @return String
     * @since 1.0
     */
    public String upload( String url, String mediaType ){
        //
        String mediaId = null;
        String key = StringTookits.md5( url );
        String mid = cache.get( key );
        if( StringUtils.isNotBlank( mid ) ){// 已经有缓存，直接返回
            return mid;
        }
        String prex = ".file";
        if( StringUtils.equals( WxConsts.MediaFileType.IMAGE, mediaType ) ){
            prex = ".png";
        }else if( StringUtils.equals( WxConsts.MediaFileType.VIDEO, mediaType ) ){
            prex = ".mp4";
        }
        String f = this.downLoadFromUrl( url, key + prex, null );
        logger.info( "local file path:{}", f );
        File file = null;
        if( f != null ){// 下载成功
            file = new File( f );
            WxMediaUploadResult res = null;
            try{
                res = wxMpService.getMaterialService().mediaUpload( mediaType, file );
                mediaId = res.getMediaId();
            }catch( WxErrorException e ){
                logger.error( "", e );
            }finally{
                if( StringUtils.isNotBlank( f ) ){// 清理临时文件
                    if( file.exists() && file.isFile() ){
                        file.delete();
                    }
                }
            }
            if( StringUtils.isNotBlank( mediaId ) ){// 加入缓存
                cache.put( key, mediaId );
            }
        }
        return mediaId;
    }

    /**
     * 从网络Url中下载文件
     * 
     * @param urlStr
     * @param fileName
     * @param savePath
     * @throws IOException
     */
    public String downLoadFromUrl( String urlStr, String fileName, String savePath ){
        URL url = null;
        HttpURLConnection conn = null;
        try{
            url = new URL( urlStr );
            conn = (HttpURLConnection) url.openConnection();
            // 设置超时间为3秒
            conn.setConnectTimeout( 3 * 1000 );
            // 防止屏蔽程序抓取而返回403错误
            conn.setRequestProperty( "User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)" );

            // 得到输入流
            InputStream inputStream = conn.getInputStream();
            // 获取自己数组
            byte[] getData = readInputStream( inputStream );

            // 文件保存位置
            if( StringUtils.isBlank( savePath ) ){
                savePath = System.getProperty( "java.io.tmpdir" );
                // savePath = "I:/logs";
            }

            File saveDir = new File( savePath );
            if( !saveDir.exists() ){
                saveDir.mkdir();
            }

            if( StringUtils.isBlank( fileName ) ){
                fileName = StringTookits.md5( urlStr );
            }

            File file = new File( saveDir + File.separator + fileName );
            FileOutputStream fos = new FileOutputStream( file );
            fos.write( getData );
            if( fos != null ){
                fos.close();
            }
            if( inputStream != null ){
                inputStream.close();
            }
            logger.info( "info:" + url + " download success" );
            return file.getAbsolutePath();
        }catch( MalformedURLException e ){
            e.printStackTrace();
        }catch( IOException e ){
            e.printStackTrace();
        }finally{
            if( conn != null ){
                conn.disconnect();
                conn = null;
            }
        }
        return null;
    }

    /**
     * 从输入流中获取字节数组
     * 
     * @param inputStream
     * @return
     * @throws IOException
     */
    public byte[] readInputStream( InputStream inputStream ) throws IOException{
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while( (len = inputStream.read( buffer )) != -1 ){
            bos.write( buffer, 0, len );
        }
        bos.close();
        return bos.toByteArray();
    }

}
