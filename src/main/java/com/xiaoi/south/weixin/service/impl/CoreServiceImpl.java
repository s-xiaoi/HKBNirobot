/*
 * Copyright [2018] [5a79074276d60953a4dc2ab95d762b2f]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");you may not
 * use this file except in compliance with the License.You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.See the
 * License for the specific language governing permissions andlimitations under
 * the License.
 */
package com.xiaoi.south.weixin.service.impl;

import java.util.Map;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiaoi.south.weixin.handler.IBot4MpHandler;
import com.xiaoi.south.weixin.handler.LogHandler;
import com.xiaoi.south.weixin.handler.MsgHandler;
import com.xiaoi.south.weixin.handler.SubscribeHandler;
import com.xiaoi.south.weixin.service.CoreService;

import javax.annotation.PostConstruct;

/**
 * <pre>
 * TODO
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
@Service
public class CoreServiceImpl implements CoreService{

    @Autowired
    protected WxMpService wxMpService;

    @Autowired
    protected LogHandler logHandler;

    @Autowired
    protected SubscribeHandler subscribeHandler;

    @Autowired
    protected IBot4MpHandler iBot4MpHandler;

    protected Logger logger = LoggerFactory.getLogger( getClass() );

    private WxMpMessageRouter router;

    @PostConstruct
    public void init(){
        this.refreshRouter();
    }

    @Override
    public void refreshRouter(){
        final WxMpMessageRouter newRouter = new WxMpMessageRouter( this.wxMpService );
        // 记录所有事件的日志
        newRouter.rule().handler( this.logHandler ).next();
        // 关注事件
        newRouter.rule().async( false ).msgType( WxConsts.XmlMsgType.EVENT ).event( WxConsts.EventType.SUBSCRIBE ).handler( this.subscribeHandler ).end();
        // 默认,智能问答
        newRouter.rule().async( false ).handler( this.iBot4MpHandler ).end();
        this.router = newRouter;
    }

    @Override
    public WxMpXmlOutMessage route( WxMpXmlMessage inMessage, final Map<String, Object> context ){
        try{
            return this.router.route( inMessage, context );
        }catch( Exception e ){
            this.logger.error( e.getMessage(), e );
        }

        return null;
    }

    @Override
    public WxMpUser getUserInfo( String openid, String lang ){
        WxMpUser wxMpUser = null;
        try{
            wxMpUser = this.wxMpService.getUserService().userInfo( openid, lang );
        }catch( WxErrorException e ){
            this.logger.error( e.getError().toString() );
        }
        return wxMpUser;
    }

}
