/*
 * Copyright [2018] [5a79074276d60953a4dc2ab95d762b2f]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");you may not
 * use this file except in compliance with the License.You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.See the
 * License for the specific language governing permissions andlimitations under
 * the License.
 */
package com.xiaoi.south.weixin.service;

import java.util.Map;

import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

/**
 * <pre>
 * TODO
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public interface CoreService{

    /**
     * 刷新消息路由器
     */
    void refreshRouter();

    /**
     * 路由消息
     * 
     * @param inMessage
     * @return
     */
    WxMpXmlOutMessage route( WxMpXmlMessage inMessage, final Map<String, Object> context );

    /**
     * 通过openid获得基本用户信息
     * 
     * @param openid
     * @param lang
     * @return
     */
    WxMpUser getUserInfo( String openid, String lang );
}
