package com.xiaoi.south.weixin.handler;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.xiaoi.south.app.entity.RobotRequest;
import com.xiaoi.south.app.entity.RobotResponse;
import com.xiaoi.south.app.service.impl.AskServiceProxy;
import com.xiaoi.south.weixin.models.WeixinUploadMaterial;
import com.xiaoi.south.weixin.task.WxSecondQuestionTask;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutNewsMessage;

/**
 * <pre>
 * 自接问答服务
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
@Component
public class IBot4MpHandler extends AbstractHandler{

    /**
     * DEFAULT_THREAD_POOL_SIZE:执行processer的内部线程池数:100线程
     */
    private static final int DEFAULT_THREAD_POOL_SIZE = 300;

    /**
     * listeningExecutorService:执行线程池
     */
    private ListeningExecutorService listeningExecutorService = MoreExecutors.listeningDecorator( Executors.newFixedThreadPool( DEFAULT_THREAD_POOL_SIZE ) );

    @Autowired
    private AskServiceProxy askServiceProxy;

    @Override
    public WxMpXmlOutMessage handle( WxMpXmlMessage wxMpMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager ) throws WxErrorException{
        // 默认空白响应
        WxMpXmlOutMessage wxMpXmlOutMsg = null;

        String userId = wxMpMessage.getFromUser();
        String platform = (String) context.get( "platform" );
        String question = null;

        final RobotRequest robotRequest = new RobotRequest();
        robotRequest.setUserId( userId );
        robotRequest.setPlatform( platform );
        // 只处理文本
        if( StringUtils.equalsIgnoreCase( wxMpMessage.getMsgType(), WxConsts.XmlMsgType.TEXT ) ){
            question = wxMpMessage.getContent();
        }else if( StringUtils.equalsIgnoreCase( wxMpMessage.getMsgType(), WxConsts.XmlMsgType.VOICE ) ){
            question = wxMpMessage.getRecognition();
        }else{// 其它类型，直接返回空报文，响应公众平台
            return wxMpXmlOutMsg;
        }

        robotRequest.setQuestion( question );
        final RobotResponse robotResponse = this.askServiceProxy.ask4Robot( robotRequest );

        // 异步处理逻辑：处理图片，视频
        final int type = robotResponse.getType();
        final WxMpService wxServcie = wxMpService;
        final WxMpXmlMessage wxMessage = wxMpMessage;
        if( type == 202 || type == 204 ){// 图片，视频
            final ListenableFuture<Integer> listenableFuture = this.listeningExecutorService.submit( new Callable<Integer>(){
                @Override
                public Integer call() throws Exception{
                    WeixinUploadMaterial weixinUploadMaterial = new WeixinUploadMaterial( wxServcie, wxMessage );
                    Map map = (Map) robotResponse.getAttachment();
                    String url = (String) map.get( "url" );

                    if( type == 202 ){
                        weixinUploadMaterial.response( url, WxConsts.MediaFileType.IMAGE );
                    }else if( type == 204 ){
                        weixinUploadMaterial.response( url, WxConsts.MediaFileType.VIDEO );
                    }
                    return 1;
                }
            } );
            //return wxMpXmlOutMsg;
        }else if( type == 201 ){// 图文消息
            List<Map<String, String>> list = (List<Map<String, String>>) robotResponse.getAttachment();
            if( list != null ){
                WxMpXmlOutNewsMessage.Item[] items = new WxMpXmlOutNewsMessage.Item[list.size()];
                for( int i = 0; i < list.size(); i++ ){
                    Map<String, String> map = list.get( i );
                    WxMpXmlOutNewsMessage.Item item = new WxMpXmlOutNewsMessage.Item();
                    item.setDescription( map.get( "description" ) );
                    item.setPicUrl( map.get( "image" ) );
                    item.setTitle( map.get( "title" ) );
                    item.setUrl( map.get( "url" ) );
                    items[i] = item;
                }
                wxMpXmlOutMsg = WxMpXmlOutMessage.NEWS().fromUser( wxMpMessage.getToUser() ).toUser( wxMpMessage.getFromUser() ).addArticle( items ).build();
            }
        }else{// 其它文本回复
            wxMpXmlOutMsg = WxMpXmlOutMessage.TEXT().fromUser( wxMpMessage.getToUser() ).toUser( wxMpMessage.getFromUser() ).content( robotResponse.getContent() ).build();
        }

        // 处理一问二答
        if( robotResponse.getCommandObject() != null ){
            String name = robotResponse.getCommandObject().get( "name" );
            if( StringUtils.equals( "secondQuestion", name ) ){
                String secondQuestion = robotResponse.getCommandObject().get( "secondQuestion" );
                final RobotRequest rrq = new RobotRequest();
                rrq.setUserId( userId );
                rrq.setPlatform( platform );
                rrq.setQuestion( secondQuestion );
                this.listeningExecutorService.submit( new Callable<Integer>(){
                    @Override
                    public Integer call() throws Exception{
                        WxSecondQuestionTask task = new WxSecondQuestionTask( askServiceProxy, wxServcie, wxMessage );
                        task.ask( rrq );
                        return 1;
                    }
                } );
            }
        }

        // 默认空白字符串（参考公众平台文档）

        return wxMpXmlOutMsg;

    }
}
