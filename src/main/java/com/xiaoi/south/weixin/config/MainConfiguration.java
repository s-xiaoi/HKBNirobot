/*
 * Copyright [2018] [5a79074276d60953a4dc2ab95d762b2f]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");you may not
 * use this file except in compliance with the License.You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.See the
 * License for the specific language governing permissions andlimitations under
 * the License.
 */
package com.xiaoi.south.weixin.config;

import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.api.impl.WxMpServiceOkHttpImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <pre>
 * TODO
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
@Configuration
public class MainConfiguration{

    @Value( "#{wxProperties.appId}" )
    private String appId;

    @Value( "#{wxProperties.appSecret}" )
    private String appSecret;

    @Value( "#{wxProperties.token}" )
    private String token;

    @Value( "#{wxProperties.aesKey}" )
    private String aesKey;

    @Bean
    public WxMpConfigStorage wxMpConfigStorage(){
        WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
        configStorage.setAppId( this.appId );
        configStorage.setSecret( this.appSecret );
        configStorage.setToken( this.token );
        configStorage.setAesKey( this.aesKey );
        return configStorage;
    }

    @Bean
    public WxMpService wxMpService(){
        WxMpService wxMpService = new WxMpServiceOkHttpImpl();
        wxMpService.setWxMpConfigStorage( wxMpConfigStorage() );
        return wxMpService;
    }

}
