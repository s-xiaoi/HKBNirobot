/*
 * Copyright [2018] [5a79074276d60953a4dc2ab95d762b2f]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xiaoi.south.weixin.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.kefu.WxMpKefuMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutNewsMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.ListenableFuture;
import com.xiaoi.south.app.entity.RobotRequest;
import com.xiaoi.south.app.entity.RobotResponse;
import com.xiaoi.south.app.service.impl.AskServiceProxy;
import com.xiaoi.south.weixin.models.WeixinUploadMaterial;

/**
 * <pre>
 * 微信一问二答处理
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class WxSecondQuestionTask{

    private static final Logger logger = LoggerFactory.getLogger( WxSecondQuestionTask.class );

    RobotResponse robotResponse;

    private AskServiceProxy askService;

    private WxMpService wxMpService;

    private WxMpXmlMessage wxMpMessage;

    public WxSecondQuestionTask( AskServiceProxy askService, WxMpService wxMpService, WxMpXmlMessage wxMpMessage ){
        super();
        this.askService = askService;
        this.wxMpService = wxMpService;
        this.wxMpMessage = wxMpMessage;
    }

    /**
     * <pre>
     * <b>一问二答处理</b>
     * </pre>
     * @param request void 
     * @since 1.0    
    */
    public void ask( RobotRequest request ){
        // 一问二答
        robotResponse = this.askService.ask4Robot( request );
        WxMpKefuMessage message = null;
        int type = robotResponse.getType();
        if( type == 202 || type == 204 ){// 图片，视频
            WeixinUploadMaterial weixinUploadMaterial = new WeixinUploadMaterial( this.wxMpService, this.wxMpMessage );
            Map map = (Map) robotResponse.getAttachment();
            String url = (String) map.get( "url" );
            if( type == 202 ){
                weixinUploadMaterial.response( url, WxConsts.MediaFileType.IMAGE );
            }else if( type == 204 ){
                weixinUploadMaterial.response( url, WxConsts.MediaFileType.VIDEO );
            }
        }else if( type == 201 ){// 图文消息
            List<Map<String, String>> list = (List<Map<String, String>>) robotResponse.getAttachment();
            if( list != null ){
                List<WxMpKefuMessage.WxArticle> articles = new ArrayList<WxMpKefuMessage.WxArticle>();
                for( int i = 0; i < list.size(); i++ ){
                    Map<String, String> map = list.get( i );
                    WxMpKefuMessage.WxArticle article = new WxMpKefuMessage.WxArticle();
                    article.setUrl( map.get( "url" ) );
                    article.setPicUrl( map.get( "image" ) );
                    article.setDescription( map.get( "description" ) );
                    article.setTitle( map.get( "title" ) );
                    articles.add( article );
                }
                message = WxMpKefuMessage.NEWS().toUser( this.wxMpMessage.getFromUser() ).articles( articles ).build();
            }
        }else{// 普通消息
            message = WxMpKefuMessage.TEXT().toUser( this.wxMpMessage.getFromUser() ).content( robotResponse.getContent() ).build();
        }
        //
        if( message != null ){
            try{
                this.wxMpService.getKefuService().sendKefuMessage( message );
            }catch( WxErrorException e ){
                logger.error( "", e );
            }
        }

    }

}
