package com.xiaoi.south.app.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService( targetNamespace = "http://www.eastrobot.cn/ws/QuestionService", name = "QuestionService" )
public interface QuestionService{

    @WebResult( name = "hotQuestions" )
    @WebMethod( action = "http://www.eastrobot.cn/ws/QuestionService/getHotQuestions" )
    public java.util.List<java.lang.String> getHotQuestions( @WebParam( name = "mode" )
    int mode, @WebParam( name = "platform" )
    java.lang.String platform, @WebParam( name = "location" )
    java.lang.String location, @WebParam( name = "top" )
    int top );

    @WebResult( name = "relatedQuestions" )
    @WebMethod( action = "http://www.eastrobot.cn/ws/QuestionService/getRelatedQuestions0" )
    public java.util.List<java.lang.String> getRelatedQuestions0( @WebParam( name = "question" )
    java.lang.String question, @WebParam( name = "platform" )
    java.lang.String platform, @WebParam( name = "location" )
    java.lang.String location, @WebParam( name = "brand" )
    java.lang.String brand, @WebParam( name = "top" )
    int top );

    @WebResult( name = "hotQuestions" )
    @WebMethod( action = "http://www.eastrobot.cn/ws/QuestionService/getHotQuestions0" )
    public java.util.List<java.lang.String> getHotQuestions0( @WebParam( name = "mode" )
    int mode, @WebParam( name = "platform" )
    java.lang.String platform, @WebParam( name = "location" )
    java.lang.String location, @WebParam( name = "brand" )
    java.lang.String brand, @WebParam( name = "top" )
    int top );

    @WebResult( name = "relatedQuestions" )
    @WebMethod( action = "http://www.eastrobot.cn/ws/QuestionService/getRelatedQuestions" )
    public java.util.List<java.lang.String> getRelatedQuestions( @WebParam( name = "question" )
    java.lang.String question, @WebParam( name = "platform" )
    java.lang.String platform, @WebParam( name = "location" )
    java.lang.String location, @WebParam( name = "top" )
    int top );

    @WebResult( name = "suggestedQuestions" )
    @WebMethod( action = "http://www.eastrobot.cn/ws/QuestionService/getSuggestedQuestions0" )
    public java.util.List<java.lang.String> getSuggestedQuestions0( @WebParam( name = "input" )
    java.lang.String input, @WebParam( name = "platform" )
    java.lang.String platform, @WebParam( name = "location" )
    java.lang.String location, @WebParam( name = "brand" )
    java.lang.String brand, @WebParam( name = "top" )
    int top );

    @WebResult( name = "suggestedQuestions" )
    @WebMethod( action = "http://www.eastrobot.cn/ws/QuestionService/getSuggestedQuestions" )
    public java.util.List<java.lang.String> getSuggestedQuestions( @WebParam( name = "input" )
    java.lang.String input, @WebParam( name = "platform" )
    java.lang.String platform, @WebParam( name = "location" )
    java.lang.String location, @WebParam( name = "top" )
    int top );
}
