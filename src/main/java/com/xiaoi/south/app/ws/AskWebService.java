package com.xiaoi.south.app.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.eastrobot.robotface.domain.RobotRequestEx;
import com.eastrobot.robotface.domain.RobotResponseEx;

@WebService
@SOAPBinding( parameterStyle = SOAPBinding.ParameterStyle.WRAPPED, use = SOAPBinding.Use.LITERAL, style = SOAPBinding.Style.DOCUMENT )
public interface AskWebService{
    @WebMethod
    public RobotResponseEx ask( RobotRequestEx request );

    @WebMethod
    public RobotResponseEx deliver( RobotRequestEx request );
}
