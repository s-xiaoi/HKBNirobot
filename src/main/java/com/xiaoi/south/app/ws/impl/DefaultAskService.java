package com.xiaoi.south.app.ws.impl;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.eastrobot.robotface.domain.RobotRequestEx;
import com.eastrobot.robotface.domain.RobotResponseEx;
import com.xiaoi.south.app.service.impl.DefaultRobotServiceProxy;
import com.xiaoi.south.app.ws.AskWebService;

@WebService( serviceName = "AskService", targetNamespace = "http://www.eastrobot.cn/ws/AskService", endpointInterface = "com.xiaoi.south.app.ws.AskWebService" )
public class DefaultAskService implements AskWebService{

    DefaultRobotServiceProxy defaultRobotServiceProxy = new DefaultRobotServiceProxy();

    public DefaultRobotServiceProxy getProxy(){
        this.defaultRobotServiceProxy.init();
        return defaultRobotServiceProxy;
    }

    @Override
    @WebMethod
    public RobotResponseEx ask( RobotRequestEx request ){
        return this.getProxy().deliver( request );
    }

    @Override
    @WebMethod
    public RobotResponseEx deliver( RobotRequestEx request ){
        return this.getProxy().deliver( request );
    }

}
