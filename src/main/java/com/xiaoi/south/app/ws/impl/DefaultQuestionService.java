/**
 * Please modify this class to meet your needs This class is not complete
 */

package com.xiaoi.south.app.ws.impl;

import java.util.List;

import com.xiaoi.south.app.service.impl.DefaultQuestionServiceProxy;
import com.xiaoi.south.app.ws.QuestionService;

@javax.jws.WebService( serviceName = "QuestionService", portName = "DefaultQuestionServicePort", targetNamespace = "http://www.eastrobot.cn/ws/QuestionService", endpointInterface = "com.xiaoi.south.app.ws.QuestionService" )
public class DefaultQuestionService implements QuestionService{

    private DefaultQuestionServiceProxy defaultQuestionServiceProxy = new DefaultQuestionServiceProxy();

    public DefaultQuestionServiceProxy getProxy(){
        defaultQuestionServiceProxy.init();
        return defaultQuestionServiceProxy;
    }

    public List<String> getHotQuestions( int mode, String platform, String location, int top ){
        return this.getProxy().getHotQuestions( mode, platform, location, top );
    }

    public List<String> getHotQuestions0( int mode, String platform, String location, String brand, int top ){
        return this.getProxy().getHotQuestions0( mode, platform, location, brand, top );
    }

    public List<String> getRelatedQuestions( String question, String platform, String location, int top ){
        return this.getProxy().getRelatedQuestions( question, platform, location, top );
    }

    public List<String> getRelatedQuestions0( String question, String platform, String location, String brand, int top ){
        return this.getProxy().getRelatedQuestions0( question, platform, location, brand, top );
    }

    public List<String> getSuggestedQuestions( String input, String platform, String location, int top ){
        return this.getProxy().getSuggestedQuestions( input, platform, location, top );
    }

    public List<String> getSuggestedQuestions0( String input, String platform, String location, String brand, int top ){
        return this.getProxy().getSuggestedQuestions0( input, platform, location, brand, top );
    }

}
