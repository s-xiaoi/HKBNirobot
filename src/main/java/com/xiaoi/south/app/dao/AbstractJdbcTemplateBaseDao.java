/**
 * 文件名：AbstractBaseDao.java
 * 
 * 版本信息： 日期：2015年12月23日 create by ziQi 2015年12月23日
 */
package com.xiaoi.south.app.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;

/**
 * <pre>
 * jdbcTemplate实现的的基础的jdbc操作
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class AbstractJdbcTemplateBaseDao<T> implements IBaseDao<T>{
    @Autowired
    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate(){
        return namedParameterJdbcTemplate;
    }

    public void setNamedParameterJdbcTemplate( NamedParameterJdbcTemplate namedParameterJdbcTemplate ){
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public int update( String sql, T entity ){
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource( entity );
        return this.namedParameterJdbcTemplate.update( sql, namedParameters );
    }

    @Override
    public int update( String sql, Map<String, Object> paramMap ){
        return this.namedParameterJdbcTemplate.update( sql, paramMap );
    }

    @Override
    public int[] batchUpdate( String sql, List<T> list ){
        SqlParameterSource[] batchArgs = SqlParameterSourceUtils.createBatch( list.toArray() );
        return this.namedParameterJdbcTemplate.batchUpdate( sql, batchArgs );
    }

    @Override
    public int save( String sql, T entity ){
        return this.update( sql, entity );
    }

    @Override
    public int save( String sql, Map<String, Object> paramMap ){
        return this.update( sql, paramMap );
    }

    @Override
    public int[] batchSave( String sql, List<T> list ){
        return this.batchUpdate( sql, list );
    }

    @Override
    public int delete( String sql, T entity ){
        return this.update( sql, entity );
    }

    @Override
    public int delete( String sql, Map<String, Object> paramMap ){
        return this.update( sql, paramMap );
    }

    @Override
    public int[] batchDelete( String sql, List<T> list ){
        return this.batchUpdate( sql, list );
    }

    @Override
    @SuppressWarnings( "unchecked" )
    public List<T> find( String sql, T entity ){
        RowMapper<T> rowMapper = (RowMapper<T>) BeanPropertyRowMapper.newInstance( entity.getClass() );
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource( entity );
        return this.namedParameterJdbcTemplate.query( sql, namedParameters, rowMapper );
    }

    @Override
    public List<T> find( String sql, T entity, Class<T> clazz ){
        RowMapper<T> rowMapper = (RowMapper<T>) BeanPropertyRowMapper.newInstance( clazz );
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource( entity );
        return this.namedParameterJdbcTemplate.query( sql, namedParameters, rowMapper );
    }

    @Override
    public List<T> find( String sql, Map<String, Object> paramMap, Class<T> clazz ){
        RowMapper<T> rowMapper = (RowMapper<T>) BeanPropertyRowMapper.newInstance( clazz );
        return this.namedParameterJdbcTemplate.query( sql, paramMap, rowMapper );
    }

    @Override
    public T get( String sql, T entity ){
        @SuppressWarnings( "unchecked" )
        RowMapper<T> rowMapper = (RowMapper<T>) BeanPropertyRowMapper.newInstance( entity.getClass() );
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource( entity );
        this.namedParameterJdbcTemplate.queryForObject( sql, namedParameters, rowMapper );
        return null;
    }

    @Override
    public T get( String sql, T entity, Class<T> clazz ){
        RowMapper<T> rowMapper = (RowMapper<T>) BeanPropertyRowMapper.newInstance( clazz );
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource( entity );
        this.namedParameterJdbcTemplate.queryForObject( sql, namedParameters, rowMapper );
        return null;
    }

    @Override
    public T get( String sql, Map<String, Object> paramMap, Class<T> clazz ){
        return this.namedParameterJdbcTemplate.queryForObject( sql, paramMap, clazz );
    }

    @Override
    public T count( String sql, Map<String, Object> paramMap, Class<T> clazz ){
        return this.namedParameterJdbcTemplate.queryForObject( sql, paramMap, clazz );
    }

    @Override
    public Long count( String sql, Map<String, Object> paramMap ){
        return this.namedParameterJdbcTemplate.queryForObject( sql, paramMap, Long.class );
    }

    @Override
    public int executeSql( String sql, T entity ){
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource( entity );
        PreparedStatementCallback<Integer> preparedStatementCallback = new PreparedStatementCallback<Integer>(){
            @Override
            public Integer doInPreparedStatement( PreparedStatement ps ) throws SQLException, DataAccessException{
                ps.execute();
                ResultSet rs = ps.getResultSet();
                rs.next();
                return rs.getInt( 1 );
            }
        };
        return this.namedParameterJdbcTemplate.execute( sql, namedParameters, preparedStatementCallback );
    }

    @Override
    public int executeSql( String sql, Map<String, Object> paramMap ){
        PreparedStatementCallback<Integer> preparedStatementCallback = new PreparedStatementCallback<Integer>(){
            @Override
            public Integer doInPreparedStatement( PreparedStatement ps ) throws SQLException, DataAccessException{
                ps.execute();
                ResultSet rs = ps.getResultSet();
                rs.next();
                return rs.getInt( 1 );
            }
        };
        return this.namedParameterJdbcTemplate.execute( sql, paramMap, preparedStatementCallback );
    }

    @Override
    public int executeSql( String sql ){
        HashMap<String, Object> paramMap = new HashMap<String, Object>();
        return this.executeSql( sql, paramMap );
    }

}
