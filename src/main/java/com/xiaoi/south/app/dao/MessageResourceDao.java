package com.xiaoi.south.app.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.xiaoi.south.app.entity.MessageResourceBean;

@Component
public class MessageResourceDao{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    // src map
    private Map<String, MessageResourceBean> srcMap = new ConcurrentHashMap<String, MessageResourceBean>();

    // platformMap : code =platform
    private Map<String, MessageResourceBean> platformMap = new ConcurrentHashMap<String, MessageResourceBean>();

    // brand的map: code = brand
    private Map<String, MessageResourceBean> brandMap = new ConcurrentHashMap<String, MessageResourceBean>();

    // location的map: code=location
    private Map<String, MessageResourceBean> locationMap = new ConcurrentHashMap<String, MessageResourceBean>();

    // null的map : code 为空
    private Map<String, MessageResourceBean> nullMap = new ConcurrentHashMap<String, MessageResourceBean>();

    // key对应组合
    private Map<String, List<String>> keySet = new ConcurrentHashMap<String, List<String>>();
    
    //是否已经加载
    private AtomicBoolean loaded = new AtomicBoolean(false); 

    /**
     * @Description <b> # 获取RM_MESSAGE_RESOURCE记录,保存到srcMap </b>
     * @return Map<String,MessageResourceBean>
     * @since 1.0
     */
    public void getSrcMessageResources(){
        String sql = "SELECT A.ID,A.KEY,A.MESSAGE FROM RM_MESSAGE_RESOURCE A";
        List<Map<String, Object>> list = this.jdbcTemplate.queryForList( sql, new Object[] {} );

        if( list != null ){
            for( Map<String, Object> map : list ){
                String id = (String) map.get( "ID" );
                String key = (String) map.get( "KEY" );
                String message = (String) map.get( "MESSAGE" );

                MessageResourceBean m = new MessageResourceBean();
                m.setId( id );
                m.setKey( key );
                m.setMessage( message );

                // 用id+key作为键，如：c9bcf4a56c8342418ed9346828ea5361#defaultReply
                String valueId = id + "#" + key;
                srcMap.put( valueId, m );
            }
        }
    }

    /**
     * @Description <b> #获取platform,brand,location下面的参数并组各自对象 </b>
     * @param platform
     * @return String
     * @since 1.0
     */
    public synchronized void list4Code(){
        // src map
        Map<String, MessageResourceBean> srcMap = new ConcurrentHashMap <String, MessageResourceBean>();  
        // platformMap : code =platform
        Map<String, MessageResourceBean> platformMap = new ConcurrentHashMap <String, MessageResourceBean>();
        // brand的map: code = brand
        Map<String, MessageResourceBean> brandMap = new ConcurrentHashMap <String, MessageResourceBean>();
        // location的map: code=location
        Map<String, MessageResourceBean> locationMap = new ConcurrentHashMap <String, MessageResourceBean>();
        // null的map : code 为空
        Map<String, MessageResourceBean> nullMap = new ConcurrentHashMap <String, MessageResourceBean>();
        // key对应组合
        Map<String, List<String>> keySet = new ConcurrentHashMap <String, List<String>>();
        
        
        String sql = "SELECT A.ID,B.DIM_TAG_ID,A.KEY,A.MESSAGE,C.CODE,C.TAG AS TAG,C.NAME FROM rm_message_resource A LEFT JOIN RM_MSGRES_DIMTAG B ON A.ID=B.MSG_ID LEFT JOIN (SELECT T.ID AS ID,T.DIM_ID AS DIM_ID,T.TAG AS TAG,T.NAME,D.CODE FROM PUB_KB_DIM_TAG T LEFT JOIN PUB_KB_DIM D ON T.DIM_ID=D.ID)C ON B.DIM_TAG_ID=C.ID";
        List<Map<String, Object>> list = this.jdbcTemplate.queryForList( sql, new Object[] {} );
        
        if( list != null ){
            for( Map<String, Object> map : list ){
                String id = (String) map.get( "ID" );
                String dimTagId = (String) map.get( "DIM_TAG_ID" );
                String key = (String) map.get( "KEY" );
                String code = (String) map.get( "CODE" );
                String tag = (String) map.get( "TAG" );
                String message = (String) map.get( "MESSAGE" );

                MessageResourceBean m = new MessageResourceBean();
                m.setId( id );
                m.setDimTageId( dimTagId );
                m.setKey( key );
                m.setCode( code );
                m.setTag( tag );
                m.setMessage( message );

                // valueId： id+key+tag :
                // a5f29cfd76744daaa30707e854fa5033#defaultReply#web
                String valueId = id + "#" + key + "#" + tag;
                if( StringUtils.isBlank( code ) ){
                    nullMap.put( valueId, m );
                }else if( StringUtils.equalsIgnoreCase( code, "platform" ) ){
                    platformMap.put( valueId, m );
                }else if( StringUtils.equalsIgnoreCase( code, "brand" ) ){
                    brandMap.put( valueId, m );
                }else if( StringUtils.equalsIgnoreCase( code, "location" ) ){
                    locationMap.put( valueId, m );
                }

                // 根据key来存储
                List<String> idList = keySet.get( key );
                if( idList == null ){
                    idList = new ArrayList<String>();
                    keySet.put( key, idList );
                }
                idList.add( id );
            }
            
            this.platformMap = platformMap;
            this.brandMap = brandMap;
            this.locationMap = locationMap;
            this.nullMap = nullMap;
            this.keySet = keySet;
            
        }
        loaded.getAndSet( true );
    }

    /**
     * @Description <b> #预定优先级: </b>
     * @param key
     * @param platform
     * @param location
     * @param brand
     * @return MessageResourceBean
     * @since 1.0
     */
    public MessageResourceBean getMessageResourceBean( String key, String platform, String location, String brand ){
        //第一次加载
        if( this.loaded.get() == false ){
            this.list4Code();
        }
        
        List<String> list = this.keySet.get( key );
       // System.out.println( JSON.toJSONString( this.keySet ) );
        MessageResourceBean resultMessage = null;
        if( list == null ){//没找到key，直接推出
            return resultMessage;
        }
        for( String id : list ){
            String valueId1 = id + "#" + key + "#" + platform;
            MessageResourceBean m1 = this.platformMap.get( valueId1 );

            String valueId2 = id + "#" + key + "#" + location;
            MessageResourceBean m2 = this.locationMap.get( valueId2 );

            String valueId3 = id + "#" + key + "#" + brand;
            MessageResourceBean m3 = this.brandMap.get( valueId3 );

            String valueId4 = id + "#" + key + "#" + brand;
            MessageResourceBean m4 = this.nullMap.get( valueId4 );

            // aa
            if( m1 != null && m2 != null && m3 != null ){// 在platform，location，brand中找到，最优先
                resultMessage = m1;
                return resultMessage;
            }else if( m1 != null && m2 != null && m3 == null ){// 在platform，location中找到，brand中没有，第二选
                resultMessage = m1;
                return resultMessage;
            }else if( m1 != null && m2 == null && m3 != null ){// 在platform，brand中找到，location中没有，第三选
                resultMessage = m1;
                return resultMessage;
            }else if( m1 == null && m2 != null && m3 != null ){// 在platform中没有，brand，location中有，第四选
                resultMessage = m2;
                return resultMessage;
            }else if( m1 != null && m2 == null && m3 == null ){// 只有platform中有，第五选
                resultMessage = m1;
                return resultMessage;
            }else if( m1 == null && m2 != null && m3 == null ){// 在location中有，第六选
                resultMessage = m2;
                return resultMessage;
            }else if( m1 == null && m2 == null && m3 != null ){// 在brand中有，第七选
                resultMessage = m3;
                return resultMessage;
            }else{// 都没有找到，可以去找nullMap
                if( m4 != null ){
                    resultMessage = m4;
                    return resultMessage;
                }
            }

        }
        return resultMessage;
    }

}