package com.xiaoi.south.app.dao;

import java.util.List;
import java.util.Map;

/**
 * @title IBaseDao
 * @description BaseDao接口
 * @author ziQi
 * @version 下午5:01:33
 * @create_date 2015年12月19日下午5:01:33
 * @copyright (c) jacky
 */
public interface IBaseDao<T> {

    /**
     * @Description 更新 </br>
     * @param sql sql语句
     * @param entity bean对象
     * @return int
     * @since 2015年12月31日
     */
    public int update( String sql, T entity );

    /**
     * update(更新)
     * 
     * @param sql sql语句
     * @param paramMap map对象
     * @return int
     * @since 2015年12月31日
     */
    public int update( String sql, Map<String, Object> paramMap );

    /**
     * batchUpdate(批量更新)
     * 
     * @param sql sql语句
     * @param list bean的list对象
     * @return int[]
     * @since 2015年12月31日
     */
    public int[] batchUpdate( String sql, List<T> list );

    /**
     * save(保存)
     * 
     * @param sql sql语句
     * @param entity bean对象
     * @return int
     * @since 2015年12月31日
     */
    public int save( String sql, T entity );

    /**
     * save(保存)
     * 
     * @param sql sql语句
     * @param paramMap Map对象
     * @return int
     * @since 2015年12月31日
     */
    public int save( String sql, Map<String, Object> paramMap );

    /**
     * batchSave(批量保存)
     * 
     * @param sql sql语句
     * @param list bean的list对象
     * @return int[]
     * @since 2015年12月31日
     */
    public int[] batchSave( String sql, List<T> list );

    /**
     * delete(删除)
     * 
     * @param sql sql语句
     * @param entity bean对象
     * @return int
     * @since 2015年12月31日
     */
    public int delete( String sql, T entity );

    /**
     * delete(删除)
     * 
     * @param sql sql语句
     * @param paramMap Map对象
     * @return int
     * @since 2015年12月31日
     */
    public int delete( String sql, Map<String, Object> paramMap );

    /**
     * batchDelete(批量删除)
     * 
     * @param sql
     * @param list
     * @return int[]
     * @since 2015年12月31日
     */
    public int[] batchDelete( String sql, List<T> list );

    /**
     * find(查找)
     * 
     * @param sql sql语句
     * @param entity bean对象
     * @return List<T>
     * @since 2015年12月31日
     */
    public List<T> find( String sql, T entity );

    /**
     * find(查找)
     * 
     * @param sql sql语句
     * @param entity bean对象
     * @param clazz 返回类
     * @return List<T>
     * @since 2015年12月31日
     */
    public List<T> find( String sql, T entity, Class<T> clazz );

    /**
     * find(查找)
     * 
     * @param sql sql语句
     * @param paramMap Map对象
     * @param clazz 返回类
     * @return List<T>
     * @since 2015年12月31日
     */
    public List<T> find( String sql, Map<String, Object> paramMap, Class<T> clazz );

    /**
     * get(获取)
     * 
     * @param sql sql语句
     * @param entity bean对象
     * @return T
     * @since 2015年12月31日
     */
    public T get( String sql, T entity );

    /**
     * get(获取)
     * 
     * @param sql sql语句
     * @param entity bean对象
     * @param clazz 返回类
     * @return T
     * @since 2015年12月31日
     */
    public T get( String sql, T entity, Class<T> clazz );

    /**
     * get(获取)
     * 
     * @param sql sql语句
     * @param paramMap Map对象
     * @param clazz 返回类
     * @return T
     * @since 2015年12月31日
     */
    public T get( String sql, Map<String, Object> paramMap, Class<T> clazz );

    /**
     * count(统计)
     * 
     * @param sql sql语句
     * @param paramMap Map对象
     * @return Long
     * @since 2015年12月31日
     */
    public Long count( String sql, Map<String, Object> paramMap );

    /**
     * count(统计)
     * 
     * @param sql sql语句
     * @param paramMap Map对象
     * @param clazz 返回类
     * @return T
     * @since 2015年12月31日
     */
    public T count( String sql, Map<String, Object> paramMap, Class<T> clazz );

    /**
     * executeSql(执行sql)
     * 
     * @param sql sql语句
     * @return int
     * @since 2015年12月31日
     */
    public int executeSql( String sql );

    /**
     * executeSql(执行sql)
     * 
     * @param sql sql语句
     * @param entity bena对象
     * @return int
     * @since 2015年12月31日
     */
    public int executeSql( String sql, T entity );

    /**
     * executeSql(执行sql)
     * 
     * @param sql sql语句
     * @param paramMap Map对象
     * @return int
     * @since 2015年12月31日
     */
    public int executeSql( String sql, Map<String, Object> paramMap );

}