package com.xiaoi.south.app.modules.utils;

import com.alibaba.fastjson.JSONObject;
import com.xiaoi.south.app.cache.CacheSupport;
import com.xiaoi.south.app.constant.GlobalConstants;
import com.xiaoi.south.app.entity.RobotRequest;
import com.xiaoi.south.app.entity.RobotResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * @author brave.chen
 * @create 2019-11-05 16:10
 */
public class ArtificialUtils {
    static CacheSupport cacheSupport = new CacheSupport(GlobalConstants.APP_CACHE);


    /**
     * 获取token ID
     * @param userId
     * @return tokenId
     */
    public static String   getTokenId(String userId){
        String tokenId="";
        if (cacheSupport.get("liveChatTokenId-" + userId) == null || "".equals(cacheSupport.get("liveChatTokenId-" + userId))) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("app_id", "0d962cbcc4694ce4905f481ca266e825");
            map.put("app_secret", "f2ee5a976b7c4ddca01b7b2269f4c9ec");
            String result = HttpUtils.doGet(PropertiesUtils.getString("9Client.getToken"), map, "utf-8");
            int i = result.indexOf("accesss_token");
            if (i != -1) {
                result = result.substring(i + 16, result.length());
                result = result.substring(0, result.indexOf("\""));
                tokenId = "liveChatTokenId-" + userId;
                cacheSupport.put(tokenId, result);
            }
        }else{
            tokenId=cacheSupport.get("liveChatTokenId-" + userId).toString();
        }
        return tokenId;
    }

    /**
     * 获取content Id
     * @param userId 用户ID
     * @param tokenId
     * @return context_id
     */
    public static String  getContentId(String userId,String tokenId){
        String context_id="";
        if (cacheSupport.get("context_id-" + userId) == null || "".equals(cacheSupport.get("context_id-") + userId)) {
            JSONObject json=new JSONObject();
            json.put("access_token",tokenId);
            JSONObject visitor_info=new JSONObject();
            visitor_info.put("visitor_id","test001");//用户ID
            visitor_info.put("visitor_name","");     //用户名
            visitor_info.put("mobile_phone","电话");
            visitor_info.put("email","xxx@163.com");
            json.put("visitor_info",visitor_info);
            json.put("visitor_origin","PC");
            String  UserInfo=HttpUtils.doPost(PropertiesUtils.getString("9Client.getContentId"),json.toJSONString());
            int i=UserInfo.indexOf("context_id");
            if(i!=-1){
                UserInfo = UserInfo.substring(i + 13, UserInfo.length());
                UserInfo = UserInfo.substring(0, UserInfo.indexOf("\""));
                context_id="liveChatContextId-" + userId;
                cacheSupport.put(context_id, UserInfo);
            }
        }else{
            context_id=cacheSupport.get("context_id-" + userId).toString();
        }


        return context_id;
    }

    /**
     *
     * @param robotRequest  请求参数
     * @param tokenId   9Client 校验
     * @param context_id    9Client 用户ID标识
     * @return 请求9Client 结果返回结果
     */
    public static String sendMessage(RobotRequest robotRequest,String tokenId, String  context_id){
        String result="";
        //发送用户问题给人工客户
        JSONObject sendJson=new JSONObject();
        sendJson.put("access_token",tokenId);
        sendJson.put("context_id",context_id);
        JSONObject sendContent=new JSONObject();
        sendContent.put("msg_type","text");
        sendContent.put("text"," {\"content\": \""+robotRequest.getQuestion()+"\"}");
        sendJson.put("content",sendContent);
        String sendResult=HttpUtils.doPost(PropertiesUtils.getString("9Client.sendMessage"),sendJson.toJSONString());

        return  result;
    }
}
