package com.xiaoi.south.app.modules.utils;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

/**
 * author: cxy
 * date: 2016/9/29
 */
@Component
public class ZxHttpUtil {

    // 表示服务器端的url

    private ZxHttpUtil() {
        // TODO Auto-generated constructor stub
    }
    public static void main(String[] args) {
		Map<String, String> map=new HashMap<String, String>();
		map.put("objectId", "test001");  
		map.put("hfjg","01"); 
		String temp=sendPostMessage("http://api.vatti.com.cn/jobtask/api/return/visit", map, "UTF-8");
		System.out.println(temp);
	}
    
    
    /**
     * 只传地址
     * @param url
     * @return
     */
    public static String httpPost(String url){
    	JSONObject jsonObj = new JSONObject();
    	jsonObj.put("1", "1");
    	return httpPostWithJson(jsonObj, url);
    }
    
    
    /*
    * post请求
    * @param url
    * @param json
    * @return
    */

    public static String httpPostWithJson(JSONObject jsonObj,String url){
        String result="";
        
        HttpPost post = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();

            // 设置超时时间
            httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 10000);
            httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 10000);
                
            post = new HttpPost(url);
            // 构造消息头
            post.setHeader("Content-type", "application/json; charset=utf-8");
            post.setHeader("Connection", "Close");
//            String sessionId = getSessionId();
//            post.setHeader("SessionId", sessionId);
//            post.setHeader("appid", appid);
                        
            // 构建消息实体
            StringEntity entity = new StringEntity(jsonObj.toString(), Charset.forName("UTF-8"));
            entity.setContentEncoding("UTF-8");
            // 发送Json格式的数据请求
            entity.setContentType("application/json");
            post.setEntity(entity);
                
            HttpResponse response = httpClient.execute(post);
            JSONObject object = null;
            HttpEntity entity1 = response.getEntity();
            if(entity!=null){
               result = EntityUtils.toString(entity1,"UTF-8");

//               System.out.println(result);

            }
            // 检验返回码
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode != HttpStatus.SC_OK){
//                LogUtil.info("请求出错: "+statusCode);
            	System.out.println("请求出错: "+statusCode);
            }else{
                int retCode = 0;
                String sessendId = "";
                // 返回码中包含retCode及会话Id
                for(Header header : response.getAllHeaders()){
                    if(header.getName().equals("retcode")){
                        retCode = Integer.parseInt(header.getValue());
                    }
                    if(header.getName().equals("SessionId")){
                        sessendId = header.getValue();
                    }
                }
                
//                if(ErrorCodeHelper.IAS_SUCCESS != retCode ){
//                    // 日志打印
//                    LogUtil.info("error return code,  sessionId: "sessendId"\t"+"retCode: "+retCode);
//                    isSuccess = false;
//                }else{
//                    isSuccess = true;
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(post != null){
                try {
                    post.releaseConnection();
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    /*
     * params 填写的URL的参数 encode 字节编码
     */
    public static String sendPostMessage(String strUrl,Map<String, String> params,
                                         String encode) {

        URL url = null;
        try {
            url = new URL(strUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        StringBuffer stringBuffer = new StringBuffer();

        if (params != null && !params.isEmpty()) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                try {
                	  if(entry.getValue()!=null&&!"".equals(entry.getValue())){
                		  stringBuffer
                            .append(entry.getKey())
                            .append("=");
                  
                    	stringBuffer.append("").append(URLEncoder.encode(entry.getValue(), encode))
                        .append("&");
                    }
                            

                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            // 删掉最后一个 & 字符
            if(stringBuffer.length()>1){
            	stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            }
            
            System.out.println("-->>" + stringBuffer.toString());

            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) url
                        .openConnection();
                httpURLConnection.setConnectTimeout(3000);
                httpURLConnection.setDoInput(true);// 从服务器获取数据
                httpURLConnection.setDoOutput(true);// 向服务器写入数据
//                JSONObject.parse(stringBuffer.toString());
                // 获得上传信息的字节大小及长度
//                byte[] mydata = stringBuffer.toString().getBytes();
                byte[] mydata = JSONObject.parse(stringBuffer.toString()).toString().getBytes();
                // 设置请求体的类型
                httpURLConnection.setRequestProperty("Content-Type",
                        "application/json; charset=utf-8");
                httpURLConnection.setRequestProperty("Content-Lenth",
                        String.valueOf(mydata.length));

                // 获得输出流，向服务器输出数据
                OutputStream outputStream = (OutputStream) httpURLConnection
                        .getOutputStream();
                outputStream.write(mydata);

                // 获得服务器响应的结果和状态码
                int responseCode = httpURLConnection.getResponseCode();
                System.out.println(responseCode);
                if (responseCode == 200) {

                    // 获得输入流，从服务器端获得数据
                    InputStream inputStream = (InputStream) httpURLConnection
                            .getInputStream();
                    return (changeInputStream(inputStream, encode));

                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return "";
    }

    /*
     * // 把从输入流InputStream按指定编码格式encode变成字符串String
     */
    public static String changeInputStream(InputStream inputStream,
                                           String encode) {

        // ByteArrayOutputStream 一般叫做内存流
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] data = new byte[1024];
        int len = 0;
        String result = "";
        if (inputStream != null) {

            try {
                while ((len = inputStream.read(data)) != -1) {
                    byteArrayOutputStream.write(data, 0, len);

                }
                result = new String(byteArrayOutputStream.toByteArray(), encode);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        return result;
    }
}