package com.xiaoi.south.app.modules.utils;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author brave.chen
 * @create 2019-10-29 14:17
 * KB单点登入
 * manager单点登入
 */
public class SSoUtils {

    /**
     * 重定向进入知识库
     * @param userName
     */
    public static String KBsso( String userName,String  url){
//        String  url="https://hkbn.xiaoi.com/kbaseui-std/ssoLogin.do";
        String ticket=KbaseDecoderUtil.encode(userName);
        //Calendar.getInstance().getTime()
        String token=KbaseDecoderUtil.getToken(Calendar.getInstance().getTime(), userName, "");
        url+="?ticket="+ticket+"&token="+token;
        System.out.println(url);
        return url;
       /* try {
            response.sendRedirect(url);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    public static final String CHARSET = "utf-8";
    //manager中配置的加密串
    private final static String debugkey = "abcaaaaaaaaaaaaaaaaxgxga";
    private static Logger log = LoggerFactory.getLogger(SSoUtils.class);
    private static byte[] getKey() throws Exception {
        byte[] ret = null;
        ret = debugkey.getBytes(CHARSET);
        return ret;
    }

    static String transformation = "DESede/ECB/PKCS5Padding";
    static String algorithm = "DESede";

    private static String encode(String src) throws Exception {
        return encode0(src, getKey());
    }

    private static String encode0(String src, byte[] key) throws Exception {
        SecretKey secretKey = new SecretKeySpec(key, algorithm);
        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] b = cipher.doFinal(src.getBytes(CHARSET));
        return new String(Hex.encodeHex(b));
    }

    public String decode(String dest) throws Exception {
        return decode0(dest, getKey());
    }

    public String decode0(String dest, byte[] key) throws Exception {
        SecretKey secretKey = new SecretKeySpec(key, algorithm);
        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] b = cipher.doFinal(Hex.decodeHex(dest.toCharArray()));
        return new String(b, CHARSET);
    }
    // for manager 后台单点
    public static String getManagerUrl(String username, String userrole, String managerPath) {
        String path = "";
        try {
            String raw = "timestamp="
                    + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
                    + "&userprivilege=0&username="
                    + username + "&userroles=" + userrole + "";
            String token = encode(raw);
            String digest = DigestUtils.md5Hex(token);
            HttpClient client = new HttpClient();
            PostMethod m = new PostMethod(managerPath
                    + "authmgr/auth!login.action");
            m.addRequestHeader("X-Requested-With", "XMLHTTPRequest");
            m.addParameter("ssotoken", token);
            m.addParameter("ssochecksum", digest);
            client.executeMethod(m);
            log.debug("登录信息验证" + "statusCode=" + m.getStatusCode()
                    + ",responseText=" + m.getResponseBodyAsString());
            m.releaseConnection();
            path = String.format(managerPath
                            + "authmgr/auth!login.action?ssotoken=%s&ssochecksum=%s",
                    token, digest);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return path;
    }

    public static void main(String[] args) throws Exception {
        String url=getManagerUrl("chenxingyu","管理员",
                "https://hkbn.xiaoi.com/manager/app/hkbn-hk/");
        System.out.println(url);
        url=KBsso("brave","https://hkbn.xiaoi.com/kbaseui-std/ssoLogin.do");
        System.out.println(url);
    }

}
