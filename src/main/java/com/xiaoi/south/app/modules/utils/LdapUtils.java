package com.xiaoi.south.app.modules.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.BindRequest;
import org.apache.directory.api.ldap.model.message.BindRequestImpl;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.apache.directory.ldap.client.api.SaslGssApiRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.io.IOException;
import java.util.Hashtable;

/**
 * @author brave.chen
 * @create 2019-11-01 12:05
 */
@Component
public class LdapUtils {
    private static final Logger LOG = LoggerFactory.getLogger(LdapUtils.class);


    /**
     *  "ldapsvrlist.hkgad.msad.local:389" "CN=xiaoi.service,OU=Service Account,DC=MSAD,DC=LOCAL" "Lk@F76=m"
     * @param url  ldapsvrlist.hkgad.msad.local:389
     * @param user CN=xiaoi.service,OU=Service Account,DC=MSAD,DC=LOCAL
     * @param pwd   Lk@F76=m
     * @return
     */
    public static   Boolean ldapConn(String url,String user,String pwd){

        if(PropertiesUtils.getBoolean("ldap.stat")){//是否开启验证
            if(url==null||"".equals(url)){
                url=PropertiesUtils.getString("ldap.url");
            }
            Hashtable<String, String> tbl = new Hashtable<String, String>(4);
            String LDAP_URL = "ldap://" +url+ "/dc=MSAD,dc=LOCAL";
            tbl.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            tbl.put(Context.PROVIDER_URL, LDAP_URL);
            tbl.put(Context.SECURITY_AUTHENTICATION, "simple");
            tbl.put(Context.SECURITY_PRINCIPAL, user);
            tbl.put(Context.SECURITY_CREDENTIALS, pwd);
            System.out.println("env setting");
            DirContext context = null;
            try {
                LdapContext ctx = null;
                Control[] connCtls = null;
                ctx = new InitialLdapContext(tbl, connCtls);
                System.out.println("login verification begins...");
                // context = new InitialDirContext(tbl);
                System.out.println("login successfully.");
                return true;
            } catch (AuthenticationException e) {
                System.out.println("Login Ldap Server Failed...");
                e.printStackTrace();
                return false;
            }catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("login failed.");
                return false;
            } finally {
                try {
                    if (context != null) {
                        context.close();
                        context = null;
                    }
                    tbl.clear();
                } catch (Exception e) {
                    System.out.println("exception happened.");
                }
            }
        }else{
            return true;
        }

    }


    public Boolean ldapConnection(String url,int port,String user,String pwd){
        LdapConnectionConfig config=new LdapConnectionConfig();
        config.setLdapHost(url);
        config.setLdapPort(port);
        config.setName(user);
        config.setCredentials(pwd);
        LdapConnection connection=new  LdapNetworkConnection(config);
        try {
            connection.bind();
            if(connection.isConnected()){
                LOG.info("连接成功");
                return true;
            }else{
                LOG.info("连接失败");
                return false;
            }
        } catch (LdapException e) {
            e.printStackTrace();
            LOG.info("连接失败");
            return false;
        }finally {
            try {
                connection.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
    /**
     * 连接ldap
     * @param url
     * @param user
     * @param pwd
     * @param path
     * @return
     */
    public Boolean ldapConnection(String url,String user,String pwd,String path){
        boolean flag=false;
        try {
            if(StringUtils.isNotBlank(""));

//            LdapNetworkConnection ldap=new LdapNetworkConnection("ldap://ldapsvrlist.gzad.msad.local:389");
            LdapNetworkConnection ldap=new LdapNetworkConnection(url);
//            ldap.bind("xiaoi.service","Lk@F76=m");
            ldap.bind(user,pwd);
            SaslGssApiRequest sga=new SaslGssApiRequest();
            sga.setKrb5ConfFilePath(path);
            ldap.bind(sga);
            flag=ldap.connect();
            if(flag){
                LOG.info("连接成功");
                return  flag;
            }else{
                LOG.info("连接失败");
                return  flag;
            }
        } catch (LdapException e) {
            LOG.info("连接异常");
            e.printStackTrace();
            return  flag;
        }
    }

    public static void main(String[] args) {
        if(StringUtils.isNotBlank(""));
        System.out.println("111");
    }
}
