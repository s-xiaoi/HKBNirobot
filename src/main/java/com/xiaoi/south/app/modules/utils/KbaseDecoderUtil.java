package com.xiaoi.south.app.modules.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 需要两个参数一个是 工号 的Base64 加密，一个是token的MD5加密
 * 概念
 * ticket 工号，加密可逆
 * token 签名认证，不可逆，组成：yyyyMMdd + 工号 + (Salt)
 * salt 密钥，双方约定保持一致，默认是 ibotkbs
 * @author brave.chen
 * @date 2018/12/23
 * @version 2.0
 */
public class KbaseDecoderUtil {
	private final static String SALT = "ibotkbs";
	private static DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
	
	/**
	 * 用于ticket加密
	 * @author eko.zhan at Jul 27, 2017 2:15:28 PM
	 * @param raw ticket明文
	 * @return
	 */
	public static String encode(String raw){
		return new String(Base64.encodeBase64(raw.getBytes()));
	}
	
	/**
	 * 用于ticket解密
	 * @author eko.zhan at Jul 27, 2017 2:15:04 PM
	 * @param enc 加密后的ticket
	 * @return
	 */
	public static String decode(String enc){
		return new String(Base64.decodeBase64(enc.getBytes()));
	}
	
	/**
	 * 根据指定的加密过的工号，token，返回指定的工号明文
	 * @author eko.zhan at Jul 27, 2017 2:34:04 PM
	 * @param enc 加密的工号
	 * @param token 签名
	 * @param salt 密钥
	 * @return
	 */
	public static String decode(String enc, String token, String salt){
		String raw = decode(enc);
		if (verify(token, raw, salt)){
			return raw;
		}
		return null;
	}
	
	/**
	 * 根据指定的日期，工号明文，密钥明文获取签名数据
	 * @author eko.zhan at Aug 10, 2017 10:16:06 AM
	 * @param date
	 * @param ticket 工号明文
	 * @param salt 密钥
	 * @return
	 */
	public static String getToken(Date date, String ticket, String salt){
		salt = StringUtils.isBlank(salt)?SALT:salt;
		return DigestUtils.md5Hex(sdf.format(date) + ticket + salt);
	}
	
	/**
	 * 根据指定的日期，工号明文，密钥明文获取签名数据
	 * @author eko.zhan at Aug 10, 2017 10:17:41 AM
	 * @param ticket
	 * @param salt
	 * @return
	 */
	public static String getToken(String ticket, String salt){
		salt = StringUtils.isBlank(salt)?SALT:salt;
		return getToken(Calendar.getInstance().getTime(), ticket, salt);
	}
	
	/**
	 * 通过salt值验证token是否是正确的数据
	 * token两分钟内有效
	 * @author eko.zhan at Aug 10, 2017 10:21:20 AM
	 * @param token 用户传入的token
	 * @param ticket 工号密文
	 * @param salt 密钥
	 * @return
	 */
	public static Boolean verify(String token, String ticket, String salt){
		if (token==null) return false;
		salt = StringUtils.isBlank(salt)?SALT:salt;
		Calendar cal = Calendar.getInstance();
		for (int i=0 ; i>-3; i--){
			cal.add(Calendar.MINUTE, i);
			String enc = getToken(cal.getTime(), ticket, salt);
			if (token.equals(enc)){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
}
