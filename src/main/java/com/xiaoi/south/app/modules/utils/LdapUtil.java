package com.xiaoi.south.app.modules.utils;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.util.Hashtable;
import java.util.List;
import java.util.Map; /**
 * @author brave.chen
 * @create 2019-11-11 15:22
 */
public class LdapUtil {



    public static Map<String,String> getUserRole(List<Map<String, Object>> listRoles, String ldapUrl, String ldapServiceAccountDn, String ldapPassword, String ldapNormalUserDn, String ldapSearchFilter, String userId, String userPwd) {
        System.out.println("IPAdress: " + ldapUrl);
        System.out.println("Username: " + userId);
        System.out.println("Password: " + ldapPassword);
        Hashtable<String, String> tbl = new Hashtable<String, String>(4);
        String LDAP_URL = "ldap://" +ldapUrl+ "/dc=MSAD,dc=LOCAL";
        tbl.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        tbl.put(Context.PROVIDER_URL, LDAP_URL);
        tbl.put(Context.SECURITY_AUTHENTICATION, "simple");
        tbl.put(Context.SECURITY_PRINCIPAL, userId);
        tbl.put(Context.SECURITY_CREDENTIALS, ldapPassword);
        System.out.println("env setting");
        DirContext context = null;
        try {
            LdapContext ctx = null;
            Control[] connCtls = null;
            ctx = new InitialLdapContext(tbl, connCtls);
            System.out.println("login verification begins...");
            // context = new InitialDirContext(tbl);
            System.out.println("login successfully.");
        } catch (AuthenticationException e) {
            System.out.println("Login Ldap Server Failed...");
            e.printStackTrace();
        }catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("login failed.");
        } finally {
            try {
                if (context != null) {
                    context.close();
                    context = null;
                }
                tbl.clear();
            } catch (Exception e) {
                System.out.println("exception happened.");
            }
        }
        return null;
    }
}
