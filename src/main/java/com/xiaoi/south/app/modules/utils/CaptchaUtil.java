package com.xiaoi.south.app.modules.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * <p>
 * Title: CaptchaUtil
 * </p>
 * <p>
 * Description: 关于验证码的工具类
 * </p>
 * 
 * @author zfree
 * @create_date 2019年6月3日
 */
public final class CaptchaUtil
{
    private CaptchaUtil(){}
    
    /*
     * 随机字符字典
     */
    private static final char[] CHARS = { '2', '3', '4', '5', '6', '7', '8',
        '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M',
        'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    
    /*
     * 随机数
     */
    private static Random random = new Random();
    
    /*
     * 获取6位随机数
     */
    private static String getRandomString()
    {
        StringBuffer buffer = new StringBuffer();
        for(int i = 0; i < 6; i++)
        {
            buffer.append(CHARS[random.nextInt(CHARS.length)]);
        }
        return buffer.toString();
    }
    
    /*
     * 获取随机数颜色
     */
    private static Color getRandomColor()
    {
        return new Color(random.nextInt(255),random.nextInt(255),
                random.nextInt(255));
    }
    
    /*
     * 返回某颜色的反色
     */
    private static Color getReverseColor(Color c)
    {
        return new Color(c.getRed(), c.getGreen(),c.getBlue());
    }
    
    /**
     * 随机验证码
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public static void outputCaptcha(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {

        response.setContentType("image/jpeg");

        String randomString = getRandomString();
        request.getSession(true).setAttribute("captcha", randomString);

        int width = 100;
        int height = 30;

        Color color = getRandomColor();
        Color reverse = getReverseColor(color);

        BufferedImage bi = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bi.createGraphics();
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
        g.setColor(color);
        g.fillRect(0, 0, width, height);
        g.setColor(reverse);
        g.drawString(randomString, 18, 20);
        for (int i = 0, n = random.nextInt(100); i < n; i++) 
        {
            g.drawRect(random.nextInt(width), random.nextInt(height), 1, 1);
        }

        // 转成JPEG格式
        ServletOutputStream out = response.getOutputStream();
        JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
        encoder.encode(bi);
        out.flush();
    }
    
	private static Color getRandColor(int fc, int bc) {
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = 230;
		int g = 230;
		int b = 230;
		return new Color(r, g, b);
	}  
    
    /**
     * 小i后台验证码
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public static void outputCaptchaByImanager(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException  {

		String vRand = "";
		int width = 55;
		int height = 20;
		BufferedImage image = new BufferedImage(width, height, 1);
		Graphics graphic = image.getGraphics();
		Random random = new Random();
		graphic.setColor(getRandColor(1, 255));
		graphic.fillRect(0, 0, width, height);
		graphic.setFont(new Font("Times New Roman", 3, 16));
		graphic.setColor(new Color(255, 0, 0));
		for (int i = 0; i < 1; i++) {
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			int xl = random.nextInt(12);
			int yl = random.nextInt(12);
			graphic.drawLine(x, y, x + xl, y + yl);
		}
		for (int i = 0; i < 4; i++) {
			String rand = String.valueOf(random.nextInt(10));
			vRand = vRand + rand;
			graphic.setColor(new Color(0, 0, 0));
			graphic.drawString(rand, 13 * i + 4, 16);
		}
		request.getSession().setAttribute("captcha", vRand);
		graphic.dispose();
		try {
			ImageIO.write(image, "JPEG", response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}