package com.xiaoi.south.app.modules.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @title PropertiesUtils
 * @description 自动加载app.properties文件，不需要重启服务
 * @author ziQi
 * @version 上午11:51:05
 * @create_date 2016-9-19上午11:51:05
 * @copyright (c) jacky
 */
public class PropertiesUtils{

    private static Properties params = new Properties();

    private static String appPropertiesPath;

    private static long appPropertiesMtime;

    private static Logger logger = LoggerFactory.getLogger( PropertiesUtils.class );

    // 定时reload配置文件
    private static Timer timer = new Timer();

    static{
        try{
            String appFile = "/app.properties";
            params.load( PropertiesUtils.class.getResourceAsStream( appFile ) );
            appPropertiesPath = PropertiesUtils.class.getResource( appFile ).getPath();
            File file = new File( appPropertiesPath );
            appPropertiesMtime = file.lastModified();
            // 30秒钟检测一次
            timer.scheduleAtFixedRate( new TimerTask(){
                public void run(){
                    reloadAppProperties();
                }
            }, 1000, 30000 );
        }catch( Exception e ){
            e.printStackTrace();
        }
    }

    /**
     * @Description <b>(获取app.properteis设置的参数值)</b></br>
     * @param key
     * @return String
     * @since 2016-9-20
     */
    public static String getString( String key ){
        return params.getProperty( key );
    }

    /**
     * @Description <b>(获取app.properteis设置的布尔值)</b></br>
     *              <p>
     *              当参数值设置为字符串true或者1时，返回true 其它参数值值都视为false, 方法默认返回为false
     *              </p>
     * @param key
     * @return boolean
     * @since 2016-9-20
     */
    public static boolean getBoolean( String key ){
        String value = params.getProperty( key );
        return StringTookits.parseBoolean( value );
    }

    /**
     * @Description <b>(获取app.properteis设置的int值)</b></br>
     *              <p>
     *              当参数值设置为数字时,返回数值数 默认返回-1
     *              </p>
     * @param key
     * @return boolean
     * @since 2016-9-20
     */
    public static int getInt( String key ){
        String value = params.getProperty( key );
        return StringTookits.parseInt( value, -1 );
    }

    /**
     * @Description <b>(重载app.properties)</b></br> void
     * @since 2016-9-20
     */
    private static void reloadAppProperties(){
        File file = new File( appPropertiesPath );
        long lastMtime = file.lastModified();
        if( lastMtime > appPropertiesMtime ){
//            logger.info( "PropertiesLoader reloadAppProperties start!" );
//            logger.info( "appPropertiesPath:" + appPropertiesPath );
            try{
                params.load( PropertiesUtils.class.getResourceAsStream( "/app.properties" ) );
            }catch( Exception e ){
                e.printStackTrace();
            }
        }
    }

    /**
     * getProperties(加载类路径下的properties)
     */
    public static Properties getProperties( String name ){
        InputStream inputStream = PropertiesUtils.class.getResourceAsStream( name );
        Properties props = new Properties();
        try{
            props.load( inputStream );
        }catch( IOException e ){
            e.printStackTrace();
        }
        return props;
    }

    /**
     * getProperties(加载properties)
     * 
     * @param File
     */
    public static Properties getProperties( File file ){
        FileInputStream in = null;
        Properties p = null;
        try{
            in = new FileInputStream( file );
            p = getProperties( in );
        }catch( FileNotFoundException e ){
            e.printStackTrace();
        }
        return p;
    }

    /**
     * getProperties(加载properties)
     * 
     * @param InputStream
     */
    public static Properties getProperties( InputStream in ){
        Properties p = null;
        try{
            p = new Properties();
            p.load( in );
            in.close();
        }catch( IOException e ){
            e.printStackTrace();
        }
        return p;
    }

    /**
     * getPropertiesMap(解析文本里面的key=value对，组装成Map)
     * 
     * @param name
     * @param charset
     * @param @return 设定文件
     */
    public static Map<String, String> getPropertiesMap( String name, String charset ){
        BufferedReader br = null;
        Map<String, String> map = new HashMap<String, String>();
        try{
            br = new BufferedReader( new InputStreamReader( PropertiesUtils.class.getResourceAsStream( name ), charset ) );
            String line = null;
            while( (line = br.readLine()) != null ){
                line = line.trim();
                if( line.length() > 0 ){
                    int idx = line.indexOf( "=" );
                    if( idx > 0 ){
                        map.put( line.substring( 0, idx ).trim(), line.substring( idx + 1 ).trim() );
                    }
                }
            }
            br.close();
            br = null;
        }catch( Exception e ){
            e.printStackTrace();
        }finally{
            if( br != null )
                try{
                    br.close();
                }catch( Throwable t ){
                }
        }
        return map;
    }

    /**
     * getPropertiesMap(解析字符串：key1=value,key2=value,key3=value，组装成Map)
     * 
     * @param name
     * @param charset
     * @param @return 设定文件
     */
    public static Map<String, String> loadStringAsMap( String str ){
        Map<String, String> map = new HashMap<String, String>();
        // 截取
        try{
            if( !StringUtils.isEmpty( str ) ){
                String[] arrs = str.split( "," );
                if( arrs != null ){
                    for( int i = 0; i < arrs.length; i++ ){
                        int idx = arrs[i].indexOf( "=" );
                        if( idx > 0 ){
                            map.put( arrs[i].substring( 0, idx ).trim(), arrs[i].substring( idx + 1 ).trim() );
                        }
                    }
                }

            }
        }catch( Exception e ){
            e.printStackTrace();
        }
        return map;
    }

}
