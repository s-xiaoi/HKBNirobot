/**    
 * 文件名：IpUtils.java    
 *
 * 版本信息：    
 * 日期：2018年3月30日    
 * create by ziQi
 */
package com.xiaoi.south.app.modules.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @title IpUtils
 * @description TODO
 * @author ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class IpUtils{
    /**
     * @Description <b> #获取本机Ip </b>
     * @return String
     * @since 1.0
     */
    public static String getHostIp(){
        String ip = null;
        try{
            ip = InetAddress.getLocalHost().getHostAddress();
        }catch( UnknownHostException e ){
            e.printStackTrace();
        }
        return ip;
    }

    /**
     * @Description <b> #获取本机host </b>
     * @return String
     * @since 1.0
     */
    public static String getHostName(){
        String name = null;
        try{
            name = InetAddress.getLocalHost().getHostName();
        }catch( UnknownHostException e ){
            e.printStackTrace();
        }
        return name;
    }

    /**
     * @Description <b> #获取本机所有网卡ip </b>
     * @return List<String>
     * @since 1.0
     */
    public static Map<String,String> getHostIps(){
        Map<String,String> result = new HashMap<String,String>();
        Enumeration<NetworkInterface> netInterfaces = null;
        try{
            netInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress ip;
            while( netInterfaces.hasMoreElements() ){
                NetworkInterface ni = netInterfaces.nextElement();
                Enumeration<InetAddress> addresses = ni.getInetAddresses();
                while( addresses.hasMoreElements() ){
                    ip = addresses.nextElement();
                    if( !ip.isLoopbackAddress() && ip.getHostAddress().indexOf( ':' ) == -1 ){
                        String hostAddr = ip.getHostAddress();
                        result.put( hostAddr, hostAddr );
                    }
                }
            }
            return result;
        }catch( Exception e ){
            return null;
        }
    }
}
