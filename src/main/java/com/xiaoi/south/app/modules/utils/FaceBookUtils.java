package com.xiaoi.south.app.modules.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.eastrobot.robotface.domain.RobotCommand;
import com.eastrobot.robotface.domain.RobotResponseEx;
import com.xiaoi.south.app.cache.CacheSupport;
import com.xiaoi.south.app.constant.GlobalConstants;
import com.xiaoi.south.app.entity.RobotRequest;

import me.ramswaroop.jbot.core.facebook.models.Attachment;
import me.ramswaroop.jbot.core.facebook.models.Button;
import me.ramswaroop.jbot.core.facebook.models.Element;
import me.ramswaroop.jbot.core.facebook.models.Event;
import me.ramswaroop.jbot.core.facebook.models.Message;
import me.ramswaroop.jbot.core.facebook.models.Payload;

/**
 * 封装报文
 * 
 * @porter Chenxingyu
 * @email brave.chen@xiaoi.com
 * @since 1.0
 * @author brave.chen
 *
 */
public class FaceBookUtils {
	private static final String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
	private static final String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式
	private static final String srcRegex = "\\[link\\s+submit=[\'\"]+([^\\[\\]\'\"]+)[\'\"]+\\s*[^\\[\\]]*\\]([^\\[\\]]+)\\[\\/link\\]";
	private static final String imageUlr=PropertiesUtils.class.getResource("/app.properties").getPath().replace("app.properties", "");
	private static CacheSupport cacheSupport = new CacheSupport( GlobalConstants.APP_CACHE );
	
	/**
	 * 请求数据解析处理成RobotRequest
	 * 
	 * @param event
	 * @return
	 */
	public static RobotRequest FaceBookEventReq(Event event) {
		RobotRequest robotRequest = new RobotRequest();
		if(event.getMessage()!=null&&event.getMessage().getAttachments()!=null) {//说明是图片或者音频
			robotRequest.setQuestion("");
		}else if(event.getPostback()!=null&&event.getPostback().getPayload()!=null) {
			//是否是回调，是的话取回调未question
			robotRequest.setQuestion(event.getPostback().getPayload());
		}else {
			robotRequest.setQuestion(event.getMessage().getText() == null ? "defaut" : event.getMessage().getText());
		}
		String lang=(String) cacheSupport.get("quLang");
		if(lang!=null&&!"".equals(lang)) {
			robotRequest.setLang(lang);
		}else {
			robotRequest.setLang("sc");
		}
		robotRequest.setUserId(event.getRecipient().getId());
		robotRequest.setPlatform("facebook");
		robotRequest.setCustom1("FM");
		return robotRequest;
	}

	/**
	 * 
	 * @param messages
	 * @return 封装content数据
	 */
	public static List<Message> FaceBookMessageResp(List<Message> messages,String text) {
		Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
		Matcher m_style = p_style.matcher(text);
		text = m_style.replaceAll(""); // 过滤style标签

		Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
		Matcher m_html = p_html.matcher(text);
		text = m_html.replaceAll(""); // 过滤html标签
		text=text.replace("\\r\\n", "");
		messages=getButton(messages,text);//组装button
		
		
//		return message.setAttachment(new Attachment().setType("template").setPayload(new Payload()
//				.setTemplateType("button").setText(text.replaceAll(srcRegex, "")).setButtons(getButton(text))));
		return messages;
	}
	
	/**
	 * 封装指令相关的数据
	 * @param messages
	 * @param resp
	 * @return
	 */
	public static List<Message> rebuild(List<Message> messages,RobotResponseEx resp){
		RobotCommand[] commands = resp.getCommands();
		Element element=new Element();
		List<Element> elements=new ArrayList<Element>();
		String title="";
    	String picurl="";
    	String url="";
    	String text="";
    	String description="";
		 if( commands != null ){
	            for( int i = 0; i < commands.length; i++ ){
	                RobotCommand command = commands[i];
	                String commandArgs[] = command.getArgs();
	                if( "imgtxtmsg".equals( command.getName() ) ){// 图文消息
	                    // 图文type=201
	                	text=commandArgs[3];
	                	title=text.substring(text.indexOf("<Title><![CDATA[")+16, text.indexOf("]]></Title"));
	            		picurl=text.substring(text.indexOf("<PicUrl><![CDATA[")+17, text.indexOf("]]></PicUrl")).replaceAll("http://hkpcpoc.demo.xiaoi.com", "https://hkpc8226.xiaoi.com")+".jpg";
	            		url=text.substring(text.indexOf("<Url><![CDATA[")+14, text.indexOf("]]></Url")).replaceAll("http://hkpcpoc.demo.xiaoi.com", "https://hkpc8226.xiaoi.com")+".jpg";
	            		description=text.substring(text.indexOf("<Description><![CDATA[")+22, text.indexOf("]]></Description"));
	            		if(title.equals("")) {
	            			title="title is null";
	            		}
	            		element.setTitle(title).setImageUrl(picurl).setSubtitle(description)
	            		.setDefaultAction(new Button().setType("web_url").setUrl(url));
//	                	messages.add(SwipeQicture());
	                }
	                if( "imgmsg".equals( command.getName() ) ){// 图片消息
	                    // 图片type=202
	                	element.setTitle("image").setImageUrl(commandArgs[2].replaceAll("http://hkpcpoc.demo.xiaoi.com", "https://hkpc8226.xiaoi.com")+".jpg")
	            		.setDefaultAction(new Button().setType("web_url").setUrl(commandArgs[2].replaceAll("http://hkpcpoc.demo.xiaoi.com", "https://hkpc8226.xiaoi.com")+".jpg"));
//	                	messages.add(new Message().setAttachment(new Attachment().setType("image")
//	                			.setPayload(new Payload().setUrl(commandArgs[2].replaceAll("http://hkpcpoc.demo.xiaoi.com", "https://hkpc8226.xiaoi.com")+".jpg"))));
	                }
	                if( "musicmsg".equals( command.getName() ) ){// 音频消息
	                    // 音频type=203
	                }
	                if( "videomsg".equals( command.getName() ) ){// 视频消息
	                    // 视频type=204
	                }
	                if ("p4".equals(command.getName())) {// p4消息
	                }
	                if ("secondQuestion".equals(command.getName())) {// 一问二答 type=401
	                }
	                
	                if(element.getTitle()!=null&&!"".equals(element.getTitle())) {
	                	  elements.add(element);
	                	  element=new Element();
	                }
	               
	            }
	        }
		 Element[] elementss=new Element[elements.size()]; 
		 int i=0;
		 for(Element e:elements) {
			 elementss[i]=e;
			 i++;
		 }
		 if(elementss.length!=0) {
		 messages.add(new Message().setAttachment(new Attachment().setType("template").setPayload(
				  new Payload().setTemplateType("generic").setElements(elementss))));
		 }
		return messages;
	}
	
	 //滚动图数据拼装
	  public static Message SwipeQicture() {
		  Message message=new Message();
		  Element[] element=new Element[] {
				  new Element().setTitle("test1").setImageUrl("https://plugins.compzets.com/images/as-logo.png")
				  .setSubtitle("We have the right hat for everyone 11111")
				  .setDefaultAction(new Button().setType("web_url").setUrl("https://plugins.compzets.com").setMessengerExtensions(false).setWebviewHeightRatio("tall")),
				  new Element().setTitle("test2").setImageUrl("https://plugins.compzets.com/contentshare/img/cs-logo-64.png")
				  .setSubtitle("We have the right hat for everyone 22222")
				  .setDefaultAction(new Button().setType("web_url").setUrl("https://plugins.compzets.com").setMessengerExtensions(false).setWebviewHeightRatio("tall")),
				  new Element().setTitle("test3").setImageUrl("https://plugins.compzets.com/images/fs-logo.png")
				  .setSubtitle("We have the right hat for everyone 33333")
				  .setDefaultAction(new Button().setType("web_url").setUrl("https://plugins.compzets.com").setMessengerExtensions(false).setWebviewHeightRatio("tall")),
		  };
		  message.setAttachment(new Attachment().setType("template").setPayload(
				  new Payload().setTemplateType("generic").setElements(element)));
		  
		  return message;
	  }

	/**
	 * 把答案里面的[link submit="link test"]【1】link test[/link]转成Button按钮
	 * 
	 * @param text
	 * @return
	 */
	public static List<Message> getButton(List<Message> messages,String text) {
		
		// [link submit="link test"]【1】link test[/link]
//        String srcRegex ="\\[link\\s+submit=[\'\"]+([^\\[\\]\'\"]+)[\'\"]+\\s*[^\\[\\]]*\\]([^\\[\\]]+)\\[\\/link\\]";
//        text=text.replaceAll(srcRegex,"");
		
		Pattern pattern = Pattern.compile(srcRegex);
		Matcher matcher = pattern.matcher(text);
		int i = 0;
		int tag=0;
		List<Button> buList=new ArrayList<Button>();
		System.out.println(buList.size());
		while (matcher.find()) {
			tag=1;
			if (matcher.groupCount() == 2) {
				Button button=new Button().setType("postback").setPayload(matcher.group( 1 )).setTitle(matcher.group( 2 ));
				buList.add(button);
				i++;
				if(i==3) {
					messages=addButton(messages, buList,text);
					i=0;
					buList=new ArrayList<Button>();
				}
			}
		}
		if(tag!=0) {//有按钮
			if(buList.size()>0) {
				messages=addButton(messages, buList,text);
			}
		}else {//没有按钮，直接取text
			if(text.length()>=640) {
				messages=textSplit(text, messages);
			}else {
				messages.add(new Message().setType("text").setText(text));
			}
		}
		
		/*
		 * if(i<=0) {//如果没有菜单直接返回文本 messages.add(new
		 * Message().setType("text").setText(text)); }else {
		 * //判断按钮是否大于3个，大于三个菜单缓存大于三个的按钮作为第二次发送 Button[] buttons = new Button[3]; i = 0;
		 * int tag=1; pattern = Pattern.compile(srcRegex); matcher =
		 * pattern.matcher(text); while (matcher.find()) { if (matcher.groupCount() ==
		 * 2) { Button button=new Button().setType("postback").setPayload(matcher.group(
		 * 1 )).setTitle(matcher.group( 2 )); buttons[i] = button; i++; if(i==3) {
		 * if(tag==1) {//如果tag==1，说明是第一条，把答案组装进去 messages.add(new
		 * Message().setAttachment(new Attachment().setType("template").setPayload(new
		 * Payload() .setTemplateType("button").setText(text.replaceAll(srcRegex,
		 * "")).setButtons(buttons)))); buttons = new Button[3]; tag=0; }else {
		 * messages.add(new Message().setAttachment(new
		 * Attachment().setType("template").setPayload(new Payload()
		 * .setTemplateType("button").setButtons(buttons)))); } i=0; } } }
		 * text=text.replaceAll(srcRegex, "").replaceAll("\\r\\n", "");;
		 * if(tag==1&&text!=null&&!"".equals(text)) {//如果tag==1，说明是第一条，把答案组装进去
		 * messages.add(new Message().setAttachment(new
		 * Attachment().setType("template").setPayload(new Payload()
		 * .setTemplateType("button").setText(text.replaceAll(srcRegex,
		 * "")).setButtons(buttons)))); }else { messages.add(new
		 * Message().setAttachment(new Attachment().setType("template").setPayload(new
		 * Payload()
		 * .setTemplateType("button").setText("These are "+i+" link buttons.").
		 * setButtons(buttons)))); } }
		 */
		return messages;
	}
	
	/**
	 * 判断文本长度是否大于640，是的话截取到下一条
	 * @param text
	 * @param messages
	 * @return
	 */
	public static List<Message>  textSplit(String text,List<Message> messages) {
		if(text.length()>=640) {
			String temp=text.substring(0, 630);
			int last=temp.lastIndexOf(" ");
			messages.add(new Message().setType("text").setText(text.substring(0, last)));
			textSplit(text.substring(last,text.length()), messages);
		}else {
			messages.add(new Message().setType("text").setText(text.substring(0,text.length())));
		}
		return messages;
	}
	/**
	 * 遍历集合的按钮放入数组
	 * @param messages
	 * @param buList
	 * @param text
	 * @return
	 */
	public static List<Message>  addButton(List<Message> messages,List<Button> buList,String text) {
		Button[] buttons = new Button[buList.size()];
		int x=0;
		for(Button bu:buList) {
			buttons[x]=bu;
			x++;
		}
		text=text.replaceAll(srcRegex, "").replaceAll("\\r\\n", "");
		if(text==null||text.equals("")) {
			text="These are "+buList.size()+" link buttons.";
		}else if(text.length()>=640) {
			messages=textSplit(text, messages);
			text="These are "+buList.size()+" link buttons.";
		}
		messages.add(new Message().setAttachment(new Attachment().setType("template").setPayload(new Payload()
				.setTemplateType("button").setText(text).setButtons(buttons))));
		return messages;
	}

	/**
	 * 判断图片url是否存在， 不存在就调用上传方法，并且返回ID 存在就直接获取id并且返回
	 * 
	 * @param url
	 * @return 返回素材ID
	 * @throws IOException
	 */
	public static String controlImage(String url) throws IOException {

		Properties prop = new Properties();
		// 读取属性文件a.properties
		InputStream in = null;
		String urlUp = "";
		try {
			judeFileExists(new File(imageUlr+"imageID.properties"));
			in = new BufferedInputStream(new FileInputStream(imageUlr+"imageID.properties"));
			prop.load(in); /// 加载属性列表
			if (prop.getProperty(url) != null) {
				in.close();
				return prop.getProperty(url);
			} else {
				return UploadImage(url, prop);
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {

		}
		return urlUp;

	}

	/*
	 * 上传图片并获取图片ID并把url，id做key,value写入文件 return 返回attachment_id
	 */
	public static String UploadImage(String url, Properties prop) {
		PropertiesUtils pro = new PropertiesUtils();
		JSONObject json = new JSONObject();
		Message mess = new Message().setAttachment(
				new Attachment().setType("image").setPayload(new Payload().setUrl(url).setIsReusable(true)));
		json.put("message", mess);
		String postUrl = "https://graph.facebook.com/v2.6/me/message_attachments?access_token="
				+ pro.getString("fbPageAccessToken");
		String resp = HttpUtils.doPost(postUrl, json.toString().replaceFirst("reusable", "is_reusable"));
		json = (JSONObject) JSONObject.parse(resp);
		if(json!=null) {
		/// 保存属性到b.properties文件
		FileOutputStream oFile;
		try {
			oFile = new FileOutputStream(imageUlr+"imageID.properties", true);
			prop.setProperty(url, json.get("attachment_id").toString());
			prop.store(oFile, "Comment");
			oFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return json.get("attachment_id").toString();
		}else {
			return "上传异常";
		}
	}

	/**
	 * 判断文件是否存在，不存在就创建
	 * @param file
	 */
	public static void judeFileExists(File file) {

		if (file.exists()) {
			System.out.println("file exists");
		} else {
			System.out.println("file not exists, create it ...");
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) throws IOException {
		String text="also click to browse related websites.The following quest";
		int a=text.lastIndexOf(" ");
		System.out.println(a);
				
	}
}
