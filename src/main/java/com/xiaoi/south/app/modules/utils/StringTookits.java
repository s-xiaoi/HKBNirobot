package com.xiaoi.south.app.modules.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class StringTookits{

    /**
     * @Description <b> # 使用默认值 </b>
     * @param value
     * @param _defaultValue
     * @return String
     * @since 1.0
     */
    public static String getString( String value, String _defaultValue ){
        if( StringUtils.isEmpty( value ) ){
            value = _defaultValue;
        }
        return value;
    }

    /**
     * @Description <b> #返回Int </b>
     * @param value
     * @return Integer
     * @since 1.0
     */
    public static int getInteger( String value ){
        Integer num = 0;
        if( StringUtils.isAlphanumeric( value ) ){
            num = Integer.parseInt( value );
        }
        return num;
    }

    /**
     * @Description <b>(将数字转换成字母)</b></br>
     * @param value
     * @return String
     * @since 2016-9-20
     */
    public static String ascii2String( String value ){
        StringBuffer sbu = new StringBuffer();
        String[] chars = value.split( "," );
        for( int i = 0; i < chars.length; i++ ){
            sbu.append( (char) Integer.parseInt( chars[i] ) );
        }
        return sbu.toString();
    }

    /**
     * @Description <b>(字符串转换成布尔值)</b></br> 字符串"true"和"1",为true，其它情况都为false
     * @param key
     * @return boolean
     * @since 2016-9-20
     */
    public static boolean parseBoolean( String value ){
        boolean yes = false;
        if( StringUtils.equals( "1", value ) ){
            yes = true;
        }else{
            yes = Boolean.parseBoolean( value );
        }
        return yes;
    }

    /**
     * <p>
     * 当参数值设置为数字时,返回数值数 当传入参数为空是，返回默认值
     * </p>
     * 
     * @param s
     * @param d
     * @return int
     * @since 2016-9-20
     */
    public static int parseInt( String s, int d ){
        int i = -1;
        if( s != null ){
            i = Integer.parseInt( s );
        }else{
            i = d;
        }
        return i;
    }

    /**
     * @Description <b>(SHA1加密)</b></br>
     * @param inStr
     * @return String
     * @since 2016-2-23
     */
    public static String sha1( String inStr ){
        MessageDigest md = null;
        String outStr = null;
        try{
            md = MessageDigest.getInstance( "SHA-1" ); // 选择SHA-1，也可以选择MD5
            byte[] digest = md.digest( inStr.getBytes() ); // 返回的是byet[]，要转化为String存储比较方便
            outStr = byte2String( digest );
        }catch( NoSuchAlgorithmException e ){
            e.printStackTrace();
        }
        return outStr;
    }

    /**
     * @Description <b>(MD5加密)</b></br>
     * @param inStr
     * @return String
     * @since 2016-2-23
     */
    public static String md5( String inStr ){
        MessageDigest md = null;
        String outStr = null;
        try{
            md = MessageDigest.getInstance( "MD5" ); // MD5
            byte[] digest = md.digest( inStr.getBytes() ); // 返回的是byet[]，要转化为String存储比较方便
            outStr = byte2String( digest );
        }catch( NoSuchAlgorithmException e ){
            e.printStackTrace();
        }
        return outStr;
    }

    /**
     * MurMurHash算法，是非加密HASH算法，性能很高，
     * 比传统的CRC32,MD5，SHA-1（这两个算法都是加密HASH算法，复杂度本身就很高，带来的性能上的损害也不可避免）
     * 等HASH算法要快很多，而且据说这个算法的碰撞率很低. http://murmurhash.googlepages.com/
     */
    public static Long murHash( String key ){
        ByteBuffer buf = ByteBuffer.wrap( key.getBytes() );
        int seed = 0x1234ABCD;
        ByteOrder byteOrder = buf.order();
        buf.order( ByteOrder.LITTLE_ENDIAN );

        long m = 0xc6a4a7935bd1e995L;
        int r = 47;
        long h = seed ^ (buf.remaining() * m);
        long k;
        while( buf.remaining() >= 8 ){
            k = buf.getLong();
            k *= m;
            k ^= k >>> r;
            k *= m;
            h ^= k;
            h *= m;
        }

        if( buf.remaining() > 0 ){
            ByteBuffer finish = ByteBuffer.allocate( 8 ).order( ByteOrder.LITTLE_ENDIAN );
            // for big-endian version, do this first:
            // finish.position(8-buf.remaining());
            finish.put( buf ).rewind();
            h ^= finish.getLong();
            h *= m;
        }

        h ^= h >>> r;
        h *= m;
        h ^= h >>> r;
        buf.order( byteOrder );
        return h;
    }

    /**
     * <pre>
     * <b>数组产生对应的hash key</b>
     * </pre>
     * 
     * @param params
     * @return String
     * @since 1.0
     */
    public static String hashKey( String... params ){
        List<String> list = new ArrayList<String>();
        for( String p : params ){
            if( p != null ){
                list.add( p );
            }
        }
        String[] keys = new String[] {};
        params = list.toArray( keys );

        // 按字典顺序排序
        Arrays.sort( params );
        // 生成字符串
        StringBuffer content = new StringBuffer();
        for( int i = 0; i < params.length; i++ ){
            content.append( params[i] );
        }
        String src = content.toString();
        // String hash = md5( src );
        String hash = String.valueOf( murHash( src ) );
        return hash;
    }

    /**
     * @Description <b>(byte数组转字符串)</b></br>
     * @param digest
     * @return String
     * @since 2016-3-6
     */
    public static String byte2String( byte[] digest ){
        String str = "";
        String tempStr = "";
        for( int i = 1; i < digest.length; i++ ){
            tempStr = (Integer.toHexString( digest[i] & 0xff ));
            if( tempStr.length() == 1 ){
                str = str + "0" + tempStr;
            }else{
                str = str + tempStr;
            }
        }
        return str.toLowerCase();
    }

}
