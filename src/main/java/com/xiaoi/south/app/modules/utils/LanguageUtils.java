/**    
 * 文件名：LanguageUtils.java    
 *
 * 版本信息：    
 * 日期：2017-12-19    
 * create by ziQi
 */
package com.xiaoi.south.app.modules.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import com.spreada.utils.chinese.ZHConverter;
import com.xiaoi.south.app.entity.AskLogBean;
import org.apache.poi.util.SystemOutLogger;

/**
 * @title LanguageUtils
 * @description TODO
 * @author ziQi
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class LanguageUtils{

    /**
     * @Description
     * <b>
     * #判斷是否包含中文字符
     * </b>
     * @param str
     * @return 
     * boolean 
     * @since 1.0    
    */
    public static boolean hasChinese( String str ){
        String regEx = "[\u4e00-\u9fa5]";
        Pattern pat = Pattern.compile( regEx );
        Matcher matcher = pat.matcher( str );
        boolean flg = false;
        if( matcher.find() ){
            flg = true;
        }
        return flg;
    }
    
    /**
     * 	判断是否全部为中文
     * 不包含：0-9
     * 不包含：“「」,，／（）.”八种特殊字符
     * @param str
     * @return
     */
    public static boolean isChinese( String str ){
        String regEx = "^[\u4e00-\u9fa5「」,，／（）.0-9]*$";
//        String regEx = "^[\u4e00-\u9fa5]*$";
        Pattern pat = Pattern.compile( regEx );
        Matcher matcher = pat.matcher( str.replaceAll("\\s", "") );
        boolean flg = false;
        if( matcher.find()){
            flg = true;
        }
        
        return flg;
    }
    
    /**
     * @Description
     * <b>
     * #判斷是否包含英文
     * </b>
     * @param str
     * @return 
     * boolean 
     * @since 1.0    
    */
    public static boolean hasEnglish( String str ){
        String regEx = "[A-Za-z]";
        Pattern pat = Pattern.compile( regEx );
        Matcher matcher = pat.matcher( str );
        boolean flg = false;
        if( matcher.find() ){
            flg = true;
        }
        return flg;
    }
    
    /**
     * 	判断是否全部为英文
     * 不包含：0-9
     * 不包含：“',，?()-.” 八种特殊符号
     * @param str
     * @return
     */
    public static boolean isEnglish( String str ){
        String regEx = "^[A-Za-z0-9',，?()-.]*$";
//        String regEx = "^[A-Za-z]*$";
        Pattern pat = Pattern.compile( regEx );
        Matcher matcher = pat.matcher( str.replaceAll("\\s", "") );
        boolean flg = false;
        if( matcher.find()){
            flg = true;
        }
        
        return flg;
    }
    
    /**
     * 判断是否中文和英文
     * @param str
     * @return
     */
    public static boolean isChineseAndEnglish( String str ){
        String regEx = "^[A-Za-z\u4e00-\u9fa5]*$";
        Pattern pat = Pattern.compile( regEx );
        Matcher matcher = pat.matcher( str.replaceAll("\\s", "") );
        boolean flg = false;
        if( matcher.find() ){
            flg = true;
        }
        return flg;
    }
    
    /**
     * @Description
     * <b>
     * #简体中文转换成繁体
     * </b>
     * @param str
     * @return 
     * boolean 
     * @since 1.0    
    */
    public static String sc2TC( String str ){
        ZHConverter converter = ZHConverter.getInstance(ZHConverter.TRADITIONAL);
        String tw = converter.convert( str );
        return tw;
    }
    
    /**
     * @Description
     * <b>
     * #繁体转换成简体中文
     * </b>
     * @param str
     * @return 
     * boolean 
     * @since 1.0    
    */
    public static String tc2SC( String str ){
        ZHConverter converter = ZHConverter.getInstance(ZHConverter.SIMPLIFIED);
        String tw = converter.convert( str );
        return tw;
    }
    
    public static void main(String[] args) {
        List<AskLogBean> lists=new ArrayList<AskLogBean>();
        AskLogBean obj=new AskLogBean();
        obj.setQuestion("system_welcome");
        lists.add(obj);
        obj=new AskLogBean();
        obj.setQuestion("你好");
        lists.add(obj);
        obj=new AskLogBean();
        obj.setQuestion("我有一个问题");
        lists.add(obj);
        int i=0;
        boolean hasEnglish=false;
        boolean hasChinese=false;
        if(lists.size()>=4) {
            i=4;
        }else{
            i=lists.size();
        }
        for(int j=1;j<i;j++) {
            AskLogBean bean=lists.get(j);
          /*  if (LanguageUtils.isEnglish(bean.getQuestion())) {//是否为全英文
                isEnglish = true;
            }
            if (LanguageUtils.isChinese(bean.getQuestion())) {//是否为全中文
                isChinese = true;
            }*/
            if (LanguageUtils.hasEnglish(bean.getQuestion())) {//是否包含英文
                hasEnglish = true;
            }
            if (LanguageUtils.hasChinese(bean.getQuestion())) {//是否包含英文
                hasChinese = true;
            }
        }
        if(hasEnglish&&hasChinese){
            System.out.println("CN_EN");
        }else if(hasEnglish){
            System.out.println("EN");
        }else{
            System.out.println("CN");
        }

	}

}
