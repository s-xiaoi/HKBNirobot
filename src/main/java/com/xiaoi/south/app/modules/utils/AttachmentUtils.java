package com.xiaoi.south.app.modules.utils;

import java.text.MessageFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.ParseException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.eastrobot.robotface.domain.RobotCommand;
import com.xiaoi.south.app.entity.RobotRequest;
import com.xiaoi.south.app.entity.RobotResponse;

/**
 * <pre>
 * 处理各种答案，返回自定义的类型( 不建议在这里改动了，需要的话，自己再做一个工具类进行相应的处理 )
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class AttachmentUtils{
    /**
     * @Description <b>(自定义答案类型消息)</b></br>
     * @param robotResponse
     * @return RobotResponse
     * @since 2016年2月19日
     */
    public static  RobotResponse rebuild( RobotRequest robotRequest, RobotResponse robotResponse, String baseUrl ){
        RobotCommand[] commands = robotResponse.getCommands();

        // 默认为空
        robotResponse.setAttachment( new HashMap<String, String>() );
        if( commands != null ){
            for( int i = 0; i < commands.length; i++ ){
                RobotCommand command = commands[i];
                if( "imgtxtmsg".equals( command.getName() ) ){// 图文消息
                    // 图文type=201
                    robotResponse.setType( 201 );
                    String commandArgs[] = command.getArgs();
                    if( commandArgs != null && commandArgs.length >= 4 ){
                        robotResponse.setAttachment( getImgtxtmsg( commandArgs[3], robotRequest, baseUrl ) );
                    }
                }else if( "imgmsg".equals( command.getName() ) ){// 图片消息
                    // 图片type=202
                    robotResponse.setType( 202 );
                    String commandArgs[] = command.getArgs();
                    String resouceUrl = baseUrl;
                    if( commandArgs.length >= 3 ){
                        Map<String, String> map = new HashMap<String, String>();
                        map.put( "name", "imgmsg" );// 图片标识
                        map.put( "title", commandArgs[0] );// 标题
                        map.put( "description", commandArgs[1] );// 描述
                        String url = commandArgs[2];
                        if( !StringUtils.startsWith( commandArgs[2], "http" ) ){
                            url = resouceUrl + commandArgs[2];
                        }
                        map.put( "url", url );// 图片资源地址
                        robotResponse.setAttachment( map );
                    }
                }else if( "musicmsg".equals( command.getName() ) ){// 音频消息
                    // 音频type=203
                    robotResponse.setType( 203 );
                    String commandArgs[] = command.getArgs();
                    String resouceUrl = baseUrl;
                    if( commandArgs.length > 3 ){
                        Map<String, String> map = new HashMap<String, String>();
                        map.put( "name", "musicmsg" );// 标识
                        map.put( "title", commandArgs[0] );// 标题
                        map.put( "description", commandArgs[1] );// 描述
                        String url = commandArgs[2];// 音频资源地址
                        String imageUrl = commandArgs[3];// 缩略图地址
                        if( !StringUtils.startsWith( url, "http" ) ){
                            url = resouceUrl + commandArgs[2];
                        }
                        if( !StringUtils.startsWith( imageUrl, "http" ) ){
                            imageUrl = resouceUrl + commandArgs[3];
                        }
                        map.put( "imageUrl", imageUrl );// 缩略图地址
                        map.put( "url", url );// 音频资源地址
                        robotResponse.setAttachment( map );
                    }
                }else if( "videomsg".equals( command.getName() ) ){// 视频消息
                    // 视频type=204
                    robotResponse.setType( 204 );
                    String commandArgs[] = command.getArgs();
                    String resouceUrl = baseUrl;
                    if( commandArgs.length > 3 ){
                        Map<String, String> map = new HashMap<String, String>();
                        map.put( "name", "videomsg" );// 标识
                        map.put( "title", commandArgs[0] );// 标题
                        map.put( "description", commandArgs[1] );// 描述
                        String url = commandArgs[2];// 视频资源地址
                        String imageUrl = commandArgs[3];// 缩略图地址
                        if( !StringUtils.startsWith( url, "http" ) ){
                            url = resouceUrl + commandArgs[2];
                        }
                        if( !StringUtils.startsWith( imageUrl, "http" ) ){
                            imageUrl = resouceUrl + commandArgs[3];
                        }
                        map.put( "url", url );// 视频资源地址
                        map.put( "imageUrl", imageUrl );// 缩略图地址
                        robotResponse.setAttachment( map );
                    }
                }else if ("p4".equals(command.getName())) {// p4消息
                    robotResponse.setType(200);
                    String commandArgs[] = command.getArgs();
                    if (commandArgs.length >= 2) {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("name", "p4");// p4标识
                        map.put("url", commandArgs[0]);// p4路径
                        map.put("title", commandArgs[1]);// p4标题
                        robotResponse.setAttachment(map);
                    }
                }else if ("secondQuestion".equals(command.getName())) {// 一问二答 type=401
                    //robotResponse.setType( 401 );
                    String commandArgs[] = command.getArgs();
                    if (commandArgs.length >= 1) {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("name", "secondQuestion");//
                        map.put("secondQuestion", commandArgs[0]);// 第二个问题
                        //robotResponse.setAttachment(map);
                        robotResponse.setCommandObject( map );
                    }
                }
            }
        }

        return robotResponse;
    }
  
    /**
     * <pre>
     * <b>自定义图文描述格式</b>
     * </pre>
     * 
     * @param str
     * @param robotRequest
     * @return List<Map<String,String>>
     * @since 1.0
     */
    public static List<Map<String, String>> getImgtxtmsg( String str, RobotRequest robotRequest, String baseUrl ){
        // userId,platform,location
        Object[] params = new Object[] { robotRequest.getUserId(), robotRequest.getPlatform(), robotRequest.getLocation() };
        String tmp = null;
        List<Map<String, String>> txImgs = new ArrayList<Map<String, String>>();
        try{
            // tmp
            if( str != null ){
                tmp = str.trim();
                if( tmp.startsWith( "<![CDATA[" ) ){
                    int end = tmp.lastIndexOf( "]]>" );
                    tmp = tmp.substring( 9, end );
                }
            }

            Document doc = null;
            // 将字符串转为XML
            doc = DocumentHelper.parseText( tmp );
            // 获取根节点
            Element root = doc.getRootElement();
            List itemList = root.elements( "item" );
            if( itemList != null ){
                int size = itemList.size();
                for( int i = 0; i < size; i++ ){
                    Element item = (Element) itemList.get( i );
                    Map<String, String> map = new HashMap<String, String>();
                    // 标题Title
                    Element title = item.element( "Title" );
                    if( title != null ){
                        map.put( "title", title.getTextTrim() );
                    }

                    // 描述Description
                    Element description = item.element( "Description" );
                    if( description != null ){
                        map.put( "description", description.getText() );
                    }
                    // 图片PicUrl
                    Element image = item.element( "PicUrl" );
                    String domainUrl = baseUrl;
                    if( image != null ){
                        String imageUrl = image.getTextTrim();
                        if( StringUtils.isNotEmpty( imageUrl ) ){
                            if( !StringUtils.startsWith( imageUrl, "http" ) ){
                                if( StringUtils.isNotEmpty( domainUrl ) ){
                                    imageUrl = domainUrl + imageUrl;
                                }
                            }else{
                                if( StringUtils.isNotEmpty( domainUrl ) ){
                                    if( StringUtils.equals( domainUrl, imageUrl ) ){
                                        imageUrl = "";
                                    }
                                }
                            }
                        }
                        map.put( "image", imageUrl );
                        map.put( "PicUrl", imageUrl );
                    }
                    // 正文地址
                    Element url = item.element( "Url" );
                    if( url != null ){
                        String urlStr = url.getTextTrim();
                        if( !StringUtils.startsWith( urlStr, "http" ) ){
                            if( StringUtils.isNotEmpty( domainUrl ) ){
                                urlStr = domainUrl + urlStr;
                            }
                        }
                        map.put( "url", MessageFormat.format( urlStr, params ) );
                    }
                    txImgs.add( map );
                }
            }
        }catch( DocumentException e ){
            e.printStackTrace();
        }
        return txImgs;
    }
    public static String strToOutlook(String  time) {
    	if (time!=null) {
			if (time.length() == 6) {
				time = AttachmentUtils.strToDateMonth(time);
			}else  if (time.length() == 8) {
				time = AttachmentUtils.strToDate(time);
			} else if (time.length() == 10) {
				try {
					time = AttachmentUtils.strToDateLong(time + "0000");
				} catch (java.text.ParseException e) {
					e.printStackTrace();
				}
			} else if (time.length() == 12) {
				try {
					time = strToDateLong(time + "00");
				} catch (java.text.ParseException e) {
					e.printStackTrace();
				}
			}
		}
    	return time;
    }
    
    /**
     * 把yyyyMMddHHmmss转为yyyy-MM-dd HH:mm:ss
     * @param strDate
     * @return
     * @throws java.text.ParseException
     */
    public static String strToDateLong(String strDate) throws java.text.ParseException {
    	SimpleDateFormat oldFormat  = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat newFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			date = oldFormat.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newFormat.format(date);
   }
    /**
     * 把yyyyMMdd转为yyyy-MM-dd
     * @param strDate
     * @return
     */
   public static String strToDate(String strDate) {
	   SimpleDateFormat oldFormat  = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat newFormat  = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = oldFormat.parse(strDate);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return newFormat.format(date);
   }
   
   /**
    * 把yyyyMMdd转为yyyy-MM-dd
    * @param strDate
    * @return
    */
  public static String strToDateMonth(String strDate) {
	   SimpleDateFormat oldFormat  = new SimpleDateFormat("yyyyMM");
		SimpleDateFormat newFormat  = new SimpleDateFormat("yyyy-MM");
		Date date = new Date();
		try {
			date = oldFormat.parse(strDate);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return newFormat.format(date);
  }

  }
