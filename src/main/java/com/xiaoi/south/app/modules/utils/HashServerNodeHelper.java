package com.xiaoi.south.app.modules.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;

import com.xiaoi.south.app.service.impl.DefaultRobotServiceProxy;


public class HashServerNodeHelper{

    // urls待添加入Hash环的服务器列表
    private String[] urls;

    // 真实结点列表,考虑到服务器上线、下线的场景，即添加、删除的场景会比较频繁，这里使用LinkedList会更好
    private List<String> realNodes = new LinkedList<String>();

    // 虚拟节点，key表示虚拟节点的hash值，value表示虚拟节点的名称
    private SortedMap<Long, String> virtualNodes = new TreeMap<Long, String>();

    // 节点最后正常相应的时间戳
    private Map<String, Long> lastResTimestamp = new HashMap<String, Long>();

    // 已经删除节点
    private Map<String, String> removedNodes = new HashMap<String, String>();

    // 虚拟节点的数目
    private final int VIRTUAL_NODES = 4;

    // 定时检查工具
    private ScheduledExecutorService executor;

    // 心跳任务执行间隔： 180秒
    private long heartbeatRate = 120;

    /**
     * 传入地址数组实例化
     * 
     * @param urls
     */
    public HashServerNodeHelper( String[] urls ){

        this.urls = urls;

        // 先把原始node添加到真实结点列表中
        for( int i = 0; i < urls.length; i++ ){
            realNodes.add( urls[i] );
        }

        // 添加虚拟节点
        for( String str : realNodes ){
            this.addNode( str );
        }

        executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate( new Runnable(){
            @Override
            public void run(){// 心跳检查，不建议使用
                // heartbeat();
            }
        }, 30, heartbeatRate, TimeUnit.SECONDS );// 延迟30秒执行，两个任务间隔120秒
    }

    /**
     * @param urlStrs 传入urlStrs
     */
    public static HashServerNodeHelper getInstanse( String urlStrs ){
        String[] urls = urlStrs.split( "," );
        return (new HashServerNodeHelper( urls ));
    }

    /**
     * @param urlStrs 传入urls
     */
    public static HashServerNodeHelper getInstanse( String[] urls ){
        return (new HashServerNodeHelper( urls ));
    }

    /**
     * @param urlStrs 传入urlList
     */
    public static HashServerNodeHelper getInstanse( List<String> urlList ){
        String[] urls = new String[] {};
        urls = urlList.toArray( urls );
        return (new HashServerNodeHelper( urls ));
    }

    /**
     * @Description #增加节点
     * @param node
     * @since 1.0
     */
    public void addNode( String node ){
        for( int i = 0; i < VIRTUAL_NODES; i++ ){
            String virtualNodeName = node + "##VN-" + String.valueOf( i );
            Long hash = murHash( virtualNodeName );
            // System.out.println("虚拟节点[" + virtualNodeName + "]被添加, hash值为"
            // + hash);
            virtualNodes.put( hash, virtualNodeName );
        }
    }

    /**
     * @Description #删除节点
     * @param node
     * @since 1.0
     */
    public void remove( String node ){
        for( int i = 0; i < VIRTUAL_NODES; i++ ){
            String virtualNodeName = node + "##VN-" + String.valueOf( i );
            Long hash = murHash( virtualNodeName );
            virtualNodes.remove( hash );
        }
    }

    /**
     * @Description # 得到应当路由到的结点
     * @param node
     * @since 1.0
     */
    public String getNode( String key ){
        // 得到该key的hash值
        Long hash = murHash( key );
        // 得到大于该Hash值的所有Map
        SortedMap<Long, String> subMap = virtualNodes.tailMap( hash );
        String virtualNode;
        if( subMap.isEmpty() ){
            // 如果没有比该key的hash值大的，则从第一个node开始
            Long i = virtualNodes.firstKey();
            // 返回对应的服务器
            virtualNode = virtualNodes.get( i );
        }else{
            // 第一个Key就是顺时针过去离node最近的那个结点
            Long i = subMap.firstKey();
            // 返回对应的node
            virtualNode = subMap.get( i );
        }
        // virtualNode虚拟节点名称要截取一下
        if( virtualNode != null ){
            return virtualNode.substring( 0, virtualNode.indexOf( "##" ) );
        }
        return null;
    }

    /**
     * @Description # 心跳检查(不建议开启使用)
     * @param nodeUrl
     * @return boolean
     * @since 1.0
     */
    public void heartbeat(){
        // 检查所有节点，更新响应时间
        for( String nodeUrl : realNodes ){
            boolean live = false;

            try{
                DefaultRobotServiceProxy proxy = new DefaultRobotServiceProxy();
                live = proxy.check( nodeUrl );
            }catch( Exception e ){
                e.printStackTrace();
                live = false;
            }

            // 服务响应正常
            if( live == true ){
                lastResTimestamp.put( nodeUrl, System.currentTimeMillis() );
                // 重新添加节点()
                String x = this.removedNodes.get( nodeUrl );
                if( StringUtils.isEmpty( x ) ){
                    this.addNode( nodeUrl );
                }
            }
        }

        // 检查所有节点：当前时间-最后响应时间 > 心跳任务间隔时间3倍，可以认为节点有问题，删除
        for( String nodeUrl : realNodes ){
            long last = 0l;
            if( this.lastResTimestamp.get( nodeUrl ) != null ){
                last = this.lastResTimestamp.get( nodeUrl );
                if( System.currentTimeMillis() - last > heartbeatRate * 2 ){
                    this.removedNodes.put( nodeUrl, nodeUrl );
                    this.remove( nodeUrl );
                }
            }
        }

    }

    /**
     * @Description # 获取当前所有的虚拟节点
     * @param node
     * @since 1.0
     */
    public SortedMap<Long, String> getVirtualNodes(){
        return this.virtualNodes;
    }

    /**
     * MurMurHash算法，是非加密HASH算法，性能很高，
     * 比传统的CRC32,MD5，SHA-1（这两个算法都是加密HASH算法，复杂度本身就很高，带来的性能上的损害也不可避免）
     * 等HASH算法要快很多，而且据说这个算法的碰撞率很低. http://murmurhash.googlepages.com/
     */
    public Long murHash( String key ){
        ByteBuffer buf = ByteBuffer.wrap( key.getBytes() );
        int seed = 0x1234ABCD;
        ByteOrder byteOrder = buf.order();
        buf.order( ByteOrder.LITTLE_ENDIAN );

        long m = 0xc6a4a7935bd1e995L;
        int r = 47;

        long h = seed ^ (buf.remaining() * m);

        long k;
        while( buf.remaining() >= 8 ){
            k = buf.getLong();
            k *= m;
            k ^= k >>> r;
            k *= m;
            h ^= k;
            h *= m;
        }

        if( buf.remaining() > 0 ){
            ByteBuffer finish = ByteBuffer.allocate( 8 ).order( ByteOrder.LITTLE_ENDIAN );
            // for big-endian version, do this first:
            // finish.position(8-buf.remaining());
            finish.put( buf ).rewind();
            h ^= finish.getLong();
            h *= m;
        }

        h ^= h >>> r;
        h *= m;
        h ^= h >>> r;

        buf.order( byteOrder );
        return h;
    }

    public static void main( String[] args ){
        String urls = "http://alxmr.demo.xiaoi.com/robot/ws/RobotServiceEx?wsdl,http://alxmr.demo.xiaoi2.com/robot/ws/RobotServiceEx?wsdl,http://alxmr.demo.xiaoi3.com/robot/ws/RobotServiceEx?wsdl";
        HashServerNodeHelper helper = new HashServerNodeHelper( urls.split( urls ) );

        String[] keys = { "5", "2", "a" };
        for( int i = 0; i < keys.length; i++ ){
            System.out.println( "[" + keys[i] + "]的hash值为" + helper.murHash( keys[i] ) + ", 被路由到结点[" + helper.getNode( keys[i] ) + "]" );
        }

        helper.remove( "http://alxmr.demo.xiaoi.com/robot/ws/RobotServiceEx?wsdl" );

        System.out.println( helper.getVirtualNodes() );
    }

}
