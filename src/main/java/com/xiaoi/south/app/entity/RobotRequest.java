/**    
 * 文件名：RobotRequest.java    
 *
 * 版本信息：    
 * 日期：2016年1月30日    
 * create by ziQi       
 * 2016年1月30日
 */
package com.xiaoi.south.app.entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @title RobotRequest
 * @description 二开定义请求对象
 * @author ziQi
 * @version 上午10:40:00
 * @create_date 2016年1月30日上午10:40:00
 * @copyright (c) jacky
 */
@XmlRootElement( name = "robotRequest" )
public class RobotRequest implements Serializable{

    private static final long serialVersionUID = -6273214084070576207L;

    private String sessionId;

    private String userId;

    private String question;

    private String[] tags;

    private String[] modules;

    private int maxReturn;

    private String attributes;

    private String platform;

    private String brand;

    private String location;

    private String custom1;

    private String custom2;

    private String custom3;
    
    private String lang;
    
    private String qlrPower;

    private String man;

    private String messageType;

    private String hkbnPlatform;//官网类型

    private String AgentLang;

    private String faqVote;//是否是解决未解决
    /**
     * format:json 或者 xml
     */
    private String format;

    public String getMan() {
        return man;
    }

    public String getAgentLang() {
        return AgentLang;
    }

    public void setAgentLang(String agentLang) {
        AgentLang = agentLang;
    }

    public String getHkbnPlatform() {
        return hkbnPlatform;
    }

    public void setHkbnPlatform(String hkbnPlatform) {
        this.hkbnPlatform = hkbnPlatform;
    }

    public String GetMan() {
        return man;
    }

    public void setMan(String man) {
        this.man = man;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getSessionId(){
        return sessionId;
    }

    public void setSessionId( String sessionId ){
        this.sessionId = sessionId;
    }

    public String getUserId(){
        return userId;
    }

    public void setUserId( String userId ){
        this.userId = userId;
    }

    public String getQuestion(){
        return question;
    }

    public void setQuestion( String question ){
        this.question = question;
    }

    public String[] getTags(){
        return tags;
    }

    public void setTags( String[] tags ){
        this.tags = tags;
    }

    public String[] getModules(){
        return modules;
    }

    public void setModules( String[] modules ){
        this.modules = modules;
    }

    public int getMaxReturn(){
        return maxReturn;
    }

    public void setMaxReturn( int maxReturn ){
        this.maxReturn = maxReturn;
    }

    public String getPlatform(){
        return platform;
    }

    public void setPlatform( String platform ){
        this.platform = platform;
    }

    public String getBrand(){
        return brand;
    }

    public void setBrand( String brand ){
        this.brand = brand;
    }

    public String getLocation(){
        return location;
    }

    public void setLocation( String location ){
        this.location = location;
    }

    public String getAttributes(){
        return attributes;
    }

    public void setAttributes( String attributes ){
        this.attributes = attributes;
    }

    public String getCustom1(){
        return custom1;
    }

    public void setCustom1( String custom1 ){
        this.custom1 = custom1;
    }

    public String getCustom2(){
        return custom2;
    }

    public void setCustom2( String custom2 ){
        this.custom2 = custom2;
    }

    public String getCustom3(){
        return custom3;
    }

    public void setCustom3( String custom3 ){
        this.custom3 = custom3;
    }

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getQlrPower() {
		return qlrPower;
	}

	public void setQlrPower(String qlrPower) {
		this.qlrPower = qlrPower;
	}

}
