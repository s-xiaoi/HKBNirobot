/**    
 * 文件名：RobotQuestion.java    
 *
 * 版本信息：    
 * 日期：2019年04月05日
 * create by zfree.zhou
 * 2019年04月05日
 */
package com.xiaoi.south.app.entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @title RobotQuestion
 * @description 二开定义请求对象
 * @author zfree.zhou
 */
@XmlRootElement( name = "RobotQuestion" )
public class RobotQuestion implements Serializable{

    private static final long serialVersionUID = -6273214084070576207L;

    private String question;

    private String qtype;

    private String input;

    private String mode;

    private String platform;

    private String brand;

    private String location;

    private String lang;

    private String top;

    /**
     * format:json 或者 xml
     */
    private String format;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQtype() {
		return qtype;
	}

	public void setQtype(String qtype) {
		this.qtype = qtype;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getTop() {
		return top;
	}

	public void setTop(String top) {
		this.top = top;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
    
    

}
