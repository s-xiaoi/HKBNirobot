/**    
 * 文件名：MessageResourceBean.java    
 *
 * 版本信息：    
 * 日期：2018-4-12    
 * create by ziQi
 */
package com.xiaoi.south.app.entity;

/**
 * @title MessageResourceBean
 * @description TODO
 * @author ziQi
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class MessageResourceBean{

    private String id;

    private String dimTageId;

    private String code;

    private String key;

    private String message;

    private String tag;

    private String name;

    public String getId(){
        return id;
    }

    public void setId( String id ){
        this.id = id;
    }

    public String getDimTageId(){
        return dimTageId;
    }

    public void setDimTageId( String dimTageId ){
        this.dimTageId = dimTageId;
    }

    public String getCode(){
        return code;
    }

    public void setCode( String code ){
        this.code = code;
    }

    public String getKey(){
        return key;
    }

    public void setKey( String key ){
        this.key = key;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage( String message ){
        this.message = message;
    }

    public String getTag(){
        return tag;
    }

    public void setTag( String tag ){
        this.tag = tag;
    }

    public String getName(){
        return name;
    }

    public void setName( String name ){
        this.name = name;
    }

}
