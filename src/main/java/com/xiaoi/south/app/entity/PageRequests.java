package com.xiaoi.south.app.entity;

import java.io.Serializable;

/** 
 * @title PageRequests
 * @description 传入的分页参数
 * @author ziQi 
 * @version 下午11:10:14 
 * @create_date 2016-9-21下午11:10:14
 * @copyright (c) jacky
 */  
public class PageRequests implements Serializable{
    private static final long serialVersionUID = -1613438268149234887L;
    //第几页
    private int pageNo; 
    //每页条数
    private int pageSize;
    
    public int getPageNo(){
        return pageNo;
    }
    public void setPageNo( int pageNo ){
        this.pageNo = pageNo;
    }
    public int getPageSize(){
        return pageSize;
    }
    public void setPageSize( int pageSize ){
        this.pageSize = pageSize;
    }
}
