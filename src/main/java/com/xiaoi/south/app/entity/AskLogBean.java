package com.xiaoi.south.app.entity;

import java.io.Serializable;

/**
 * @author brave.chen
 * @create 2019-11-17 20:08
 */
public class AskLogBean implements Serializable{

    private String UserId;//用户ID
    private String SessionId;//会话ID
    private String plstform;//维度  web app
    private int type;       //问题类型 1，标准问答  0，默认回复   5，敏感词  更多请看小i客服接口规范
    private String  businessType;  //业务类型
    private String lang;           //用户前端语言
    private String question;      //用户问题
    private String answer;         //用户答案
    private String createTime;     //写入时间
    private String startTime;      //开始时间
    private String endTime;        //结束时间
    private String hkbnPlatform;//官网类型

    public String getPlstform() {
        return plstform;
    }

    public void setPlstform(String plstform) {
        this.plstform = plstform;
    }

    public String getHkbnPlatform() {
        return hkbnPlatform;
    }

    public void setHkbnPlatform(String hkbnPlatform) {
        this.hkbnPlatform = hkbnPlatform;
    }

    @Override
    public String toString() {
        return "AskLogBean{" +
                "UserId='" + UserId + '\'' +
                ", SessionId='" + SessionId + '\'' +
                ", type=" + type +
                ", businessType='" + businessType + '\'' +
                ", lang='" + lang + '\'' +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", createTime='" + createTime + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                '}';
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getSessionId() {
        return SessionId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
