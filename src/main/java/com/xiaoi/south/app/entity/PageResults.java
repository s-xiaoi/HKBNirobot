/**    
 * 文件名：PageResults.java    
 *
 * 版本信息：    
 * 日期：2016-9-20    
 * create by ziQi       
 * 2016-9-20
 */
package com.xiaoi.south.app.entity;

import java.util.List;

import com.github.pagehelper.Page;

/**
 * @title PageResults
 * @description 分页List<T>封装类(用于做分页查询的基础类，封装了一些分页的相关属性)
 * @version 下午9:01:59
 * @create_date 2016-9-20下午9:01:59
 * @copyright (c) jacky
 */
public class PageResults<T> {
    // 当前页
    private int pageNo;

    // 每页个数
    private int pageSize;

    // 总条数
    private long totalCount;

    // 总页数
    int  pageCount;
    
    // 当前页的数量 <= pageSize，该属性来自ArrayList的size属性
    private int currentPageSize;

    // 记录
    private List<T> results;

    public PageResults(){
    }
       
    public PageResults( List<T> list ){
        if( list instanceof Page ){
            Page<T> page = (Page<T>) list;
            this.pageSize = page.getPageSize();
            this.totalCount = page.getTotal();
            this.pageCount = page.getPages();
            this.results = page;
            this.currentPageSize = page.size();
            
            this.setPageNo( page.getPageNum() );
            //计算当前总页数
            //computePageCount();
            //设置当前第几页
          
        }
    }

    public int getPageCount(){
        return pageCount;
    }

    public void setPageCount( int pageCount ){
        this.pageCount = pageCount;
    }

    public int getPageNo(){
        if( pageNo <= 0 ){
            pageNo = 1;
        }else{
            pageNo = pageNo > pageCount ? pageCount : pageNo;
        }
        return pageNo;
    }

    public void setPageNo( int pageNo ){
        
        this.pageNo = pageNo;
        
    }

    public List<T> getResults(){
        return results;
    }

    public void setResults( List<T> results ){
        this.results = results;
    }

    public int getPageSize(){
        return pageSize;
    }

    public void setPageSize( int pageSize ){
        this.pageSize = pageSize <= 0 ? 10 : pageSize;
    }

    public long getTotalCount(){
        return totalCount;
    }

    public void setTotalCount( int totalCount ){
        this.totalCount = totalCount;
    }

    public void computePageCount(){
        pageCount = (int) (totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1);
    }

    public int getCurrentPageSize(){
        return currentPageSize;
    }

    public void setCurrentPageSize( int currentPageSize ){
        this.currentPageSize = currentPageSize;
    }

    public void setTotalCount( long totalCount ){
        this.totalCount = totalCount;
    }
    
    
}
