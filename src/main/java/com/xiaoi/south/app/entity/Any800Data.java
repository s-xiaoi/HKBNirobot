package com.xiaoi.south.app.entity;

/**
 * @author brave.chen
 * @create 2019-11-15 15:25
 */
public class Any800Data {
    private String stat;
    private int startTime;
    private int endTime;

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }
}
