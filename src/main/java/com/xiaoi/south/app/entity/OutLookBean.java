package com.xiaoi.south.app.entity;

public class OutLookBean {
	private String id;
	private String starttime;
	private String endtime;
	private String subject;
	private String creattime;
	private String organisers;//组织者
	private String venue;//地点
	private String theme;//主题
	private String guest;//嘉宾
	private String hagtag;//标签
	private String link;//链接
	private String isdisplay;//0表示显示，1表示不显示
	private String isdelete; //0表示过期14天内的数据，1表示不做搜索数据即表示删除
	private String emailtype;//1 表示Training 2 表示Conference 3表示Ceremony 4表示Seminar
	
	public String getEmailtype() {
		return emailtype;
	}
	public void setEmailtype(String emailtype) {
		this.emailtype = emailtype;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCreattime() {
		return creattime;
	}
	public void setCreattime(String creattime) {
		this.creattime = creattime;
	}
	public String getOrganisers() {
		return organisers;
	}
	public void setOrganisers(String organisers) {
		this.organisers = organisers;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public String getGuest() {
		return guest;
	}
	public void setGuest(String guest) {
		this.guest = guest;
	}
	public String getHagtag() {
		return hagtag;
	}
	public void setHagtag(String hagtag) {
		this.hagtag = hagtag;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getIsdisplay() {
		return isdisplay;
	}
	public void setIsdisplay(String isdisplay) {
		this.isdisplay = isdisplay;
	}
	public String getIsdelete() {
		return isdelete;
	}
	public void setIsdelete(String isdelete) {
		this.isdelete = isdelete;
	}
	@Override
	public String toString() {
		return "OutLookBean [id=" + id + ", starttime=" + starttime + ", endtime=" + endtime + ", subject=" + subject
				+ ", creattime=" + creattime + ", organisers=" + organisers + ", venue=" + venue + ", theme=" + theme
				+ ", guest=" + guest + ", hagtag=" + hagtag + ", link=" + link + ", isdisplay=" + isdisplay
				+ ", isdelete=" + isdelete + "]";
	}
	

	
}
