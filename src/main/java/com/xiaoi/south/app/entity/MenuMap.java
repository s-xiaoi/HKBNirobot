/**    
 * 文件名：MenuMap.java    
 *
 * 版本信息：    
 * 日期：2016年2月15日    
 * create by ziQi       
 * 2016年2月15日
 */
package com.xiaoi.south.app.entity;

import java.io.Serializable;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @title MenuMap
 * @description TODO
 * @author ziQi
 * @version 下午4:16:55
 * @create_date 2016年2月15日下午4:16:55
 * @copyright (c) jacky
 */
public class MenuMap implements Serializable{
    
    private static final long serialVersionUID = -5179163625368523151L;

    // 当前序号
    private Integer lastKey = 21;

    // 序号队列
    Queue<Object> queue = new ConcurrentLinkedQueue<Object>();

    // 菜单
    ConcurrentHashMap<Integer, Menu> menuMap = new ConcurrentHashMap<Integer, Menu>();

    private int maxSize = 100;

    public Menu put( Menu menu, int maxSize ){
        /*
         * 原来的代码 if( this.lastKey == null ){ this.lastKey = new Integer(21); }
         * //序号增长 this.lastKey++; this.removePoll(); queue.add( this.lastKey );
         * //设置序号菜单 menu.setIdx( this.lastKey ); return menuMap.put(
         * this.lastKey, menu );
         */

        this.maxSize = maxSize;
        // System.out.println("maxsize=="+maxSize);
        if( maxSize == 0 ){
            maxSize = 30;
        }else{
            maxSize = maxSize;
        }

        if( this.lastKey == null ){
            this.lastKey = new Integer( 21 );
        }
        if( this.lastKey > maxSize - 1 ){
            this.lastKey = 1;
            // 序号出现聊天时
            // this.removePoll();
            // queue.add( this.lastKey );
            // 设置序号菜单
            menu.setIdx( this.lastKey );
        }else{
            // 序号增长
            this.lastKey++;
            // this.removePoll();
            // queue.add( this.lastKey );
            // 设置序号菜单
            menu.setIdx( this.lastKey );
        }

        return menuMap.put( this.lastKey, menu );

    }

    /**
     * @Description <b>(获得序号对应的菜单)</b></br>
     * @param key
     *            菜单对应序号
     * @return Menu
     * @since 2016年2月16日
     */
    public Menu get( Integer key ){
        return menuMap.get( key );
    }

    public Integer getLastKey(){
        return lastKey;
    }

    public void setLastKey( Integer lastKey ){
        this.lastKey = lastKey;
    }

    /**
     * removePoll(清理)
     * 
     * @return void
     * @since 2014-8-9 下午07:54:15
     */
    public void removePoll(){
        while( queue.size() > maxSize ){
            Object obj = queue.poll();
            menuMap.remove( obj );
        }
    }

    /**
     * @Description <b>(清理全部)</b></br>
     * @since 2016年2月16日
     */
    public void clear(){
        this.queue.clear();
        this.menuMap.clear();
        this.lastKey = 21;
    }

}
