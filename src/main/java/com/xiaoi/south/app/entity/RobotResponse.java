/**    
 * 文件名：RobotRequest.java    
 *
 * 版本信息：    
 * 日期：2016年1月30日    
 * create by ziQi       
 * 2016年1月30日
 */
package com.xiaoi.south.app.entity;

import java.io.Serializable;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import com.eastrobot.robotface.domain.RobotCommand;

/**
 * @title RobotRequest
 * @description 二开定义结果对象
 * @author ziQi
 * @version 上午10:40:00
 * @create_date 2016年1月30日上午10:40:00
 * @copyright (c) jacky
 */
@XmlRootElement( name = "robotResponse" )
public class RobotResponse implements Serializable{
    private static final long serialVersionUID = -6030246545960255119L;

    private int type = -1;

    private String content;

    private String nodeId;

    private String moduleId;

    private String[] tags;

    private float similarity;
    
    private String sessionId;

    private RobotCommand[] commands;

    private String[] relatedQuestions;

    private String format;
    
    private String month;
    
    private String LoadAmount;
    
    

    public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getLoadAmount() {
		return LoadAmount;
	}

	public void setLoadAmount(String loadAmount) {
		LoadAmount = loadAmount;
	}

	// 一般为Map或者为List<Map>
    private Object attachment;

    // 人工状态标识
    private String acsStatus;
    
    //传入的资源地址
    private String baseUrl;
    
    //自定义command处理
    private Map<String,String> commandObject;

    public String getAcsStatus(){
        return acsStatus;
    }

    public void setAcsStatus( String acsStatus ){
        this.acsStatus = acsStatus;
    }

    public int getType(){
        return type;
    }

    public void setType( int type ){
        this.type = type;
    }

    public String getContent(){
        return content;
    }

    public void setContent( String content ){
        this.content = content;
    }

    public String getNodeId(){
        return nodeId;
    }

    public void setNodeId( String nodeId ){
        this.nodeId = nodeId;
    }

    public String getModuleId(){
        return moduleId;
    }

    public void setModuleId( String moduleId ){
        this.moduleId = moduleId;
    }

    public String[] getTags(){
        return tags;
    }

    public void setTags( String[] tags ){
        this.tags = tags;
    }

    public float getSimilarity(){
        return similarity;
    }

    public void setSimilarity( float similarity ){
        this.similarity = similarity;
    }

    public RobotCommand[] getCommands(){
        return commands;
    }

    public void setCommands( RobotCommand[] commands ){
        this.commands = commands;
    }

    public String[] getRelatedQuestions(){
        return relatedQuestions;
    }

    public void setRelatedQuestions( String[] relatedQuestions ){
        this.relatedQuestions = relatedQuestions;
    }

    public Object getAttachment(){
        return attachment;
    }

    public void setAttachment( Object attachment ){
        this.attachment = attachment;
    }

    public String getFormat(){
        return format;
    }

    public void setFormat( String format ){
        this.format = format;
    }

    public String getBaseUrl(){
        return baseUrl;
    }

    public void setBaseUrl( String baseUrl ){
        this.baseUrl = baseUrl;
    }

    public Map<String, String> getCommandObject(){
        return commandObject;
    }

    public void setCommandObject( Map<String, String> commandObject ){
        this.commandObject = commandObject;
    }

    public String getSessionId(){
        return sessionId;
    }

    public void setSessionId( String sessionId ){
        this.sessionId = sessionId;
    }
}
