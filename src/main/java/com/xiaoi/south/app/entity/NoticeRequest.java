package com.xiaoi.south.app.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author brave.chen
 * @create 2019-11-11 13:59
 * 通知实体类
 */
@XmlRootElement( name = "NoticeRequest" )
public class NoticeRequest {
    private String UserId;
    private String Type;
    private String Msg;

    @Override
    public String toString() {
        return "NoticeRequest{" +
                "UserId='" + UserId + '\'' +
                ", Type='" + Type + '\'' +
                ", Msg='" + Msg + '\'' +
                '}';
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }
}
