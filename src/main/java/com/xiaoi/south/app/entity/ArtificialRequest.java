package com.xiaoi.south.app.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author brave.chen
 * @create 2019-11-05 15:42
 */
@XmlRootElement( name = "ArtificialRequest" )
public class ArtificialRequest {
    private String msg;
    private String msg_type;
    private String context_id;
    private String visitorId;
    private String chat_mode;
    private String chatStatus;//对话状态[queue(排队) ,queueEnd（排队结束）, start（对话开始）, chatting（对话中）, end（对话结束）]
    private String closeType;//关闭类型[vistor_close(访客关闭队) ,visitor_timeout（访客超时）, network_interrupt（客服网络中断）, customer_close（客服关闭）queue_timeout（排队超时close_queue（访客关闭排队）“”(空值正常对话)]
    private String sessionID;

    public String getSessionID() {
        return sessionID;
    }

    @Override
    public String toString() {
        return "ArtificialRequest{" +
                "msg='" + msg + '\'' +
                ", msg_type='" + msg_type + '\'' +
                ", context_id='" + context_id + '\'' +
                ", visitorId='" + visitorId + '\'' +
                ", chat_mode='" + chat_mode + '\'' +
                ", chatStatus='" + chatStatus + '\'' +
                ", closeType='" + closeType + '\'' +
                ", sessionID='" + sessionID + '\'' +
                '}';
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * 消息类型,text,image,voice,video,news,satisfication,others
     * @return
     */
    public String getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(String msg_type) {
        this.msg_type = msg_type;
    }

    /**
     * 9Client的用户ID
     * 通过对话初始化接口获取，必须
     * @return
     */
    public String getContext_id() {
        return context_id;
    }

    public void setContext_id(String context_id) {
        this.context_id = context_id;
    }

    public String getvisitorId() {
        return visitorId;
    }

    public void setvisitorId(String visitorId) {
        this.visitorId = visitorId;
    }

    public String getChat_mode() {
        return chat_mode;
    }

    public void setChat_mode(String chat_mode) {
        this.chat_mode = chat_mode;
    }

    /**
     * 对话模式
     * 对话状态包括(机器人，人工)：robot,operator
     * @return
     */
    public String getChatStatus() {
        return chatStatus;
    }

    public void setChatStatus(String chatStatus) {
        this.chatStatus = chatStatus;
    }

    public String getCloseType() {
        return closeType;
    }

    public void setCloseType(String closeType) {
        this.closeType = closeType;
    }
}
