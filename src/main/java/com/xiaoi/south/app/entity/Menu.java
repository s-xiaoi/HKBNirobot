/**
 * 文件名：Menu.java
 * 
 * 版本信息： 日期：2016年2月4日 create by ziQi 2016年2月4日
 */
package com.xiaoi.south.app.entity;

import java.io.Serializable;

/**
 * @title Menu
 * @description 菜单对象
 * @author ziQi
 * @version 上午1:49:36
 * @create_date 2016年2月4日上午1:49:36
 * @copyright (c) jacky
 */
public class Menu implements Serializable{
    
    private static final long serialVersionUID = 8465041601821514800L;

    // 对应序号
    private int idx;

    // 对应标准问
    private String key;

    // 对应别名
    private String alias;

    public int getIdx(){
        return idx;
    }

    public void setIdx( int idx ){
        this.idx = idx;
    }

    public String getKey(){
        return key;
    }

    public void setKey( String key ){
        this.key = key;
    }

    public String getAlias(){
        return alias;
    }

    public void setAlias( String alias ){
        this.alias = alias;
    }
}
