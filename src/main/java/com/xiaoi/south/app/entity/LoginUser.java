/**    
 * 文件名：RobotRequest.java    
 *
 * 版本信息：    
 * 日期：2016年1月30日    
 * create by ziQi       
 * 2016年1月30日
 */
package com.xiaoi.south.app.entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @title RobotRequest
 * @description 二开定义请求对象
 * @author ziQi
 * @version 上午10:40:00
 * @create_date 2016年1月30日上午10:40:00
 * @copyright (c) jacky
 */
@XmlRootElement( name = "LoginUser" )
public class LoginUser implements Serializable{

    private static final long serialVersionUID = -6273214084070576207L;

    private String userId;

    private String userPwd;

    private String lang;
    
    private String captcha;

    private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

}
