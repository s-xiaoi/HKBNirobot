package com.xiaoi.south.app.facebook;

import me.ramswaroop.jbot.core.common.Controller;
import me.ramswaroop.jbot.core.common.EventType;
import me.ramswaroop.jbot.core.common.JBot;
import me.ramswaroop.jbot.core.facebook.Bot;
import me.ramswaroop.jbot.core.facebook.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;

import com.eastrobot.robotface.domain.RobotCommand;
import com.eastrobot.robotface.domain.RobotResponseEx;
import com.xiaoi.south.app.cache.CacheSupport;
import com.xiaoi.south.app.constant.GlobalConstants;
import com.xiaoi.south.app.entity.Menu;
import com.xiaoi.south.app.entity.MenuMap;
import com.xiaoi.south.app.entity.RobotRequest;
import com.xiaoi.south.app.entity.RobotResponse;
import com.xiaoi.south.app.modules.utils.FaceBookUtils;
import com.xiaoi.south.app.modules.utils.PropertiesUtils;
import com.xiaoi.south.app.service.impl.AskServiceProxy;
import com.xiaoi.south.app.service.impl.DefaultMessageInterfaceResourceProxy;
import com.xiaoi.south.app.servlet.controller.IBotController;

/**
 * A simple Facebook Bot. You can create multiple bots by just
 * extending {@link Bot} class like this one. Though it is
 * recommended to create only bot per jbot instance.
 * 
 * @author ramswaroop
 * @version 17/09/2016
 */
/*
@JBot
//@Profile("facebook")
public class FbBot extends Bot {
	private static final Logger logger = LoggerFactory.getLogger(FbBot.class);
	 @Autowired
	 private AskServiceProxy askServiceProxy;
	 @Autowired
	 private DefaultMessageInterfaceResourceProxy defaultMessageResourceProxy;
	
	 
	 public ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
	 */
/**
	  * 普通消息
	  * @param event
	  *//*

//    @Controller(events = {EventType.MESSAGE, EventType.POSTBACK}, pattern = "^(?i)")
	@Controller(events = {EventType.MESSAGE, EventType.POSTBACK, EventType.QUICK_REPLY}, pattern = "^(?i)")
    public void All(Event event) {
    	String temp=PropertiesUtils.getString("defaultMessger");
        
    	try {
        //加入之前的二开逻辑
        RobotRequest robotRequest=new RobotRequest();
        List<Message> messages=new ArrayList<Message>();//集合多次发送
        //封装转换facebook event转robotRequest 
        robotRequest=FaceBookUtils.FaceBookEventReq(event);
        RobotResponseEx resp=this.askServiceProxy.askFbRobot(robotRequest);//请求产品
        //序号自动增长
    	//this.menu2Faq( robotRequest );
//        RobotResponse robotResponse = this.askServiceProxy.askFbRobot( robotRequest );
        //返回数据封装
        messages=this.askServiceProxy.FaceBookEventResp(robotRequest,resp);
        //返回数据拆分发送，比如文字+图片  先发送文字，再发送图片
        for(Message obj:messages) {
//        	obj=FaceBookUtils.FaceBookMessageResp(obj);
        	if(obj.getType()!=null&&obj.getType().equals("text")) {//如果答案是纯文本，直接返回答案
        		reply(event,obj.getText());
        	}else {
        		reply(event,obj);
        	}
        	
        }
        */
/**
         * 创建一个单线程化的线程池，它只会用唯一的工作线程来执行任务，
         *//*

            final RobotResponseEx resp2=resp;
            final FbBot fbbot = new FbBot();
            final Event event1=event;
            singleThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                    	RobotCommand[] command=resp2.getCommands();
                    	for(int i=0;i<command.length;i++) {
                    		if(command[i].getName()!=null&&command[i].getName().equals("towReq")) {
                    			event1.setMessage(event1.getMessage().setText(command[i].getArgs()[0]));
                    			fbbot.All(event1);
                    		}
                    	}
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });
       
    	}catch (Exception e) {
    		reply(event,"controller Error:For details, please check the log.");//处理异常提示
    		e.printStackTrace();
			// TODO: handle exception
		}
        
    }
    */
/**
     * 处理所有的QUICK_REPLY消息
     * @param event
     *//*

    @Controller(events = EventType.QUICK_REPLY, pattern = "^(?i)")
    public void AllQuickReply(Event event) {
        if ("yes".equals(event.getMessage().getQuickReply().getPayload())) {
            reply(event, "Cool! You can type: \n 1) Show Buttons \n 2) Show List \n 3) Setup meeting");
        } else {
            reply(event, "See you soon!");
        }
    }
    */
/**
     * Set this property in {@code application.properties}.
     *//*

    @Value("${fbBotToken}")
    private String fbToken;

    */
/**
     * Set this property in {@code application.properties}.
     *//*

    @Value("${fbPageAccessToken}")
    private String pageAccessToken;

    @Override
    public String getFbToken() {
        return fbToken;
    }

    @Override
    public String getPageAccessToken() {
        return pageAccessToken;
    }

    */
/**
     * Sets the "Get Started" button with a payload "hi". It also set the "Greeting Text" which the user sees when it
     * opens up the chat window. Uncomment the {@code @PostConstruct} annotation only after you have verified your 
     * webhook.
     *//*

    //@PostConstruct
    public void init() {
        setGetStartedButton("hi");
        setGreetingText(new Payload[]{new Payload().setLocale("default").setText("JBot is a Java Framework to help" +
                " developers make Facebook, Slack and Twitter bots easily. You can see a quick demo by clicking " +
                "the \"Get Started\" button.")});
    }

    
    */
/**
     * This method gets invoked when a user clicks on the "Get Started" button or just when someone simply types
     * hi, hello or hey. When it is the former, the event type is {@code EventType.POSTBACK} with the payload "hi"
     * and when latter, the event type is {@code EventType.MESSAGE}.
     *
     * @param event
     *//*

    @Controller(events = {EventType.MESSAGE, EventType.POSTBACK}, pattern = "^(?i)(hi|hello|hey)$")
    public void onGetStarted(Event event) {
        // quick reply buttons
        Button[] quickReplies = new Button[]{
                new Button().setContentType("text").setTitle("Sure").setPayload("yes"),
                new Button().setContentType("text").setTitle("Nope").setPayload("no")
        };
        reply(event, new Message().setText("Hello, I am JBot. Would you like to see more?").setQuickReplies(quickReplies));
    }

    */
/**
     * This method gets invoked when the user clicks on a quick reply button whose payload is either "yes" or "no".
     *
     * @param event
     *//*

    @Controller(events = EventType.QUICK_REPLY, pattern = "(yes|no)")
    public void onReceiveQuickReply(Event event) {
        if ("yes".equals(event.getMessage().getQuickReply().getPayload())) {
            reply(event, "Cool! You can type: \n 1) Show Buttons \n 2) Show List ");
        } else {
            reply(event, "See you soon!");
        }
    }

    */
/**
     * This method is invoked when the user types "Show Buttons" or something which has "button" in it as defined
     * in the {@code pattern}.
     *
     * @param event
     *//*

    @Controller(events = EventType.MESSAGE, pattern = "(?i:button)")
    public void showButtons(Event event) {
        Button[] buttons = new Button[]{
                new Button().setType("web_url").setUrl("http://blog.ramswaroop.me").setTitle("JBot Docs"),
                new Button().setType("web_url").setUrl("https://goo.gl/uKrJWX").setTitle("Buttom Template")
        };
        IBotController ibot=new IBotController();
        
        reply(event, new Message().setAttachment(new Attachment().setType("template").setPayload(new Payload()
                .setTemplateType("button").setText("These are 2 link buttons.").setButtons(buttons))));
    }

    */
/**
     * This method is invoked when the user types "Show List" or something which has "list" in it as defined
     * in the {@code pattern}.
     *
     * @param event
     *//*

    @Controller(events = EventType.MESSAGE, pattern = "(?i:list)")
    public void showList(Event event) {
        Element[] elements = new Element[]{
                new Element().setTitle("AnimateScroll").setSubtitle("A jQuery Plugin for Animating Scroll.")
                        .setImageUrl("https://plugins.compzets.com/images/as-logo.png")
                        .setDefaultAction(new Button().setType("web_url").setMessengerExtensions(true)
                        .setUrl("https://plugins.compzets.com/animatescroll/")),
                new Element().setTitle("Windows on Top").setSubtitle("Keeps a specific Window on Top of all others.")
                        .setImageUrl("https://plugins.compzets.com/images/compzets-logo.png")
                        .setDefaultAction(new Button().setType("web_url").setMessengerExtensions(true)
                        .setUrl("https://www.compzets.com/view-upload.php?id=702&action=view")),
                new Element().setTitle("SimpleFill").setSubtitle("Simplest form filler ever.")
                        .setImageUrl("https://plugins.compzets.com/simplefill/chrome-extension/icon-64.png")
                        .setDefaultAction(new Button().setType("web_url").setMessengerExtensions(true)
                        .setUrl("https://plugins.compzets.com/simplefill/"))
        };
        reply(event, new Message().setAttachment(new Attachment().setType("template").setPayload(new Payload()
                .setTemplateType("list").setElements(elements))));
    }

    */
/**
     * Show the github project url when the user says bye.
     *
     * @param event
     *//*

    @Controller(events = EventType.MESSAGE, pattern = "(?i)(bye|tata|ttyl|cya|see you)")
    public void showGithubLink(Event event) {
        reply(event, new Message().setAttachment(new Attachment().setType("template").setPayload(new Payload()
                .setTemplateType("button").setText("Bye. Happy coding!").setButtons(new Button[]{new Button()
                        .setType("web_url").setTitle("View code").setUrl("https://github.com/ramswaroop/jbot")}))));
    }


    // Conversation feature of JBot

    */
/**
     * Type "setup meeting" to start a conversation with the bot. Provide the name of the next method to be
     * invoked in {@code next}. This method is the starting point of the conversation (as it
     * calls {@link Bot#startConversation(Event, String)} within it. You can chain methods which will be invoked
     * one after the other leading to a conversation.
     *
     * @param event
     *//*

    @Controller(pattern = "(?i)(setup meeting)", next = "confirmTiming")
    public void setupMeeting(Event event) {
        startConversation(event, "confirmTiming");   // start conversation
        reply(event, "Cool! At what time (ex. 15:30) do you want me to set up the meeting?");
    }

    */
/**
     * This method will be invoked after {@link FbBot#setupMeeting(Event)}. You need to
     * call {@link Bot#nextConversation(Event)} to jump to the next question in the conversation.
     *
     * @param event
     *//*

    @Controller(next = "askTimeForMeeting")
    public void confirmTiming(Event event) {
        reply(event, "Your meeting is set at " + event.getMessage().getText() +
                ". Would you like to repeat it tomorrow?");
        nextConversation(event);    // jump to next question in conversation
    }	

    */
/**
     * This method will be invoked after {@link FbBot#confirmTiming(Event)}. You can
     * call {@link Bot#stopConversation(Event)} to end the conversation.
     *
     * @param event
     *//*

	*/
/*
	 * @Controller(next = "askWhetherToRepeat") public void askTimeForMeeting(Event
	 * event) { if (event.getMessage().getText().contains("yes")) { reply(event,
	 * "Okay. Would you like me to set a reminder for you?");
	 * nextConversation(event); // jump to next question in conversation } else {
	 * reply(event,
	 * "No problem. You can always schedule one with 'setup meeting' command.");
	 * stopConversation(event); // stop conversation only if user says no } }
	 *//*


    */
/**
     * This method will be invoked after {@link FbBot#askTimeForMeeting(Event)}. You can
     * call {@link Bot#stopConversation(Event)} to end the conversation.
     * defaut
     * @param event
     *//*

    */
/**
     * 序号自动增长
     * @param robotRequest
     * @return
     *//*

    public RobotRequest menu2Faq( RobotRequest robotRequest ){

        CacheSupport cacheSupport = new CacheSupport( GlobalConstants.APP_CACHE );

        String question = robotRequest.getQuestion();
        String userId = robotRequest.getUserId();
        // 序号自增长，取缓存
        String realQuestion = null;
        MenuMap menuMap = (MenuMap) cacheSupport.get( userId );
        if( menuMap != null && menuMap.getLastKey() > 0 ){
            int key = -1;
            if( StringUtils.isNumeric( question ) ){
                key = Integer.valueOf( question );
            }
            // 判断是否获取到菜单
            Menu menu = menuMap.get( key );
            if( menu != null ){
                realQuestion = menu.getKey();
                // 设置实际问题
                robotRequest.setQuestion( realQuestion );
                logger.info( "Menu number {} for {}", question, realQuestion );
            }
        }else{
            menuMap = new MenuMap();
            Integer startKey = this.defaultMessageResourceProxy.getInteger( "x.automatic.increase.menu.startKey", robotRequest.getPlatform() );
            menuMap.setLastKey( startKey );
            cacheSupport.put( userId, menuMap );
        }
        return robotRequest;
    }
}
*/
