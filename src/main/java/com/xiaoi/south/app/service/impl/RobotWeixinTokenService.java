/*
 * Copyright [2018] [5a79074276d60953a4dc2ab95d762b2f]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");you may not
 * use this file except in compliance with the License.You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.See the
 * License for the specific language governing permissions andlimitations under
 * the License.
 */
package com.xiaoi.south.app.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.eastrobot.robotface.util.StringUtils;
import com.xiaoi.south.app.modules.utils.HttpUtils;
import com.xiaoi.south.app.modules.utils.PropertiesUtils;
import com.xiaoi.south.app.service.WeixinTokenService;

/**
 * <pre>
 * 获取产品配置的微信号的token
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
@Service( "robotWeixinTokenService" )
public class RobotWeixinTokenService implements WeixinTokenService{

    /**
     * 创建一个新的实例 RobotWeixinTokenService.
     * 
     */
    public RobotWeixinTokenService(){
        // TODO Auto-generated constructor stub
    }

    /*
     * (非 Javadoc) <p>Title: getToken</p> <p>Description: </p>
     * 
     * @param appId
     * 
     * @return
     * 
     * @see
     * com.xiaoi.south.app.service.WeixinTokenService#getToken(java.lang.String)
     */
    @Override
    public String getToken( String appId ){
        String xWeixinTokenUrl = PropertiesUtils.getString( "xiaoi.irobot.weixin.token.url" );
        xWeixinTokenUrl = xWeixinTokenUrl + appId;
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put( "X-TOKEN", "true" );
        String token = HttpUtils.getContent( xWeixinTokenUrl, new HashMap<String, String>(), headerMap, "UTF-8" );
        if( StringUtils.isNotEmpty( token ) && StringUtils.length( token ) <= 512 ){
            token = StringUtils.trim( token );
        }else{
            token = null;
        }
        return token;
    }

}
