/**    
 * 文件名：RobotServiceProxy.java    
 *
 * 版本信息：    
 * 日期：2018-4-11    
 * create by ziQi
 */
package com.xiaoi.south.app.service;

import com.eastrobot.robotface.domain.RobotRequestEx;
import com.eastrobot.robotface.domain.RobotResponseEx;
import com.xiaoi.south.app.entity.RobotRequest;

/**
 * @title RobotServiceProxy
 * @description 访问产品ws接口
 * @author ziQi
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public interface RobotServiceProxy{
    
    public RobotResponseEx deliver( RobotRequest robotRequest );

    RobotResponseEx ask4Robot( String askUrl, RobotRequestEx request );

    public boolean check( String askUrl );
}
