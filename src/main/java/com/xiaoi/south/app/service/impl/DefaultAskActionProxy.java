package com.xiaoi.south.app.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eastrobot.robotface.RobotServiceEx;
import com.eastrobot.robotface.domain.RobotRequestEx;
import com.eastrobot.robotface.domain.RobotResponseEx;
import com.eastrobot.robotface.domain.UserAttribute;
import com.xiaoi.south.app.entity.RobotRequest;
import com.xiaoi.south.app.modules.utils.HashServerNodeHelper;
import com.xiaoi.south.app.modules.utils.PropertiesUtils;
import com.xiaoi.south.app.modules.utils.SpringContextUtils;

/**
 * <pre>
 * question service实现逻辑
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class DefaultAskActionProxy{

    private static final Logger logger = LoggerFactory.getLogger( DefaultAskActionProxy.class );

    private RobotServiceEx robotService;

    /**
     * hashServerNodeHelper:接口地址挑选器( 一致性hash )
     */
    private HashServerNodeHelper hashServerNodeHelper;

    public DefaultAskActionProxy(){
    }

    /**
     * <pre>
     * <b>获取proxy</b>
     * </pre>
     * 
     * @return DefaultRobotServiceProxy
     * @since 1.0
     */
    public DefaultAskActionProxy getProxy(){
        this.init();
        return this;
    }

    /**
     * <b> #初始化HashServerNodeHelper </b>
     * 
     * @since 1.0
     */
    @SuppressWarnings( "unchecked" )
    public void init(){
        if( hashServerNodeHelper == null ){
            String askUrlstr = PropertiesUtils.getString( "irobot.ask.ws.urls" );
            if( StringUtils.isNotBlank( askUrlstr ) ){// 从配置文件读
                hashServerNodeHelper = HashServerNodeHelper.getInstanse( askUrlstr );
            }else{// 从spring取
                List<String> askUrlList = (List<String>) SpringContextUtils.getBean( "irobot.ask.ws.urls" );
                hashServerNodeHelper = HashServerNodeHelper.getInstanse( askUrlList );
            }
        }
    }

    /**
     * <pre>
     * <b>初始化robotService</b>
     * </pre>
     * 
     * @param askUrl webService的wsdl地址
     * @return RobotServiceEx
     * @since 1.0
     */
    private RobotServiceEx createRobotServiceEx( String askUrl ){
        // java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");

        if( StringUtils.startsWithIgnoreCase( askUrl, "https"  ) ){
            try{
                trustAllHttpsCertificates();
                HttpsURLConnection.setDefaultHostnameVerifier( hv );// 执行方法的最前面一定要加上这两个方法的调用
            }catch( Exception e ){
                e.printStackTrace();
            }
        }

        logger.info( "requesting robot's url:{}", askUrl );
        RobotServiceEx robotServiceEx = null;
        Service svc = Service.create( new QName( "http://www.eastrobot.cn/ws/RobotServiceEx", "RobotServiceEx" ) );
        svc.addPort( new QName( "http://www.eastrobot.cn/ws/RobotServiceEx", "DefaultRobotServiceExPort" ), SOAPBinding.SOAP11HTTP_BINDING, askUrl );
        robotServiceEx = svc.getPort( new QName( "http://www.eastrobot.cn/ws/RobotServiceEx", "DefaultRobotServiceExPort" ), RobotServiceEx.class );
        Map<String, Object> requestContext = ((BindingProvider) robotServiceEx).getRequestContext();
        requestContext.put( "com.sun.xml.internal.ws.connect.timeout", 3000 ); // 连接超时
        requestContext.put( "com.sun.xml.internal.ws.request.timeout", 10000 ); // 请求超时
        requestContext.put( "thread.local.request.context", "true" );
        requestContext.put( BindingProvider.ENDPOINT_ADDRESS_PROPERTY, askUrl );
        return robotServiceEx;
    }

    /**
     * @Description <b> #传入robot的ws地址，发起问答请求 </b>
     * @param askUrl robot的ws
     * @param robotRequest 参数对象
     * @return RobotResponseEx
     * @since 1.0
     */
    private RobotResponseEx ask4Robot( String askUrl, RobotRequestEx request ){
        RobotResponseEx response = null;
        try{
            this.robotService = this.createRobotServiceEx( askUrl );
            // 调用产品ws接口
            response = robotService.deliver( request );
        }catch( Exception e ){
            logger.error( "", e );
            response = new RobotResponseEx();
        }
        return response;
    }

    /**
     * <pre>
     * <b>二开调用实现</b>
     * </pre>
     * 
     * @param robotRequest
     * @return RobotResponseEx
     * @since 1.0
     */
    public RobotResponseEx deliver( RobotRequest robotRequest ){
        RobotRequestEx req = this.createRobotRequestEx( robotRequest );
        return this.deliver( req );
    }

    /**
     * <pre>
     * <b>兼容产品接口</b>
     * </pre>
     * 
     * @param robotRequest
     * @return RobotResponseEx
     * @since 1.0
     */
    public RobotResponseEx deliver( RobotRequestEx request ){
        init();
        String url = hashServerNodeHelper.getNode( request.getUserId() );
        return this.ask4Robot( url, request );
    }

    /**
     * @Description <b> #用于检查接口状态 </b>
     * @param askUrl
     * @return boolean
     * @since 1.0
     */
    public boolean check( String askUrl ){
        RobotRequestEx request = new RobotRequestEx();
        request.setUserId( "9A4E638DC7905AE0_XXX" );
        request.setQuestion( "ae2b1fca515949e5d54fb22b8ed95575" );
        RobotResponseEx response = this.ask4Robot( askUrl, request );
        if( response != null && response.getContent() != null ){
            return true;
        }
        return false;
    }

    /**
     * @Description <b>二开自定义RobotRequest转换产品接口包定义的RobotRequestEx</b></br>
     * @param robotRequest
     * @return RobotRequestEx
     */
    public RobotRequestEx createRobotRequestEx( RobotRequest robotRequest ){
        // 组装产品RobotRequestEx
        RobotRequestEx req = new RobotRequestEx();
        // 设置UserId
        req.setUserId( robotRequest.getUserId() );
        // 设置SessionId
        if( StringUtils.isNotBlank( robotRequest.getUserId() ) ){
            req.setSessionId( robotRequest.getUserId() );
        }
        // 设置question
        req.setQuestion( robotRequest.getQuestion() );
        ArrayList<UserAttribute> attrs = new ArrayList<UserAttribute>();
        // 设置platform
        if( StringUtils.isNotEmpty( robotRequest.getPlatform() ) ){
            attrs.add( new UserAttribute( UserAttribute.PLATFORM, robotRequest.getPlatform() ) );
        }
        // 设置location
        if( StringUtils.isNotEmpty( robotRequest.getLocation() ) ){
            attrs.add( new UserAttribute( UserAttribute.LOCATION, robotRequest.getLocation() ) );
        }
        // 设置brand
        if( StringUtils.isNotEmpty( robotRequest.getBrand() ) ){
            attrs.add( new UserAttribute( UserAttribute.BRAND, robotRequest.getBrand() ) );
        }

        // 设置custom1
        if( StringUtils.isNotEmpty( robotRequest.getCustom1() ) ){
            attrs.add( new UserAttribute( "custom1", robotRequest.getCustom1() ) );
        }
        // 设置custom2
        if( StringUtils.isNotEmpty( robotRequest.getCustom2() ) ){
            attrs.add( new UserAttribute( "custom2", robotRequest.getCustom2() ) );
        }
        // 设置custom3
        if( StringUtils.isNotEmpty( robotRequest.getCustom3() ) ){
            attrs.add( new UserAttribute( "custom3", robotRequest.getCustom3() ) );
        }

        // 自定义属性
        if( StringUtils.isNotEmpty( robotRequest.getAttributes() ) ){// 分解附加维度属性
            String[] ats = robotRequest.getAttributes().split( "," );
            for( String a : ats ){
                String[] as = a.split( ":" );
                if( as.length == 2 ){
                    String key = as[0];
                    if( key.indexOf( "dim_" ) == -1 ){
                        key = "dim_" + key;
                    }
                    attrs.add( new UserAttribute( key, as[1] ) );
                }
            }
        }
        if( attrs.size() > 0 ){
            req.setAttributes( attrs.toArray( new UserAttribute[attrs.size()] ) );
        }

        // 设置tag
        if( robotRequest.getTags() != null ){
            req.setTags( robotRequest.getTags() );
        }

        // 设置modules
        if( robotRequest.getModules() != null ){
            req.setModules( robotRequest.getModules() );
        }

        // 最大返回相关问数量
        if( robotRequest.getMaxReturn() > 0 ){
            req.setMaxReturn( robotRequest.getMaxReturn() );
        }

        return req;
    }
    
   static HostnameVerifier hv = new HostnameVerifier(){
        public boolean verify( String urlHostName, SSLSession session ){
            System.out.println( "Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost() );
            return true;
        }
    };

    private static void trustAllHttpsCertificates() throws Exception{
        javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
        javax.net.ssl.TrustManager tm = new MyTM();
        trustAllCerts[0] = tm;
        javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance( "SSL" );
        sc.init( null, trustAllCerts, null );
        javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory( sc.getSocketFactory() );
    }

    static class MyTM implements javax.net.ssl.TrustManager,javax.net.ssl.X509TrustManager{
        public java.security.cert.X509Certificate[] getAcceptedIssuers(){
            return null;
        }

        public boolean isServerTrusted( java.security.cert.X509Certificate[] certs ){
            return true;
        }

        public boolean isClientTrusted( java.security.cert.X509Certificate[] certs ){
            return true;
        }

        public void checkServerTrusted( java.security.cert.X509Certificate[] certs, String authType ) throws java.security.cert.CertificateException{
            return;
        }

        public void checkClientTrusted( java.security.cert.X509Certificate[] certs, String authType ) throws java.security.cert.CertificateException{
            return;
        }
    }

}
