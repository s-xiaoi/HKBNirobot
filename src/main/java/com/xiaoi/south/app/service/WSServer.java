package com.xiaoi.south.app.service;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.resources.Messages_es;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * websocket
 * 2019-11-01
 * brave
 */
//ws://127.0.0.1:8081/HKBN/ws/张三
@ServerEndpoint("/ws/{user}")
public class WSServer {
    /**
     * 静态变量 用来记录当前在线连接数
     */
    private static int onlineCount = 0;

    private static final Logger logger = LoggerFactory.getLogger(WSServer.class);
    private String currentUser;
    private Session session;
    /**
     * 服务端与单一客户端通信 使用Map来存放 其中标识Key为id
     */
    private static ConcurrentMap<String, WSServer> webSocketMap = new ConcurrentHashMap<String, WSServer>();

    public static ConcurrentMap<String, WSServer> getWebSocketMap() {
        return webSocketMap;
    }

    //连接打开时执行
    @OnOpen
    public void onOpen(@PathParam("user") String user, Session session) {
        webSocketMap.put(user, this);

        this.session = session;
        currentUser = user;
        logger.info("Connected socketSession... ===" + session.getId() + ",cacheKey=socketSession-" + user);

    }

    //收到消息时执行
    @OnMessage
    public String onMessage(String message, Session session) {
        this.session = session;
        JSONObject jsonMessage=new JSONObject();
        if(message!=null&& message.equals("PING")){
        jsonMessage.put("msg","PONG");
        jsonMessage.put("code","1024");
        return jsonMessage.toString();
        }else if(message!=null&&message.equals("closeSocket")){
            this.onClose(session,null);
            jsonMessage.put("msg","PONG_CLOSE");
            jsonMessage.put("code","1024");
            return jsonMessage.toString();
        }else{
            jsonMessage.put("msg",message);
            jsonMessage.put("code","0");
            return jsonMessage.toString();
        }
    }

    //连接关闭时执行
    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        logger.info(String.format("Session %s closed because of %s", session.getId(), closeReason));

    }

    //连接错误时执行
    @OnError
    public void onError(Throwable t) {

        logger.info("loading error");
        t.printStackTrace();
    }

    /**
     * 发送消息方法。
     *
     * @param message
     * @throws IOException
     */
    public void sendMessage(Session session, String message) throws IOException {
        session.getBasicRemote().sendText(message);   //向客户端发送数据
    }

    /**
     * 这个方法与上面几个方法不一样。没有用注解，是根据自己需要添加的方法。
     *
     * @param message
     * @throws IOException
     */
    public void sendMessage(JSONObject message) throws IOException {
        logger.info("websocket sendMessage:"+message);
        //如果需要传送bean，可转成json字符串
        this.session.getBasicRemote().sendText(message.toString());
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public void PingSocket() {
//        this.session.getBasicRemote().sendPing("test");
    }

    public static synchronized void addOnlineCount() {
        WSServer.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WSServer.onlineCount--;
    }
}
