package com.xiaoi.south.app.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiaoi.south.app.mybatis.mapper.DefaultSqlMapper;

/**  
 * <p>Title: DefaultSqlServiceImpl</p>  
 * <p>Description: </p>  
 * @author zfree  
 * @create_date 2018年7月12日
 */
@Service
public class DefaultSqlServiceImpl {
	
	
	@Autowired
	private DefaultSqlMapper sqlMapper;

	public Map<String, Object> getCategoryNameById(String id) {
		
		return sqlMapper.selectCategoryNameById(id);
	}
	
	public List<Map<String, Object>> getCategoryNames() {
		
		return sqlMapper.selectCategoryNames();
	}

	public Map<String, Object> getRoleNameByUsernameAndPwd(String username, String password) {
		
		return sqlMapper.selectRoleByNameAndPwd(username, password);
	}

	public Map<String, Object> getRoleNameByAdsGroup(String groupName) {
		
		return sqlMapper.selectRoleByAdsGroup(groupName);
	}

	public List<Map<String, Object>> getRolesByAds() {
		
		return sqlMapper.selectRolesByAds();
	}
	
	
	
}
