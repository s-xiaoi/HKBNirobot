package com.xiaoi.south.app.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.xiaoi.south.app.entity.AskLogBean;
import com.xiaoi.south.app.modules.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eastrobot.robotface.domain.RobotCommand;
import com.eastrobot.robotface.domain.RobotResponseEx;
import com.xiaoi.south.app.cache.CacheSupport;
import com.xiaoi.south.app.constant.GlobalConstants;
import com.xiaoi.south.app.entity.OutLookBean;
import com.xiaoi.south.app.entity.RobotRequest;
import com.xiaoi.south.app.entity.RobotResponse;

import me.ramswaroop.jbot.core.facebook.models.Attachment;
import me.ramswaroop.jbot.core.facebook.models.Button;
import me.ramswaroop.jbot.core.facebook.models.Element;
import me.ramswaroop.jbot.core.facebook.models.Message;
import me.ramswaroop.jbot.core.facebook.models.Payload;

/**
 * <pre>
 * AskServiceProxy 问答服务类
 * </pre>
 *
 * @porter ziQi
 * @email u36369@163.com
 * @copyright (c) 池塘上的树
 * @since 1.0
 */
@Service("askServiceProxy")
public class AskServiceProxy {
    private static final Logger logger = LoggerFactory.getLogger(AskServiceProxy.class);
    @Autowired
    private DefaultMessageInterfaceResourceProxy iBotMessageResourceProxy;
    // DefaultRobotServiceProxy proxy = new DefaultRobotServiceProxy();
    DefaultRobotServiceCXFProxy proxy = new DefaultRobotServiceCXFProxy();
    DefaultRobotTCServiceProxy tcProxy = new DefaultRobotTCServiceProxy();
    public String langFlag = "";
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");

    CacheSupport cacheSupport = new CacheSupport(GlobalConstants.APP_CACHE);
    @Autowired
    private RedisUtils redisUtil;
    /**
     * @param robotRequest
     * @return RobotResponse
     * @Description <b>(提交产品提问,组装并返回结果)</b></br>
     * @since 2016年1月30日
     */
    public RobotResponse ask4Robot(RobotRequest robotRequest) {

        // 不入库处理
        // 问题不入库处理
        // 约定不入库的问题规则(以:_XXX_TAG结尾的问题不入库)
        // 约定userId以_XXX结尾的不记录，需在前端jetty.sh加入参数:-Drobot.logDisabledUserIdSuffix=_XXX

        if (StringUtils.isNotEmpty(robotRequest.getQuestion())
                && (StringUtils.endsWith(robotRequest.getQuestion(), "_XXX_TAG")
                || StringUtils.equalsIgnoreCase(robotRequest.getQuestion(), "system_welcome"))) {
            // welcome.flag.data
            String welcomeFlag = this.iBotMessageResourceProxy.getString("welcome.flag.data",
                    robotRequest.getPlatform());
            if (welcomeFlag != null && welcomeFlag.equals("true")) {
                robotRequest.setUserId(robotRequest.getUserId() + "_XXX");

            }
        }

//        RobotResponseEx resp = proxy.deliver( robotRequest );
        // 问题语言识别
        RobotResponseEx resp = questionLanguageRecognition(robotRequest);
        // 组装二次开发自定义response
        RobotResponse response = this.createRobotResponse(resp);

        // 获取baseUrl
        String baseUrl = this.iBotMessageResourceProxy.getString("x.attachment.resource.url",
                robotRequest.getPlatform());
        // 定义attachment,设置二开自定义报文，如：图文消息，图片消息，P4等
        response = AttachmentUtilsbak.rebuild(robotRequest, response, baseUrl);
        //交互记录写缓存
        Object object=redisUtil.get(robotRequest.getUserId()+"-asklog");
        List<AskLogBean> lists=new ArrayList<AskLogBean>();
        if(object!=null){
            lists=(List<AskLogBean>)object;
        }
        AskLogBean askObj=new AskLogBean();
        askObj.setQuestion(robotRequest.getQuestion());
        askObj.setAnswer(response.getContent());
        askObj.setCreateTime(sdf.format(new Date()));
        askObj.setType(response.getType());
        askObj.setUserId(robotRequest.getUserId());
        askObj.setSessionId(robotRequest.getSessionId());
        askObj.setPlstform(robotRequest.getPlatform());
        lists.add(askObj);
        redisUtil.set(robotRequest.getUserId()+"-asklog",lists);
        return response;
    }

    /**
     * whatsApp调用机器人问答方法
     * 含有逻辑：
     * 1，不入库处理
     * 2，语言识别
     * 3，约定userId以_XXX结尾的不入库
     *
     * @param robotRequest
     * @return
     */
    public RobotResponseEx askToWhatsApp(RobotRequest robotRequest) {
        if (StringUtils.isNotEmpty(robotRequest.getQuestion())
                && (StringUtils.endsWith(robotRequest.getQuestion(), "_XXX_TAG")
                || StringUtils.equalsIgnoreCase(robotRequest.getQuestion(), "system_welcome"))) {
            // welcome.flag.data
            String welcomeFlag = this.iBotMessageResourceProxy.getString("welcome.flag.data",
                    robotRequest.getPlatform());
            if (welcomeFlag != null && welcomeFlag.equals("true")) {
                robotRequest.setUserId(robotRequest.getUserId() + "_XXX");
            }
        }

//        RobotResponseEx resp = proxy.deliver( robotRequest );
        // 问题语言识别
        RobotResponseEx resp = questionLanguageRecognition(robotRequest);


        return resp;
    }

    public RobotResponse createRobotResponse(RobotResponseEx resp) {
        // 自定义响应对象
        RobotResponse response = new RobotResponse();
        response.setType(resp.getType());
        response.setContent(resp.getContent());
        response.setNodeId(resp.getNodeId());
        response.setModuleId(resp.getModuleId());
        response.setTags(resp.getTags());
        response.setSimilarity(resp.getSimilarity());
        response.setCommands(resp.getCommands());
        response.setRelatedQuestions(resp.getRelatedQuestions());
        return response;
    }

    public DefaultMessageInterfaceResourceProxy getiBotMessageResourceProxy() {
        return iBotMessageResourceProxy;
    }

    public void setiBotMessageResourceProxy(DefaultMessageInterfaceResourceProxy iBotMessageResourceProxy) {
        this.iBotMessageResourceProxy = iBotMessageResourceProxy;
    }

    /**
     * 请求报文封装
     *
     * @param robotRequest
     * @return
     */
    public RobotResponseEx askFbRobot(RobotRequest robotRequest) {

        // 不入库处理
        // 问题不入库处理
        // 约定不入库的问题规则(以:_XXX_TAG结尾的问题不入库)
        // 约定userId以_XXX结尾的不记录，需在前端jetty.sh加入参数:-Drobot.logDisabledUserIdSuffix=_XXX
        if (StringUtils.isNotEmpty(robotRequest.getQuestion())
                && (StringUtils.endsWith(robotRequest.getQuestion(), "_XXX_TAG")
                || StringUtils.equalsIgnoreCase(robotRequest.getQuestion(), "system_welcome"))) {
            robotRequest.setUserId(robotRequest.getUserId() + "_XXX");
        }

//        RobotResponseEx resp = enProxy.deliver( robotRequest );
        RobotResponseEx resp = questionLanguageRecognition(robotRequest);
        return resp;
    }

    /**
     * 问题语言识别，根据语言字体识别：
     *
     * @param robotRequest
     * @return
     */
    private RobotResponseEx questionLanguageRecognition(RobotRequest robotRequest) {

        RobotResponseEx resp = null;
        String question = robotRequest.getQuestion();
        String lang = robotRequest.getLang();
        String qlrPower = robotRequest.getQlrPower();
        String qlrTipPower = "";
        String qlrTip = "";
//		String tcQuestion = LanguageUtils.sc2TC(question);
        String scQuestion = LanguageUtils.sc2TC(question);
        String userId = robotRequest.getUserId();
        boolean qlrFlag = false;
        String qlrLang = null;
//		String askEx = "user_ask_ex:"+lang;

        if (StringUtils.isNotBlank(lang)) {
            lang = lang.toLowerCase();
        }
        if (StringUtils.isBlank(qlrPower) && robotRequest.getCustom1() == null) {
            try {
                qlrPower = this.iBotMessageResourceProxy.getValue(GlobalConstants.QLR_POWER, "all", "off");
            } catch (Exception e) {
                e.printStackTrace();
                qlrPower = "off";
                // TODO: handle exception
            }
        }

        langFlag = lang;
        String taskUserId = UUID.randomUUID() + GlobalConstants.USERID_TASK;
        resp = tcProxy.deliver(robotRequest);
        if (GlobalConstants.LANG[1].equals(lang)) { // lang=简体
//			resp = scProxy.deliver(robotRequest);
            if (0 == resp.getType() && resp.getCommands() != null && !resp.getCommands()[0].getName().equals("eventWord")) {
                if (LanguageUtils.hasEnglish(question) && !LanguageUtils.hasChinese(question)) {
                    robotRequest.setUserId(userId);
                    robotRequest.setLocation(GlobalConstants.LANG[2]);
                    robotRequest.setLang(GlobalConstants.LANG[2]);
//					resp = enProxy.deliver(robotRequest); // 入库调用
                    qlrFlag = true;
                    qlrLang = GlobalConstants.LANG[2];
                    langFlag = GlobalConstants.LANG[2];
                    cacheSupport.put("quLang", GlobalConstants.LANG[2]);
                } else {
                    robotRequest.setUserId(userId);
                    robotRequest.setLocation(GlobalConstants.LANG[0]);
                    robotRequest.setLang(GlobalConstants.LANG[0]);
                    resp = tcProxy.deliver(robotRequest); // 入库调用
                    qlrFlag = true;
                    qlrLang = GlobalConstants.LANG[0];
                    langFlag = GlobalConstants.LANG[0];
                    cacheSupport.put("quLang", GlobalConstants.LANG[0]);
                }
            }
        } else if (GlobalConstants.LANG[2].equals(lang)) { // lang=英文
//			resp = enProxy.deliver(robotRequest);
            if (0 == resp.getType() && resp.getCommands() != null && !resp.getCommands()[0].getName().equals("eventWord")) {
                if (LanguageUtils.hasChinese(question)) {
                    if (!question.equals(scQuestion)) {
                        robotRequest.setUserId(taskUserId);
                        robotRequest.setUserId(userId);
                        robotRequest.setLang(GlobalConstants.LANG[1]);
                        robotRequest.setLocation(GlobalConstants.LANG[1]);
//						resp = scProxy.deliver(robotRequest); // 入库调用
                        qlrFlag = true;
                        qlrLang = GlobalConstants.LANG[1];
                        langFlag = GlobalConstants.LANG[1];
                        cacheSupport.put("quLang", GlobalConstants.LANG[1]);
                    } else {
                        robotRequest.setUserId(taskUserId);
                        robotRequest.setUserId(userId);
                        robotRequest.setLang(GlobalConstants.LANG[0]);
                        robotRequest.setLocation(GlobalConstants.LANG[0]);
                        resp = tcProxy.deliver(robotRequest); // 入库调用
                        qlrFlag = true;
                        qlrLang = GlobalConstants.LANG[0];
                        langFlag = GlobalConstants.LANG[0];
                        cacheSupport.put("quLang", GlobalConstants.LANG[0]);
                    }
                }
            }
        } else { // 繁体
//			resp = tcProxy.deliver(robotRequest); // 默认繁体
            if (0 == resp.getType() && resp.getCommands() != null && resp.getCommands()[0].getName() != null && !resp.getCommands()[0].getName().equals("eventWord")) {
                if (LanguageUtils.hasEnglish(question) && !LanguageUtils.hasChinese(question)) {
                    robotRequest.setUserId(userId);
                    robotRequest.setLang(GlobalConstants.LANG[2]);
                    robotRequest.setLocation(GlobalConstants.LANG[2]);
//						resp = enProxy.deliver(robotRequest); // 入库调用
                    qlrFlag = true;
                    qlrLang = GlobalConstants.LANG[2];
                    langFlag = GlobalConstants.LANG[2];
                    cacheSupport.put("quLang", GlobalConstants.LANG[2]);
                }
            }
        }
        robotRequest.setUserId(userId);
        //判断语言设值agent传的语言
        if (langFlag == null || "".equals(langFlag)) {
            langFlag = lang;
        }
        Object agent = cacheSupport.get(robotRequest.getUserId() + "-agent");
        Object agentCount = cacheSupport.get(robotRequest.getUserId() + "-agent-Count");
        Integer defaultReplyTimes = 0;
        if(agentCount!=null){
            if((Integer)agentCount<3){
                defaultReplyTimes=(Integer)agentCount;
            }
        }else {

        }
        List<String> lists=null;
        if (agent != null) {
            lists = (List<String>) agent;
        }else{
            lists=new ArrayList<String>();
        }
        if (lists.size() < 3) {
            lists.add(langFlag);
            cacheSupport.put(robotRequest.getUserId() + "-agent", lists);
        }else{

        }

        if (qlrFlag && robotRequest.getCustom1() == null) {
            qlrTipPower = this.iBotMessageResourceProxy.getValue(GlobalConstants.QLR_TIP_POWER,
                    robotRequest.getPlatform(), "off");
            qlrTip = this.iBotMessageResourceProxy.getValue(
                    GlobalConstants.QLR_TIP.replace("{0}", lang == null ? "en" : lang), robotRequest.getPlatform(),
                    "defaultValue");
            resp = setQLRinfo(resp, qlrTipPower, qlrTip, qlrLang);
        } else if (robotRequest.getCustom1() != null && robotRequest.getCustom1().equals("FM")) {
            resp.setContent(resp.getContent());
        }
        //langFlag = lang;
        return resp;
    }


    private RobotResponseEx setQLRinfo(RobotResponseEx resp, String qlrTipPower, String qlrTip, String qlrLang) {
        String qlrContent = resp.getContent();
        RobotCommand[] commands = resp.getCommands();
        RobotCommand command = new RobotCommand();
        command.setName("qlrinfo");
        String[] args = new String[1];
        JSONObject qlrInfo = new JSONObject();
        qlrInfo.put("qlrLang", qlrLang);
        qlrInfo.put("qlrTipPower", qlrTipPower);
        qlrInfo.put("qlrTip", qlrTip);
        qlrInfo.put("qlrContent", qlrContent);
        args[0] = qlrInfo.toJSONString();
        command.setArgs(args);
        if (commands == null) {
            commands = new RobotCommand[1];
            commands[0] = command;
        } else {
            commands = Arrays.copyOf(commands, commands.length + 1);
            commands[commands.length - 1] = command;
        }
        resp.setCommands(commands);
        //resp.setType(102); // 问题语言识别功能的消息类型
        return resp;
    }

    /**
     * 返回报文封装
     *
     * @param req resp
     * @return
     */
    public List<Message> FaceBookEventResp(RobotRequest req, RobotResponseEx resp) {
        RobotRequest robotRequest = new RobotRequest();
        List<Message> messages = new ArrayList<Message>();
        String question = req.getQuestion();

        if (question != null && ((question.toLowerCase()).equals("help".trim())
                || (question.toLowerCase()).equals("hi".trim()) || (question.toLowerCase()).equals("hello".trim()))) {
            Button[] quickReplies = new Button[]{
                    new Button().setContentType("text").setTitle("Sure").setPayload("yes"),
                    new Button().setContentType("text").setTitle("Nope").setPayload("no")};
            messages.add(new Message().setText("Hello, I am JBot. Would you like to see more?")
                    .setQuickReplies(quickReplies));
        } else if (question != null && (question.toLowerCase()).equals("yes".trim()) || question.equals("no".trim())) {
            if ("yes".equals(question)) {
                messages.add(new Message().setType("text")
                        .setText("\"Cool! You can type: \\n 1) Show Buttons \\n 2) Show List \\n 3) swipe picture"));
            } else {
                messages.add(new Message().setType("text").setText("See you soon!"));
            }
        } else if (question != null
                && ((question.equals("information")) || (question.toLowerCase()).equals("show buttons".trim()))) {
            Button[] buttons = new Button[]{
                    new Button().setType("web_url").setUrl("http://blog.ramswaroop.me").setTitle("JBot Docs1"),
                    new Button().setType("web_url").setUrl("https://goo.gl/uKrJWX").setTitle("Buttom Template"),
                    new Button().setType("web_url").setUrl("http://blog.ramswaroop.me").setTitle("JBot Docs1"),};
            messages.add(new Message().setAttachment(new Attachment().setType("template").setPayload(
                    new Payload().setTemplateType("button").setText("These are 2 link buttons.").setButtons(buttons))));
        } else if (question != null && (question.toLowerCase()).equals("show list".trim())) {
            Element[] elements = new Element[]{ // postback
                    new Element().setTitle("AnimateScroll").setSubtitle("A jQuery Plugin for Animating Scroll.")
                            .setImageUrl("https://plugins.compzets.com/images/as-logo.png"),
                    new Element().setTitle("AnimateScroll").setSubtitle("A jQuery Plugin for Animating Scroll.")
                            .setImageUrl("https://plugins.compzets.com/images/as-logo.png"),
                    new Element().setTitle("AnimateScroll").setSubtitle("A jQuery Plugin for Animating Scroll.")
                            .setImageUrl("https://plugins.compzets.com/images/as-logo.png"),
                    new Element().setTitle("AnimateScroll").setSubtitle("A jQuery Plugin for Animating Scroll.")
                            .setImageUrl("https://plugins.compzets.com/images/as-logo.png"),};
            messages.add(new Message().setAttachment(new Attachment().setType("template").setPayload(new Payload()
                    .setTemplateType("list").setTopElementStyle("compact").setElements(elements).setButtons(
                            new Button[]{new Button().setType("postback").setTitle("test1").setPayload("test1")}))));
        } else if (question != null && (question.toLowerCase()).equals("swipe picture".trim())) {
            messages.add(this.SwipeQicture());
        } else {// 前面的都是为了过审核，facebook的测试机器人会问上面的一些问题
            if (resp.getContent() != null) {// 文本封装，比如菜单封装成按钮，去除html标签
//    			Message message=new Message();
                messages = FaceBookUtils.FaceBookMessageResp(messages, resp.getContent());
//        		messages.add(message);
            }
            messages = FaceBookUtils.rebuild(messages, resp);// 图文消息，图片消息封装

        }
        // messages.add(new Message().setType("text").setText(req.getQuestion()));
        return messages;
    }

    // 滚动图数据拼装
    public Message SwipeQicture() {
        Message message = new Message();
        Element[] element = new Element[]{
                new Element().setTitle("test1").setImageUrl("https://plugins.compzets.com/images/as-logo.png")
                        .setSubtitle("We have the right hat for everyone 11111")
                        .setDefaultAction(new Button().setType("web_url").setUrl("https://plugins.compzets.com")
                        .setMessengerExtensions(false).setWebviewHeightRatio("tall")),
                new Element().setTitle("test2")
                        .setImageUrl("https://plugins.compzets.com/contentshare/img/cs-logo-64.png")
                        .setSubtitle("We have the right hat for everyone 22222")
                        .setDefaultAction(new Button().setType("web_url")
                        .setUrl("https://plugins.compzets.com/images/fs-logo.png").setMessengerExtensions(false)
                        .setWebviewHeightRatio("tall")),
                new Element().setTitle("test3").setImageUrl("https://plugins.compzets.com/images/fs-logo.png")
                        .setSubtitle("We have the right hat for everyone 33333")
                        .setDefaultAction(new Button().setType("web_url").setUrl("https://plugins.compzets.com")
                        .setMessengerExtensions(false).setWebviewHeightRatio("tall")),};
        message.setAttachment(new Attachment().setType("template")
                .setPayload(new Payload().setTemplateType("generic").setElements(element)));

        return message;
    }

    public static void main(String[] args) throws ParseException {
        System.out.println(sdf.format(new Date("2019-04-23 12:00:00")));
        ;
    }

}
