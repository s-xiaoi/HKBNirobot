package com.xiaoi.south.app.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xiaoi.south.app.modules.utils.HttpUtils;
import com.xiaoi.south.app.modules.utils.PropertiesUtils;
import com.xiaoi.south.app.modules.utils.StringTookits;

import jodd.cache.LFUCache;

/**
 * @description 获取服务参数实现
 * @since 1.0
 * @author brave.chen
 * @date 2019-06-26
 */
@Service( "DefaultMessageInterfaceResourceProxy" )
public class DefaultMessageInterfaceResourceProxy {
	private static final Logger logger = LoggerFactory.getLogger( DefaultMessageInterfaceResourceProxy.class );
	// 2000容量，缓存30分钟/1800秒
	LFUCache<String, String> cache = new LFUCache<String, String>( 2000, 1800000 );
	
    /**
     * 根据messenger接口获取服务参数并缓存
     */
	@Scheduled( cron = "0 0/30 * * * ?" )
    public  void reloadManagerMessage() {
    	Map<String, String> paramsMap=new HashMap<String, String>();
    	String password = DigestUtils.shaHex(PropertiesUtils.getString("manager.password"));
    	paramsMap.put("ibot-auth-username", PropertiesUtils.getString("manager.username"));
    	paramsMap.put("ibot-auth-password", password);
    	Map<String, String> dimensionMap=new HashMap<String, String>();
    	dimensionMap=dimension(paramsMap);
    	//获取服务参数，维度里面只有ID,需要从下面的接口中去获取name及tag
		String messageresource=HttpUtils.getPostContent(PropertiesUtils.getString("manager.messageresource"),paramsMap, paramsMap, "utf-8");
		JSONObject json=new JSONObject();
//		System.out.println("messageresource:"+messageresource);
		json=JSON.parseObject(messageresource);
		JSONArray jsonArray=(JSONArray) json.get("data");
		for(int i=0;i<jsonArray.size();i++) {
	        String dimenTag=jsonArray.getJSONObject(i).get("dimenTags").toString();
	        String[] dimenTags=dimenTag.split(",");
	        String valueKey = StringTookits.hashKey( (String)jsonArray.getJSONObject(i).get("key") ,dimensionMap.get(dimenTags[0]));
	        this.cache.put(valueKey, (String)jsonArray.getJSONObject(i).get("message"));
	    }
		
    }
    /**
     * <pre>
     * <b>最多会从缓存查四次</b>
     *  第一次直接根据key+platform从缓存取
     *  没取到，假设key存在，有可能是服务参数设置全维度
     *  第二次直接用key从缓存取
     *  还是没取到，假设存在，那又可能是缓存过期，然后调用接口缓存
     *  第三四次，继续第一二次的逻辑
     *  任然没有说明服务参数不存在，返回null
     * </pre>
     * @param key	参数标识
     * @param platform  维度
     * @return	参数内容
     */
    public String getString(String key,String platform) {
    	String valueKey = StringTookits.hashKey( key, platform);
        String value = this.cache.get( valueKey );
        if( StringUtils.isNotBlank( value ) ){
            return value;
        }else {
        	valueKey = StringTookits.hashKey( key);
        	value = this.cache.get( valueKey );
        	if( StringUtils.isNotBlank( value ) ){
                return value;
            }
        }
        try {
        	this.reloadManagerMessage();
        }catch (Exception e) {
			// TODO: handle exception
        	e.printStackTrace();
        	logger.error("加载服务参数异常");
		}
        valueKey = StringTookits.hashKey( key, platform);
        value = this.cache.get( valueKey );
        if( StringUtils.isNotBlank( value ) ){
            return value;
        }else {
        	valueKey = StringTookits.hashKey( key);
        	value = this.cache.get( valueKey );
        	if( StringUtils.isNotBlank( value ) ){
                return value;
            }
        }
        
    	return null;
    }
    
    /**
     * 修改tag为name就可以返回id对应的名字
     * @return 返回维度键值   key  维度id   value  维度tag
     */
    public  Map<String,String> dimension(Map<String, String> paramsMap){
    	Map<String,String> dimensionMap=new HashMap<String, String>();
    	//获取维度id及对应值
    	String dimension=HttpUtils.getPostContent(PropertiesUtils.getString("manager.ontologydimension"), paramsMap, paramsMap, "utf-8");
//    	System.out.println("dimension"+dimension);
    	JSONObject json=new JSONObject();
		json=JSON.parseObject(dimension);
		if(json!=null) {
			JSONArray jsonArray = (JSONArray) json.get("data");
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONArray jsontags = (JSONArray) jsonArray.getJSONObject(i).get("tags");
				for (int j = 0; j < jsontags.size(); j++) {
					dimensionMap.put((String) jsontags.getJSONObject(j).get("id"), (String) jsontags.getJSONObject(j).get("name"));

				}
			}
		}
    	return dimensionMap;
    }

    public static void main(String[] args) {
//    	reloadManagerMessage();
		/*
		 * Map<String, String> paramsMap=new HashMap<String, String>(); String password
		 * = DigestUtils.shaHex(PropertiesUtils.getString("manager.password"));
		 * paramsMap.put("ibot-auth-username",
		 * PropertiesUtils.getString("manager.username"));
		 * paramsMap.put("ibot-auth-password", password); Map<String,String>
		 * dimensionMap=new HashMap<String, String>();
		 * dimensionMap=dimension(paramsMap); Iterator<Entry<String, String>> entries =
		 * dimensionMap.entrySet().iterator(); while (entries.hasNext()) { Entry<String,
		 * String> entry = entries.next(); System.out.println("Key = " + entry.getKey()
		 * + ", Value = " + entry.getValue()); }
		 */
	}
    /**
     * 
     * @param key
     * @param platform
     * @return 返回Integer类型
     */
	public Integer getInteger(String key, String platform) {
		Integer i = 0;
        String value = this.getString( key, platform );
        if( StringUtils.isNumeric( value ) ){
            i = Integer.valueOf( value );
        }
        return i;
	}
	 /**
     * @Description <b> 根据维度获取服务参数 </b>
     * @param key key值
     * @param platform
     * @param _default
     * @return String
     * @since 1.0
     */
    public String getValue( String key, String platform, String _default ){
        String value = this.getString( key, platform );
        if( StringUtils.isBlank( value ) ){
            value = _default;
        }
        return value;
    }
}
