package com.xiaoi.south.app.service.impl;

import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import org.apache.commons.lang.StringUtils;

import com.xiaoi.south.app.modules.utils.HashServerNodeHelper;
import com.xiaoi.south.app.modules.utils.PropertiesUtils;
import com.xiaoi.south.app.modules.utils.SpringContextUtils;
import com.xiaoi.south.app.ws.QuestionService;

/**
 * <pre>
 * 智能提示、相关问、热点问题提示
 * </pre>
 * 
 * @porter ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class DefaultQuestionServiceProxy{

    private QuestionService questionService;

    /**
     * hashServerNodeHelper:接口地址挑选器( 一致性hash )
     */
    private static HashServerNodeHelper hashServerNodeHelper;

    /**
     * 实例DefaultQuestionServiceProxy.
     * 
     */
    public DefaultQuestionServiceProxy(){
    }

    /**
     * <b> #初始化HashServerNodeHelper </b>
     * 
     * @since 1.0
     */
    @SuppressWarnings( "unchecked" )
    public void init(){
        if( hashServerNodeHelper == null ){
            String questionUrlstr = PropertiesUtils.getString( "irobot.question.ws.urls" );
            if( StringUtils.isNotBlank( questionUrlstr ) ){// 从配置文件读
                hashServerNodeHelper = HashServerNodeHelper.getInstanse( questionUrlstr );
            }else{// 从spring取
                List<String> questionUrlList = (List<String>) SpringContextUtils.getBean( "irobot.question.ws.urls" );
                hashServerNodeHelper = HashServerNodeHelper.getInstanse( questionUrlList );
            }
        }
    }

    /**
     * <pre>
     * <b>初始化QuestionServiceEx</b>
     * </pre>
     * 
     * @param askUrl webService的wsdl地址
     * @return RobotServiceEx
     * @since 1.0
     */
    public QuestionService createQuestionService( String questionServiceUrl ){
        QuestionService questionService = null;
        Service svc = Service.create( new QName( "http://www.eastrobot.cn/ws/QuestionService", "QuestionService" ) );
        svc.addPort( new QName( "http://www.eastrobot.cn/ws/QuestionService", "DefaultQuestionServicePort" ), SOAPBinding.SOAP11HTTP_BINDING, questionServiceUrl );
        questionService = svc.getPort( new QName( "http://www.eastrobot.cn/ws/QuestionService", "DefaultQuestionServicePort" ), QuestionService.class );
        Map<String,Object> requestContext = ((BindingProvider) questionService).getRequestContext(); 
        requestContext.put("com.sun.xml.internal.ws.connect.timeout", 3000); // 连接超时    
        requestContext.put("com.sun.xml.internal.ws.request.timeout", 10000); // 请求超时   
        requestContext.put( "thread.local.request.context", "true" );
        requestContext.put( BindingProvider.ENDPOINT_ADDRESS_PROPERTY, questionServiceUrl );
        return questionService;
    }

    public List<String> getHotQuestions( int mode, String platform, String location, int top ){
        String url = hashServerNodeHelper.getNode( String.valueOf( Math.random() ) );
        this.questionService = this.createQuestionService( url );
        return this.questionService.getHotQuestions( mode, platform, location, top );
    }

    public List<String> getHotQuestions0( int mode, String platform, String location, String brand, int top ){
        String url = hashServerNodeHelper.getNode( String.valueOf( Math.random() ) );
        this.questionService = this.createQuestionService( url );
        return this.questionService.getHotQuestions0( mode, platform, location, brand, top );
    }

    public List<String> getRelatedQuestions( String question, String platform, String location, int top ){
        String url = hashServerNodeHelper.getNode( String.valueOf( Math.random() ) );
        this.questionService = this.createQuestionService( url );
        return this.questionService.getRelatedQuestions( question, platform, location, top );
    }

    public List<String> getRelatedQuestions0( String question, String platform, String location, String brand, int top ){
        String url = hashServerNodeHelper.getNode( String.valueOf( Math.random() ) );
        this.questionService = this.createQuestionService( url );
        return this.questionService.getRelatedQuestions0( question, platform, location, brand, top );
    }

    public List<String> getSuggestedQuestions( String input, String platform, String location, int top ){
        String url = hashServerNodeHelper.getNode( String.valueOf( Math.random() ) );
        this.questionService = this.createQuestionService( url );
        return this.questionService.getSuggestedQuestions( input, platform, location, top );
    }

    public List<String> getSuggestedQuestions0( String input, String platform, String location, String brand, int top ){
        String url = hashServerNodeHelper.getNode( String.valueOf( Math.random() ) );
        this.questionService = this.createQuestionService( url );
        return this.questionService.getSuggestedQuestions0( input, platform, location, brand, top );
    }

}
