package com.xiaoi.south.app.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.xiaoi.south.app.modules.utils.HttpUtils;

/**
 * <p>
 * Title: ManagerSsoService
 * </p>
 * <p>
 * Description: 提供或者调用产品单点登陆系统
 * </p>
 * 
 * @author zfree
 * @create_date 2018年7月24日
 */
public class ManagerSsoService {
	public static final String CHARSET = "utf-8";
	// manager中配置的加密串，加密串长度必须为24位
	private final static String debugkey = "hsbc_login_key_sso000000";
	private static final Logger logger = LoggerFactory.getLogger(ManagerSsoService.class);

	private static byte[] getKey() throws Exception {
		byte[] ret = null;
		ret = debugkey.getBytes(CHARSET);
		return ret;
	}

	static String transformation = "DESede/ECB/PKCS5Padding";
	static String algorithm = "DESede";

	private static String encode(String src) throws Exception {
		return encode0(src, getKey());
	}

	private static String encode0(String src, byte[] key) throws Exception {
		SecretKey secretKey = new SecretKeySpec(key, algorithm);
		Cipher cipher = Cipher.getInstance(transformation);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] b = cipher.doFinal(src.getBytes(CHARSET));
		return new String(Hex.encodeHex(b));
	}

	public static String decode(String dest) throws Exception {
		return decode0(dest, getKey());
	}

	public static String decode0(String dest, byte[] key) throws Exception {
		SecretKey secretKey = new SecretKeySpec(key, algorithm);
		Cipher cipher = Cipher.getInstance(transformation);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] b = cipher.doFinal(Hex.decodeHex(dest.toCharArray()));
		return new String(b, CHARSET);
	}

	// for 单点
	public static String getManagerUrl(String username, String userrole,
			String managerPath) {
		String path = "";
		try {
			String raw = "timestamp="
					+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
					+ "&userprivilege=0&username=" + username + "&userroles="
					+ userrole + "";
			String token = encode(raw);
			String digest = DigestUtils.md5Hex(token);
			
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put("X-Requested-With", "XMLHTTPRequest");
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put("ssotoken", token);
			paramsMap.put("ssochecksum", digest);
			String responseText = HttpUtils.getPostContent(managerPath
					+ "/authmgr/auth!login.action", paramsMap, headerMap, CHARSET);
			logger.info("登录信息验证：" + responseText);
			
			path = String.format(managerPath
					+ "/authmgr/auth!login.action?ssotoken=%s&ssochecksum=%s",
					token, digest);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return path;
	}

	public static JSONObject getManagerLoginInfo(String debugKey, String username, String userrole,
			String managerPath) {
		JSONObject result = new JSONObject();
		try {
			
			String raw = "timestamp="
					+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
					+ "&userprivilege=0&username=" + username + "&userroles="
					+ userrole + "";
			String token = encode0(raw, debugKey.getBytes(CHARSET));
			String digest = DigestUtils.md5Hex(token);
			
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put("X-Requested-With", "XMLHTTPRequest");
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put("ssotoken", token);
			paramsMap.put("ssochecksum", digest);
			String responseText = HttpUtils.getPostContent(managerPath
					+ "/authmgr/auth!login.action", paramsMap, headerMap, CHARSET);
			logger.info("登录信息验证：" + responseText);
			if (responseText != null) {
				result = JSONObject.parseObject(responseText);
				String loginUrl = String.format(managerPath
						+ "/authmgr/auth!login.action?ssotoken=%s&ssochecksum=%s",
						token, digest);
				result.put("url", loginUrl);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
	public static JSONObject getManagerLoginInfo(String username, String userrole,
			String managerPath) {
		JSONObject result = new JSONObject();
		try {
			
			String raw = "timestamp="
					+ new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
					+ "&userprivilege=0&username=" + username + "&userroles="
					+ userrole + "";
			String token = encode0(raw, debugkey.getBytes(CHARSET));
			String digest = DigestUtils.md5Hex(token);
			
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put("X-Requested-With", "XMLHTTPRequest");
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put("ssotoken", token);
			paramsMap.put("ssochecksum", digest);
			
			String loginUrl = String.format(managerPath
			+ "/authmgr/auth!login.action?ssotoken=%s&ssochecksum=%s",
			token, digest);
			result.put("url", loginUrl);
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		return result;
	}
	

	public static void main(String[] args) throws Exception {
		String url = getManagerUrl("ssoTest3", "InfoDir-HK-CHATB-AdminUsers",
				"http://hfxgsc.demo.xiaoi.com/managerSC");
		System.out.println(url);
	}

}
