package com.xiaoi.south.app.mybatis.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.xiaoi.south.app.mybatis.entity.DefaultSqlBean;
import tk.mybatis.mapper.common.Mapper;

/**  
 * <p>Title: SqlMapper</p>  
 * <p>Description: </p>  
 * @author zfree  
 * @create_date 2018年7月13日
 */
public interface DefaultSqlMapper extends Mapper<DefaultSqlBean> {
	
	public Map<String, Object> selectCategoryNameById(@Param("id") String id);
	public List<Map<String, Object>> selectCategoryNames();
	
	public Map<String, Object> selectRoleByNameAndPwd(@Param("username") String username, @Param("password") String password);
	public Map<String, Object> selectRoleByAdsGroup(@Param("groupName") String groupName);
	public List<Map<String, Object>> selectRolesByAds();
}
