/**    
 * 文件名：SqlMapper.java    
 *
 * 版本信息：    
 * 日期：2016-9-21    
 * create by ziQi       
 * 2016-9-21
 */
package com.xiaoi.south.app.mybatis.mapper;

/** 
 * @title SqlMapper
 * @description 所有的mybatis的Mapper继承这个接口 
 * @author ziQi 
 * @version 上午12:51:22 
 * @create_date 2016-9-21上午12:51:22
 * @copyright (c) jacky
 */
public interface SqlMapper{

}
