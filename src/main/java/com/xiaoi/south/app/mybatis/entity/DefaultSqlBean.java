package com.xiaoi.south.app.mybatis.entity;

import java.io.Serializable;

/**  
 * <p>Title: Base</p>  
 * <p>Description: </p>  
 * @author zfree  
 * @create_date 2018年7月13日
 */
public class DefaultSqlBean implements Serializable {
	private String baseId;

	public String getBaseId() {
		return baseId;
	}

	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}
}
