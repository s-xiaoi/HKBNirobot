/**
 * 文件名：MenuIncreaseHandlerInterceptor.java
 * 
 * 版本信息：
 * 日期：2018-4-10
 * create by ziQi
 */
package com.xiaoi.south.app.interceptor;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSON;
import com.xiaoi.south.app.cache.CacheSupport;
import com.xiaoi.south.app.entity.Menu;
import com.xiaoi.south.app.entity.MenuMap;
import com.xiaoi.south.app.entity.RobotRequest;
import com.xiaoi.south.app.entity.RobotResponse;
import com.xiaoi.south.app.service.impl.DefaultMessageInterfaceResourceProxy;
import com.xiaoi.south.app.servlet.controller.BaseController;

/**
 * @title MenuIncreaseHandlerInterceptor
 * @description 序号自动增长等转换拦截器
 * @author ziQi
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class MenuIncreaseHandlerInterceptor extends HandlerInterceptorAdapter{

    @Autowired
    private DefaultMessageInterfaceResourceProxy defaultMessageResourceProxy;

    CacheSupport cacheSupport = new CacheSupport( "app_cache" );

    // Controller方法执行之前
    @Override
    public boolean preHandle( HttpServletRequest request, HttpServletResponse response, Object handler ) throws Exception{
        // 继续执行
        return super.preHandle( request, response, handler );
    }

    // Controller方法执行之后,显示视图之前
    @Override
    public void postHandle( HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView ) throws Exception{
        RobotRequest robotRequest = (RobotRequest) request.getAttribute( "robotRequest" );
        RobotResponse robotResponse = (RobotResponse) request.getAttribute( "robotResponse" );
        if( robotResponse==null ){ return; }
        String content = robotResponse.getContent();
        String platform = robotRequest.getPlatform();

        // 空白直接退出
		/*
		 * if( StringUtils.isBlank( content ) ){ return; }
		 */

        // 转人工
        int answerType = robotResponse.getType();
        if(robotRequest.getCustom3()!=null&&robotRequest.getCustom3().equals("2") ){
            answerType=0;
        }
        // 连续N次默认回复提示(连续默认回复和建议问)
        if( answerType == 0 ){
            int limitTimes = defaultMessageResourceProxy.getInteger( "x.defaultReply.limit.times", robotRequest.getPlatform() );
            if( limitTimes > 0 ){// N次默认回复次数不为零
                String uKey = robotRequest.getUserId() + "_defaultReplyTimes";
                Object uValue = cacheSupport.get( uKey );
                Integer defaultReplyTimes = 0;
                if( uValue != null ){
                    defaultReplyTimes = (Integer) uValue;
                }
                //判断默认回复次数，设置不同的话术
                if(robotRequest.getLang().equals("sc")) {
                	if(defaultReplyTimes==0) {
                    	content = defaultMessageResourceProxy.getString( "x.defaultReply.content.one.sc", robotRequest.getPlatform() );
                    }else if(defaultReplyTimes==1) {
                    	content = defaultMessageResourceProxy.getString( "x.defaultReply.content.two.sc", robotRequest.getPlatform() );
                    }else if(defaultReplyTimes>=2) {
                    	content = defaultMessageResourceProxy.getString( "x.defaultReply.content.three.sc", robotRequest.getPlatform() );
                    }
                }else if(robotRequest.getLang().equals("en")) {
                	if(defaultReplyTimes==0) {
                    	content = defaultMessageResourceProxy.getString( "x.defaultReply.content.one.en", robotRequest.getPlatform() );
                    }else if(defaultReplyTimes==1) {
                    	content = defaultMessageResourceProxy.getString( "x.defaultReply.content.two.en", robotRequest.getPlatform() );
                    }else if(defaultReplyTimes>=2) {
                    	content = defaultMessageResourceProxy.getString( "x.defaultReply.content.three.en", robotRequest.getPlatform() );
                    }
                }else {
                	if(defaultReplyTimes==0) {
                    	content = defaultMessageResourceProxy.getString( "x.defaultReply.content.one.tc", robotRequest.getPlatform() );
                    }else if(defaultReplyTimes==1) {
                    	content = defaultMessageResourceProxy.getString( "x.defaultReply.content.two.tc", robotRequest.getPlatform() );
                    }else if(defaultReplyTimes>=2) {
                        robotResponse.setType(101);
                    	content = defaultMessageResourceProxy.getString( "x.defaultReply.content.three.tc", robotRequest.getPlatform() );
                    }
                }
                
                if( defaultReplyTimes == 0 ){// 第一次默认回复
                    cacheSupport.put( uKey, 1 );
                }else{
                    defaultReplyTimes++;
                    cacheSupport.put( uKey, defaultReplyTimes );
                }
                
            }
        }else{// 遇到其它就清除连续N次统计次数
            String uKey = robotRequest.getUserId() + "_defaultReplyTimes";
            cacheSupport.remove( uKey );
        }

        if(content!=null) {
        // 处理内容，转换超链接
        String convertLlinkSwitch = defaultMessageResourceProxy.getValue( "x.convert.link.switch", platform, "true" );
        if( StringUtils.equalsIgnoreCase( "true", convertLlinkSwitch ) ){
            String convertLinkResultRegex = defaultMessageResourceProxy.getValue( "x.convert.link.source.regex", platform, "<a href=\"$1\" target=\"_blank\">$2</a>" );
            String convertLinkSrcRegex = defaultMessageResourceProxy.getValue( "x.convert.link.result.regex", platform, "\\[link\\s+url=[\'\"]+([^\\[\\]\'\"]+)[\'\"]+\\s*[^\\[\\]]*\\]([^\\[\\]]+)\\[\\/link\\]" );
            content = content.replaceAll( convertLinkSrcRegex, convertLinkResultRegex );
        }

        // 清除<HTML></HTML>标签
        String convertHtmlSwitch = defaultMessageResourceProxy.getValue( "x.convert.html.switch", platform, "true" );
        if( StringUtils.equalsIgnoreCase( "true", convertHtmlSwitch ) ){
            content = content.replaceAll( "<HTML>", "" );
            content = content.replaceAll( "</HTML>", "" );
        }

        // 去除多个换行和首尾换行
        String xMultipleLine = defaultMessageResourceProxy.getValue( "x.convert.text.multipleLine", platform, "true" );
        if( StringUtils.equals( xMultipleLine, "true" ) ){
            if( StringUtils.isNotEmpty( content ) ){
                content = content.trim();
                content = content.replaceAll( "\r\n", "\n" );
                content = content.replaceAll( "(\n)+", "\n" );
            }
        }else{
            if( StringUtils.isNotEmpty( content ) ){
                content = content.trim();
                content = content.replaceAll( "\r\n", "\n" );
            }
        }
            robotResponse.setContent(content);
        }

       
        this.writeJson(response, robotResponse);
    }
    
    /**
     * <pre>
     * <b>直接输出json(使用fastjons输出json字符串)</b>
     * </pre>
     * @param response
     * @param obj void 
     * @since 1.0    
    */
    protected String writeJson( HttpServletResponse response, Object obj ){
        if( obj != null ){
            String result = JSON.toJSONString( obj );
            return this.writeString( response, result, "text/json" );
        }
        return null;
    }
    
    /**
     * @Description <b>(直接输出字符串)</b></br>
     * @param response
     * @param str void
     * @since 2016-9-20
     */
    protected String writeString( HttpServletResponse response, String str, String type ){
        try{
            response.reset();
            response.setContentType( type );
            response.setCharacterEncoding( "utf-8" );
            // 解决跨域问题
            response.setHeader( "Access-Control-Allow-Origin", "*" );
            response.getWriter().print( str );
            return null;
        }catch( IOException e ){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @Description <b>(序号自动增长，重建菜单)</b></br>
     * @param content
     *        答案
     * @param srcRegex
     *        原始菜单格式
     * @param resultRegex
     *        新菜单格式
     * @param menuMap
     *        缓存菜单实例
     * @return String
     * @since 2016年2月16日
     */
    public static String rebuildMenu( String content, String srcRegex, String resultRegex, String faqvoteRegex, MenuMap menuMap ){
        // String resultStyle = "[link submit=\"{0}\"]{1}[/link]" ;
        // String srcRegex =
        // "\\[link\\s+submit=[\'\"]+([^\\[\\]\'\"]+)[\'\"]+\\s*[^\\[\\]]*\\]([^\\[\\]]+)\\[\\/link\\]";
        StringBuffer sb = new StringBuffer();
        Pattern pattern = Pattern.compile( srcRegex );
        Matcher matcher = pattern.matcher( content );
        while( matcher.find() ){
            if( matcher.groupCount() == 2 ){
                Menu menu = new Menu();
                String key = matcher.group( 1 );
                String alias = matcher.group( 2 );
                menu.setKey( key );
                menu.setAlias( alias );
                System.out.println( key + ":" + alias );
                if( StringUtils.startsWith( key, "faqvote" ) ){
                    // {0}=序号，{1}=标准问题，{2}=别名
                    Object[] params = new Object[] { menu.getKey(), menu.getKey(), menu.getAlias() };
                    matcher.appendReplacement( sb, MessageFormat.format( faqvoteRegex, params ) );
                }else{
                    // 缓存当前菜单
                	if(menuMap!=null)menuMap.put( menu, 100 );
                    // {0}=序号，{1}=标准问题，{2}=别名
                    Object[] params = new Object[] { menu.getIdx(), menu.getKey(), menu.getAlias() };
                    matcher.appendReplacement( sb, MessageFormat.format( resultRegex, params ) );
                }
            }
        }
        matcher.appendTail( sb );
        return sb.toString();
    }

}
