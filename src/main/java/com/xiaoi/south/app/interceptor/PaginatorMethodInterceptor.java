package com.xiaoi.south.app.interceptor;


import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.pagehelper.PageHelper;
import com.xiaoi.south.app.annotation.PaginatorRequired;
import com.xiaoi.south.app.entity.PageRequests;

/**
 * @title PaginatorHandlerInterceptor
 * @description 分页处理拦截器
 * @author ziQi
 * @version 下午10:13:59
 * @create_date 2016-9-21下午10:13:59
 * @copyright (c) jacky
 */
public class PaginatorMethodInterceptor implements MethodInterceptor{

    private static final Logger logger = LoggerFactory.getLogger( PaginatorMethodInterceptor.class );

    @Override
    public Object invoke( MethodInvocation invocation ) throws Throwable{
        //logger.info( "-----------PaginatorMethodInterceptor starting------------ " );
        Object[] args = invocation.getArguments();
        for( Object obj : args ){
            if( obj instanceof PageRequests ){
                PageRequests pageBean = (PageRequests) obj;
                PageHelper.startPage( pageBean.getPageNo(), pageBean.getPageSize() );
            }
        }
        // 判断该方法是否加了@LoginRequired 注解
        if( invocation.getMethod().isAnnotationPresent( PaginatorRequired.class ) ){
            logger.info( "-----------PaginatorMethodInterceptor @PaginatorRequired------------ " );
        }
        // 执行被拦截的方法，切记，如果此方法不调用，则被拦截的方法不会被执行。
        Object result =  invocation.proceed();
        //logger.info( "----------PaginatorMethodInterceptor end-------------------------" );
        return result;
    }

}
