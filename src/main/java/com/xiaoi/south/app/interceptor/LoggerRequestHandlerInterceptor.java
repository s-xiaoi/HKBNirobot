/**    
 * 文件名：AskHandlerInterceptor.java    
 *
 * 版本信息：    
 * 日期：2016年1月30日    
 * create by ziQi       
 * 2016年1月30日
 */
package com.xiaoi.south.app.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSON;
import com.xiaoi.south.app.entity.RobotRequest;

/**
 * @title LoggerRequestHandlerInterceptor
 * @description 主要打印各种日志
 * @author ziQi
 * @version 下午12:05:42
 * @create_date 2016年1月30日下午12:05:42
 * @copyright (c) jacky
 */
public class LoggerRequestHandlerInterceptor extends HandlerInterceptorAdapter{

    private static final Logger logger = LoggerFactory.getLogger( LoggerRequestHandlerInterceptor.class );

    @Override
    // Controller方法执行之前(主要一点要返回true)
    public boolean preHandle( HttpServletRequest request, HttpServletResponse response, Object handler ) throws Exception{
        logger.info( "request parameters#:{}", JSON.toJSONString( request.getParameterMap() ) );
        return true;
    }

    @Override
    // Controller方法执行之后
    public void postHandle( HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView ) throws Exception{
        RobotRequest robotRequest = (RobotRequest) request.getAttribute( "robotRequest" );
        logger.info( "request parameters#:{}", JSON.toJSONString( robotRequest ) );
    }

    @Override
    // //最后执行
    public void afterCompletion( HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex ) throws Exception{
        super.afterCompletion( request, response, handler, ex );
    }

    @Override
    // 额外提供了afterConcurrentHandlingStarted方法，该方法是用来处理异步请求
    public void afterConcurrentHandlingStarted( HttpServletRequest request, HttpServletResponse response, Object handler ) throws Exception{
        super.afterConcurrentHandlingStarted( request, response, handler );
    }

}
