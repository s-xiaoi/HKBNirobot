/**
 * 文件名：EhCacheManagerFactoryBean.java
 * 
 * 版本信息： 日期：2018年4月2日 create by ziQi
 */
package com.xiaoi.south.app.cache.ehcache;

import java.util.List;
import java.util.Map;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;
import net.sf.ehcache.config.FactoryConfiguration;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;

import com.xiaoi.south.app.modules.utils.IpUtils;

/**
 * @title EhCacheManagerFactoryBean
 * @description spring自定义的 EhCacheManagerFactory
 * @author ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class EhCacheManagerFactoryBean implements FactoryBean<CacheManager>,InitializingBean,DisposableBean{

    private static final Logger logger = LoggerFactory.getLogger( EhCacheManagerFactoryBean.class );

    private Resource configLocation;

    private String cacheManagerName;

    private boolean acceptExisting = false;

    private boolean shared = false;

    private CacheManager cacheManager;

    private boolean locallyManaged = true;

    private ManualRmiCacheConfig manualRmiCacheConfig;

    public ManualRmiCacheConfig getManualRmiCacheConfig(){
        return manualRmiCacheConfig;
    }

    public void setManualRmiCacheConfig( ManualRmiCacheConfig manualRmiCacheConfig ){
        this.manualRmiCacheConfig = manualRmiCacheConfig;
    }

    /**
     * Set the location of the EhCache config file. A typical value is
     * "/WEB-INF/ehcache.xml".
     * <p>
     * Default is "ehcache.xml" in the root of the class path, or if not found,
     * "ehcache-failsafe.xml" in the EhCache jar (default EhCache
     * initialization).
     * 
     * @see net.sf.ehcache.CacheManager#create(java.io.InputStream)
     * @see net.sf.ehcache.CacheManager#CacheManager(java.io.InputStream)
     */
    public void setConfigLocation( Resource configLocation ){
        this.configLocation = configLocation;
    }

    /**
     * Set the name of the EhCache CacheManager (if a specific name is desired).
     * 
     * @see net.sf.ehcache.config.Configuration#setName(String)
     */
    public void setCacheManagerName( String cacheManagerName ){
        this.cacheManagerName = cacheManagerName;
    }

    /**
     * Set whether an existing EhCache CacheManager of the same name will be
     * accepted for this EhCacheManagerFactoryBean setup. Default is "false".
     * <p>
     * Typically used in combination with {@link #setCacheManagerName
     * "cacheManagerName"} but will simply work with the default CacheManager
     * name if none specified. All references to the same CacheManager name (or
     * the same default) in the same ClassLoader space will share the specified
     * CacheManager then.
     * 
     * @see #setCacheManagerName #see #setShared
     * @see net.sf.ehcache.CacheManager#getCacheManager(String)
     * @see net.sf.ehcache.CacheManager#CacheManager()
     */
    public void setAcceptExisting( boolean acceptExisting ){
        this.acceptExisting = acceptExisting;
    }

    /**
     * Set whether the EhCache CacheManager should be shared (as a singleton at
     * the ClassLoader level) or independent (typically local within the
     * application). Default is "false", creating an independent local instance.
     * <p>
     * <b>NOTE:</b> This feature allows for sharing this
     * EhCacheManagerFactoryBean's CacheManager with any code calling
     * <code>CacheManager.create()</code> in the same ClassLoader space, with no
     * need to agree on a specific CacheManager name. However, it only supports
     * a single EhCacheManagerFactoryBean involved which will control the
     * lifecycle of the underlying CacheManager (in particular, its shutdown).
     * <p>
     * This flag overrides {@link #setAcceptExisting "acceptExisting"} if both
     * are set, since it indicates the 'stronger' mode of sharing.
     * 
     * @see #setCacheManagerName
     * @see #setAcceptExisting
     * @see net.sf.ehcache.CacheManager#create()
     * @see net.sf.ehcache.CacheManager#CacheManager()
     */
    public void setShared( boolean shared ){
        this.shared = shared;
    }

    @Override
    public void afterPropertiesSet() throws CacheException{

        Configuration configuration = (this.configLocation != null ? EhCacheManagerUtils.parseConfiguration( this.configLocation ) : ConfigurationFactory.parseConfiguration());
        if( this.cacheManagerName != null ){
            configuration.setName( this.cacheManagerName );
        }

        // 初始化RMI集群参数
        if( this.manualRmiCacheConfig != null && !this.manualRmiCacheConfig.isDeveloping() ){
            // 初始化PeerListener
            if( StringUtils.isNotBlank( this.manualRmiCacheConfig.getCacheManagerPeerListenerFactoryClass() ) ){
                // hostName=localhost, port=40001,socketTimeoutMillis=2000
                StringBuilder sb = new StringBuilder();
                sb.append( "hostName=" ).append( this.manualRmiCacheConfig.getHostName() ).append( ",port=" ).append( this.manualRmiCacheConfig.getPort() ).append( ",socketTimeoutMillis=" ).append( this.manualRmiCacheConfig.getSocketTimeoutMillis() );
                String properties = sb.toString();
                logger.info( "CacheManagerPeerListener Properties {}", properties );
                // System.out.println( properties );
                configuration.cacheManagerPeerListenerFactory( new FactoryConfiguration<FactoryConfiguration<?>>().className( this.manualRmiCacheConfig.getCacheManagerPeerListenerFactoryClass() ).properties( properties ) );
            }
            ;
            // 初始化peerProvider
            if( StringUtils.isNotBlank( this.manualRmiCacheConfig.getCacheManagerPeerProviderFactoryClass() ) ){
                // peerDiscovery=manual,rmiUrls=//10.143.18.93:40002/app_cache|//10.143.18.93:40002/csr_cache
                StringBuilder sb = new StringBuilder();

                // Map<String,String> peerUrls = new HashMap<String,String>();
                // 所有节点ip地址列表
                List<String> providerHosts = this.manualRmiCacheConfig.getProviderHosts();

                // 所有需要集群同步的cacheName
                List<String> syncCacheNames = this.manualRmiCacheConfig.getSyncCacheNames();
                sb.append( "peerDiscovery=" ).append( this.manualRmiCacheConfig.getPeerDiscovery() ).append( ",rmiUrls=" );
                // 获取本机所有IP
                Map<String, String> allAddrIp = IpUtils.getHostIps();
                // rmiUrls=//10.143.18.94:40001/app_cache
                if( providerHosts != null && providerHosts.size() > 0 ){// 拼接rmiUrl（创建除掉本机IP以外的provider节点）
                    int size = providerHosts.size();
                    int providerPort = this.manualRmiCacheConfig.getProviderPort();
                    for( int i = 0; i < size; i++ ){
                        String theHost = providerHosts.get( i );
                        if( allAddrIp.get( theHost ) == null ){
                            String rmiBase = "//" + theHost + ":" + providerPort;
                            for( int k = 0; k < syncCacheNames.size(); k++ ){
                                sb.append( rmiBase ).append( "/" ).append( syncCacheNames.get( k ) ).append( "|" );
                            }
                        }
                    }
                }
                String properties = sb.toString();
                logger.info( "CacheManagerPeerProvider Properties {}", properties );
                configuration.cacheManagerPeerProviderFactory( new FactoryConfiguration<FactoryConfiguration<?>>().className( this.manualRmiCacheConfig.getCacheManagerPeerProviderFactoryClass() ).properties( properties ) );
            }
        }

        if( this.shared ){
            // Old-school EhCache singleton sharing...
            // No way to find out whether we actually created a new CacheManager
            // or just received an existing singleton reference.
            this.cacheManager = CacheManager.create( configuration );
        }else if( this.acceptExisting ){
            // EhCache 2.5+: Reusing an existing CacheManager of the same name.
            // Basically the same code as in CacheManager.getInstance(String),
            // just storing whether we're dealing with an existing instance.
            synchronized( CacheManager.class ){
                this.cacheManager = CacheManager.getCacheManager( this.cacheManagerName );
                if( this.cacheManager == null ){
                    this.cacheManager = new CacheManager( configuration );
                }else{
                    this.locallyManaged = false;
                }
            }
        }else{
            // Throwing an exception if a CacheManager of the same name exists
            // already...
            this.cacheManager = new CacheManager( configuration );
        }
    }

    @Override
    public CacheManager getObject(){
        return this.cacheManager;
    }

    @Override
    public Class<? extends CacheManager> getObjectType(){
        return (this.cacheManager != null ? this.cacheManager.getClass() : CacheManager.class);
    }

    @Override
    public boolean isSingleton(){
        return true;
    }

    @Override
    public void destroy(){
        if( this.locallyManaged ){
            this.cacheManager.shutdown();
        }
    }

}
