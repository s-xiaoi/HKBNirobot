/**    
 * 文件名：ManualRMICacheConfig.java    
 *
 * 版本信息：    
 * 日期：2018年4月1日    
 * create by ziQi
 */
package com.xiaoi.south.app.cache.ehcache;

import java.util.List;

/**
 * @title ManualRMICacheConfig
 * @description echache RMI集群自定义参数类
 * @author ziQi
 * @email u36369@163.com
 * @since 1.0
 * @copyright (c) 池塘上的树
 */
public class ManualRmiCacheConfig{
    
    private String hostName;

    private Integer port;

    private Integer remoteObjectPort;

    private Integer socketTimeoutMillis;

    private String peerDiscovery;

    private List<String> providerHosts;
    
    private List<String> syncCacheNames;
    
    private Integer providerPort;
    
    private String cacheManagerPeerListenerFactoryClass;
    
    private String cacheManagerPeerProviderFactoryClass;
    
    private String cacheEventListenerFactoryClass;
    
    private String replicateAsynchronously;
    
    private String replicatePuts;
    
    private String replicatePutsViaCopy;
    
    private String replicateUpdates;

    private String replicateUpdatesViaCopy;
    
    private String replicateRemovals;
    
    private boolean developing;

    public String getHostName(){
        return hostName;
    }

    public void setHostName( String hostName ){
        this.hostName = hostName;
    }

    public Integer getPort(){
        return port;
    }

    public void setPort( Integer port ){
        this.port = port;
    }

    public Integer getRemoteObjectPort(){
        return remoteObjectPort;
    }

    public void setRemoteObjectPort( Integer remoteObjectPort ){
        this.remoteObjectPort = remoteObjectPort;
    }

    public Integer getSocketTimeoutMillis(){
        return socketTimeoutMillis;
    }

    public void setSocketTimeoutMillis( Integer socketTimeoutMillis ){
        this.socketTimeoutMillis = socketTimeoutMillis;
    }

    public String getPeerDiscovery(){
        return peerDiscovery;
    }

    public void setPeerDiscovery( String peerDiscovery ){
        this.peerDiscovery = peerDiscovery;
    }

    public List<String> getProviderHosts(){
        return this.providerHosts;
    }

    public void setProviderHosts( List<String> providerHosts ){
        this.providerHosts = providerHosts;
    }

    public Integer getProviderPort(){
        return providerPort;
    }

    public void setProviderPort( Integer providerPort ){
        this.providerPort = providerPort;
    }

    public String getCacheManagerPeerListenerFactoryClass(){
        return cacheManagerPeerListenerFactoryClass;
    }

    public void setCacheManagerPeerListenerFactoryClass( String cacheManagerPeerListenerFactoryClass ){
        this.cacheManagerPeerListenerFactoryClass = cacheManagerPeerListenerFactoryClass;
    }

    public String getCacheManagerPeerProviderFactoryClass(){
        return cacheManagerPeerProviderFactoryClass;
    }

    public void setCacheManagerPeerProviderFactoryClass( String cacheManagerPeerProviderFactoryClass ){
        this.cacheManagerPeerProviderFactoryClass = cacheManagerPeerProviderFactoryClass;
    }
    public List<String> getSyncCacheNames(){
        return syncCacheNames;
    }

    public void setSyncCacheNames( List<String> syncCacheNames ){
        this.syncCacheNames = syncCacheNames;
    }

    public String getCacheEventListenerFactoryClass(){
        return cacheEventListenerFactoryClass;
    }

    public void setCacheEventListenerFactoryClass( String cacheEventListenerFactoryClass ){
        this.cacheEventListenerFactoryClass = cacheEventListenerFactoryClass;
    }

    public String getReplicateAsynchronously(){
        return replicateAsynchronously;
    }

    public void setReplicateAsynchronously( String replicateAsynchronously ){
        this.replicateAsynchronously = replicateAsynchronously;
    }

    public String getReplicatePuts(){
        return replicatePuts;
    }

    public void setReplicatePuts( String replicatePuts ){
        this.replicatePuts = replicatePuts;
    }

    public String getReplicatePutsViaCopy(){
        return replicatePutsViaCopy;
    }

    public void setReplicatePutsViaCopy( String replicatePutsViaCopy ){
        this.replicatePutsViaCopy = replicatePutsViaCopy;
    }

    public String getReplicateUpdates(){
        return replicateUpdates;
    }

    public void setReplicateUpdates( String replicateUpdates ){
        this.replicateUpdates = replicateUpdates;
    }

    public String getReplicateUpdatesViaCopy(){
        return replicateUpdatesViaCopy;
    }

    public void setReplicateUpdatesViaCopy( String replicateUpdatesViaCopy ){
        this.replicateUpdatesViaCopy = replicateUpdatesViaCopy;
    }

    public String getReplicateRemovals(){
        return replicateRemovals;
    }

    public void setReplicateRemovals( String replicateRemovals ){
        this.replicateRemovals = replicateRemovals;
    }

    public boolean isDeveloping(){
        return developing;
    }

    public void setDeveloping( boolean developing ){
        this.developing = developing;
    }
    
}
