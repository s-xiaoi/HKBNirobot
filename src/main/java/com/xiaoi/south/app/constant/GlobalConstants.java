package com.xiaoi.south.app.constant;

/**  
 * <p>Title: GlobalConstants</p>  
 * <p>Description: 全局常量定义</p>  
 * @author zfree  
 * @create_date 2018-4-20
 */
public class GlobalConstants {

	// ---------------------定制化服务参数名称常量---------------------------
	/** 产品应用访问地址 **/
	public static final String ROBOT_URL = "hsbcrobot.robot.url";
	/** 问题语言识别开关 **/
	public static final String QLR_POWER = "hsbcrobot.qlr.power";
	/** 问题语言识别开关 **/
	public static final String QLR_TIP_POWER = "hsbcrobot.qlr.tip.power";
	/** 问题语言识别提示语，其中 {0} 表示为需要替换的语言标识 **/
	public static final String QLR_TIP = "hsbcrobot.{0}.qlr.tip";
	/** 繁简英语言参数值：0=tc、1=sc、2=en **/
	public static final String [] LANG = {"tc","sc","en"};
	/** 欢迎语标识，用于约定欢迎语问题不入库 **/
	public static final String  SYSTEM_WELCOME = "system_welcome";
	
	// ---------------------产品robot常量---------------------------	
	/** 定制热点问模式：1=动态模式；2=定制模式；3=混合模式； **/
	public static final int [] HOT_QUESTIONS_MODE = {1,2,3};
	/** 产品问答接口默认不入库标识：适用于输入参数 userId (7+版本都支持,7以下的不一定) **/
	public static final String  USERID_TASK = ":task:";
		
	// ---------------------getQuestions接口类型常量定义---------------------------
	/** 热点问题类型 **/
	public static final String HOT_QUESTIONS = "hotQuestions";
	/** 相关问类型 **/
	public static final String RELATED_QUESTIONS = "relatedQuestions";
	/** 智能提示类型 **/
	public static final String SUGGESTED_QUESTIONS = "suggestedQuestions";
		
	// ---------------------缓存对象常量定义---------------------------
	/** 序号自增长缓存 **/
	public static final String APP_CACHE = "app_cache";
	/** 其他缓存 **/
	public static final String OTHER_CACHE = "other_cache";
	/** 科学园公司、商店、活动缓存 **/
	public static final String SCIPARK_CACHE = "SCIPARK_CACHE";
	/** 活动流程缓存 **/
	public static final String EVENT_CACHE = "EVENT_CACHE";
	
	// ---------------------xml配置文件常量定义---------------------------
	/** 产品繁体的wsdl问答接口地址ID **/
	public static final String IROBOT_TC_WS_URLS = "irobot.tc.ask.ws.urls";
	/** 产品简体的wsdl问答接口地址ID **/
	public static final String IROBOT_SC_WS_URLS = "irobot.sc.ask.ws.urls";
	/** 产品英文的wsdl问答接口地址ID **/
	public static final String IROBOT_EN_WS_URLS = "irobot.en.ask.ws.urls";
	/** 产品繁体的wsdl热点问题、智能提示、相关问接口地址ID **/
	public static final String IROBOT_TC_QUESTION_WS_URLS = "irobot.tc.question.ws.urls";
	/** 产品简体的wsdl热点问题、智能提示、相关问接口地址ID **/
	public static final String IROBOT_SC_QUESTION_WS_URLS = "irobot.sc.question.ws.urls";
	/** 产品英文的wsdl热点问题、智能提示、相关问接口地址ID **/
	public static final String IROBOT_EN_QUESTION_WS_URLS = "irobot.en.question.ws.urls";
	/** outlook效果展示文字0=sc、1=tc、2=en **/
	public static final String[] tdStartTime= {"开始时间","開始時間","Start time"};
	public static final String[] tdendTime= {"结束时间","結束時間","End time"};
	public static final String[] tdOrganisers= {"举办方","舉辦方","Organisers"};
	public static final String[] tdTheme= {"主题","主題","Theme"};
	public static final String[] tdAddress= {"举办地址","舉辦地址","Venue"};
	public static final String[] tdGuest= {"嘉宾","嘉賓","Guest"};
	public static final String[] tdUrl= {"网址","網址","URL"};
	public static final String[] defaultOutlook= {"对不起，我们找不到你查询的活动。请致电我们的服务热线:2788 5555","對不起，我們找不到你查詢的活動。請致電我們的服務熱線:2788 5555","I am sorry we cannot find your requested activity.  Please call our service hotline at 2788 5555."};

	public static final String IROBOT_MANAGER_URLS="";
}
