package com.xiaoi.south.app.servlet.controller;


import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.xiaoi.south.app.modules.utils.*;
import com.xiaoi.south.app.service.impl.DefaultMessageInterfaceResourceProxy;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.xiaoi.south.app.cache.CacheSupport;
import com.xiaoi.south.app.constant.GlobalConstants;
import com.xiaoi.south.app.entity.LoginUser;
import com.xiaoi.south.app.service.impl.DefaultMessageInterfaceResourceProxy;
import com.xiaoi.south.app.service.impl.DefaultSqlServiceImpl;
import com.xiaoi.south.app.service.impl.ManagerSsoService;

/**  
* <p>Title: SSOController</p>  
* <p>Description: 对接第三方统一登录系统</p>  
* @author zfree  
* @create_date 2019年5月27日
*/
@Controller
public class SsoController extends BaseController{

    private static final Logger logger = LoggerFactory.getLogger( SsoController.class );
    private static final String privateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC0qzb7MDta5Ce2t1/P/E/toveprEwa9fvjx2uNdmWriL1WOyc+VIuyAUDXXddeQEsI4trWiJvfp7rpQ+oNgZa0z7lkg6Ak/jwkZuNj3PelyWTGHG3lgAzJic1csQJY7EbWqTSV+HXZSbYMyfAXiBbYqHkkF548G2ukiXkL/TSNTrhVLMSH7LFPfCyGoNmjSHcYF19XpWdhMcudwjDTEDsZkV0/rLx4gCHlb3OqvzBEaAQ2EKxHLqf9p9eu89uCbOX+eBbSMr95crLunxrgz5cA2UaMY7uIy8izgn4IaeFbzYG6uW7/PLa1feyx2UWBUn5pfZ6KLhYgAcm6Kxi5LLlvAgMBAAECggEAWefd63AcCWYCUU29kz2m9MWlYz0Hfl8sozQo1HO3sn3QYqn7JGxkPAedoc7kDtYFwP1LE3lVwB2yyMPwJBp5ya6ZPmi199LAUQkWXSqn3ktzJ5ccJRnswxodJOQ9G80wj0ASmBLVanTg8EpJfRxO6jAkYI6UQqsOjQmQQCQX070glhES/h4YTsZ3I59YhWXCfmNqrXB218yCpXgRp0+bGf7pIl7D+PpXgyphnLFzXoZLupZQ0yCOE0t/U2DgXwQ40zAjS/KK2kJKWpYF34bYb4twFc7SPyPw/CQnbQz3T01yeFwF/vMlDlR+186cobYprakCe9IDEOTSD2qj/j9HcQKBgQD/J6GWADMfVqqFV9RXqK1/fxVe5IC0sjfZHGBTrbDYcRTh9iudDcK0xVjO/8ooDr7IxRrd3NBy2Wp/bEKd3IqspGKvYUtIggtaAArmRaokSCj5gJFh2I7N9FBg7JWAzfRwVep85IW2rfecJrjHVXO+EBuDPsIOKJeo8tRvWlL5iQKBgQC1RGuQH4U1t1zX559ZAYDCE+b/nmzTdiRrC2OM8X4K9RU1+QHYm4HaAOscZcsnX3KAqQOYSfAtf0hydqvHiRIls3e3hbT6OsCdzjpGyhDT038EGwDr+XVEqIhkc9AismfSt0vYo+7hr9UFgagdhjCImjtB9pZPZBf1XkwPtwH1NwKBgQC+MvezzkBNixgfwAHsujNiyAPRMbpzeCpDcHCpz9706Q6tBgkq0LgkcrupymkShTNYi/1kBxdnlYRaqgPakyzYVs2teHRa/32gwElfr+yNkTDiFCHLc1e4VPeZ2KH1WxFsr87U/LBkJbhfMUoqZRubQXyQ0DZtwXqUGTecLhOOKQKBgDX4djJGQEpbGwipzTcaDHU8qcWoLOzVO0in7m8TpBytFjCowFT3EDWRmm8tG8zQTW5jZE+ejCVvxWlXThQuCrow8k8xP05V2e3iblWPam03hpT5WU/pSXI0389Q8EK0bdc+fhW10bVevgVSsnR/9MlnaodcXSqsObnz5p+zNKEHAoGBAMY1JKgfblyVd6psloyLEAbub1sR9kuv4GMMO2/CRZTcmk6HP+6qjbMd1Ib00JcptReOdZ5RvZ2WIKXuGPO0lEyes1O5VYYTrxwN0KOef7Vx1KwSGqzo76FjHOrjed+lhYbKNULrQ49iJJpC6cZjE0h+7IvBfvL0l9Yvs1sUbcoT";
	

    CacheSupport cacheSupport = new CacheSupport( "role_cache" );

    @Autowired
    private DefaultMessageInterfaceResourceProxy defaultMessageResourceProxy;
    
    @Autowired
    private DefaultSqlServiceImpl defaultSqlServiceImpl;
    
    private static Map<String, String> managerUrlMap = null;

    /**
     * 	集成kb与manager的SSO登录
     * @param user
     * @param request
     * @param response
     * @param session
     * @throws IOException 
     */
    @RequestMapping( value = "/imanager/validLogin", method=RequestMethod.POST, produces = { "text/html;charset=UTF-8", "application/json;charset=UTF-8", "application/xml;charset=UTF-8" } )
    public void managerSsoLogin(@RequestBody LoginUser user, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException{
    	
    	if (user != null && StringUtils.isNotBlank(user.getLang())) {
			JSONObject result = new JSONObject();
    		if (!user.getCaptcha().equals(request.getSession().getAttribute("captcha"))) {// 验证码不正确
    			result.put("msg", "errorCaptcha");
    			this.writeJson(response, result);
    		} else {
        		String lang = user.getLang();
        		//通过私匙将密码解密
        		String password = user.getUserPwd();
        		try {
        			password = SecurityUtil.decryptRSADefault(privateKey,user.getUserPwd());
        			user.setUserPwd(password);
				} catch (Exception e) {
					e.printStackTrace();
					}
				if(!LdapUtils.ldapConn("",user.getUserId(),user.getUserPwd())){

					if(user.getType().equals("1")){ //1是manager管理后台  2是知识库
						String roleName=getUserRoleByXiaoI(user);//获取角色名
						if(roleName==null||"".equals(roleName)){
							result.put("msg", "Role error");
							this.writeJson(response, result);
							return;
						}

						String managerUrl=SSoUtils.getManagerUrl(user.getUserId(),roleName,PropertiesUtils.getString("manager.url"));
	//					response.sendRedirect(managerUrl);
						result.put("url",managerUrl);
						this.writeJson(response, result);
					}else {

						String kbUrl="";
						kbUrl=SSoUtils.KBsso(user.getUserId(),PropertiesUtils.getString("kb.url"));
						result.put("url",kbUrl);
						this.writeJson(response, result);
					}

				}else{
					result.put("msg", "ldap connection faile");
					this.writeJson(response, result);
					return;
				}




//    			this.writeJson(response, result);
    		}
    	}
    	
//    	
    	
    }

    /**
     * 	manager根据语言的SSO登录
     * @param lang
     * @param request
     * @param response
     * @param session
     * @throws IOException
     */
    @RequestMapping( value = "/imanager/login", method=RequestMethod.GET, produces = { "text/html;charset=UTF-8", "application/json;charset=UTF-8", "application/xml;charset=UTF-8" } )
    public void loginInManager(@RequestParam(required = false) String lang, @RequestParam(required = false) String ssotoken,
    		HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException{
    	
    	if (StringUtils.isNotBlank(lang) && StringUtils.isNoneBlank(ssotoken)) {
        	String userRole = null;
        	String userName = null;
        	try {
				String loginInfoStr = ManagerSsoService.decode(ssotoken);
				String [] loginInfo = loginInfoStr.split("&");
				for (String info : loginInfo) {
					String [] params = info.split("=");
					if ("username".equals(params[0])) {
						userName = params[1];
					}
					if ("userroles".equals(params[0])) {
						userRole = params[1];
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
        	
        	// 获取manager的URL
        	if (managerUrlMap==null) {
        		managerUrlMap = (Map<String, String>) SpringContextUtils.getBean(GlobalConstants.IROBOT_MANAGER_URLS);
        	}
    		String managerUrl = managerUrlMap.get(lang);
    		
    		JSONObject result = ManagerSsoService.getManagerLoginInfo(userName, userRole, managerUrl);
    		response.sendRedirect(result.getString("url"));
		
    	}
    	
    }

	/**
	 * 验证码生成
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
    @RequestMapping(value = "/imanager/captcha", method = RequestMethod.GET)
    @ResponseBody
	public void captcha(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "no-cache, no-store");
		response.setHeader("Pragma", "no-cache");
		
		CaptchaUtil.outputCaptchaByImanager(request, response);
	}     
    
	/**
	 * 测试公司环境SSO登录
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
    @RequestMapping(value = "/getManagerUrl", method = RequestMethod.GET)
    @ResponseBody
	public void getManagerUrl( @RequestParam(required = false) String userName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
		JSONObject result = ManagerSsoService.getManagerLoginInfo(userName, "InfoDir-HK-CHATB-AdminUsers", "http://hfxgsc.demo.xiaoi.com/managerSC");
		response.sendRedirect(result.getString("url"));
	}  

	private String getUserRoleByXiaoI(LoginUser user) {
		String userRole = null;
		String username = user.getUserId();
		String password = DigestUtils.sha1Hex(user.getUserPwd());
		
		Map<String, Object> map = defaultSqlServiceImpl.getRoleNameByUsernameAndPwd(username, password);
		if (map != null) {
			userRole = map.get("name")==null?"":map.get("name").toString();
		}
		return userRole;
	}

	private Map<String, String> getUserRoleByADS(LoginUser user)  throws Exception {
		String userId = user.getUserId();
		String userPwd = user.getUserPwd();
		String ldapUrl = defaultMessageResourceProxy.getString("hsbcrobot.ldap.url", "all");
		String ldapPassword = defaultMessageResourceProxy.getString("hsbcrobot.ldap.password", "all");
		String ldapServiceAccountDn = defaultMessageResourceProxy.getString("hsbcrobot.ldap.serviceAccountDn", "all");
		String ldapNormalUserDn = defaultMessageResourceProxy.getString("hsbcrobot.ldap.normalUserDn", "all");
		String ldapSearchFilter = defaultMessageResourceProxy.getString("hsbcrobot.ldap.searchFilter", "all");
		List<Map<String, Object>> listRoles = getRoleByCache();
		Map<String, String> resultRole = LdapUtil.getUserRole(listRoles, ldapUrl, ldapServiceAccountDn, ldapPassword, ldapNormalUserDn,
				ldapSearchFilter, userId, userPwd);
		return resultRole;
	}

	private List<Map<String, Object>> getRoleByCache() {
		List<Map<String, Object>> roles = (List<Map<String, Object>>) cacheSupport.get("roles");
		if (roles == null) {
			roles = defaultSqlServiceImpl.getRolesByAds();
			cacheSupport.put("roles", roles);
		}
		return roles;
	}

	public DefaultMessageInterfaceResourceProxy getDefaultMessageResourceProxy(){
        return defaultMessageResourceProxy;
    }

    public void setDefaultMessageResourceProxy( DefaultMessageInterfaceResourceProxy defaultMessageResourceProxy ){
        this.defaultMessageResourceProxy = defaultMessageResourceProxy;
    }

	public DefaultSqlServiceImpl getDefaultSqlServiceImpl() {
		return defaultSqlServiceImpl;
	}

	public void setDefaultSqlServiceImpl(DefaultSqlServiceImpl defaultSqlServiceImpl) {
		this.defaultSqlServiceImpl = defaultSqlServiceImpl;
	}

}
