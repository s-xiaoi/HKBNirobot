package com.xiaoi.south.app.servlet.view;

import java.util.Locale;
import java.util.Map;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

/**
 * @title MultiViewResover
 * @description MVC多视图
 * @author ziQi
 * @version 上午3:12:16
 * @create_date 2015年12月17日上午3:12:16
 * @copyright (c) jacky
 */
public class MultiViewResover implements ViewResolver{

    private Map<String, ViewResolver> resolvers;

    public static final String DEFAULT_VIEW_SUFFIX = "jsp";

    @Override
    public View resolveViewName( String viewName, Locale locale ) throws Exception{
        int n = viewName.lastIndexOf( "." ); // 获取
         // viewName(modelAndView中的名字)看其有没有@
        // 默认为jsp
        String suffix = DEFAULT_VIEW_SUFFIX;
        // 有的话截取@后面的字符串 这里一般是jsp,ftl,vm与配置文件中的<entry key="ftl">的key匹配
        if( n != -1 ){
            suffix = viewName.substring( n + 1 );
            // 取@前面的部分那时真正的资源名.比如我们要使用say.jsp 那viewName就应该是say@jsp
            viewName = viewName.substring( 0, n );
        }

        // 获取托管的视图解析类对象
        ViewResolver resolver = resolvers.get( suffix );
        if( resolver != null ){
            return resolver.resolveViewName( viewName, locale );
        }
        return null;
    }

    public Map<String, ViewResolver> getResolvers(){
        return resolvers;
    }

    public void setResolvers( Map<String, ViewResolver> resolvers ){
        this.resolvers = resolvers;
    }
}