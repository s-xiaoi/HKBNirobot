package com.xiaoi.south.app.servlet.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xiaoi.south.app.service.impl.AskServiceProxy;

@Controller
public class WhatsAppController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(WhatsAppController.class);
	
	@Autowired
	private AskServiceProxy askServiceProxy;
	
//	@Autowired
//	private TaskPool tasks=new TaskPool();
	
	/**
	 * 1，用户在WhatsApp发信息给HKPC，
	 * 2，由第三方EMMA转发调用此方法
	 * 3，此方法返回200，并异步调用机器人方法
	 * 4，最后组装对应的报文格式请求EMMA提供的接口
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/wa/messages")
	public void messages(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("come on whatsApp...");
		logger.info("httpBody="+getBodyMsg(request));
		//获取用户问题
		
		//this.askServiceProxy.askToWhatsApp(robotRequest);
		JSONObject message=new JSONObject();
		message.put("code", "0");
		message.put("message", "success");
		JSONArray ja=new JSONArray();
		message.put("data", ja);
		
		
		this.writeJson(response, message);
	}
	
	/**，，，
	 * 通过request.getReader()的方式来获取body
	 * 获取body内容，封装成机器人可识别的request对象
	 * @param request
	 * @return  robotRequest
	 */
	 public static String getBodyMsg(HttpServletRequest request)
	    {
	 
	        BufferedReader br = null;
	        StringBuilder sb = new StringBuilder("");
	        try
	        {
	            br = request.getReader();
	            String str;
	            while ((str = br.readLine()) != null)
	            {
	                sb.append(str);
	            }
	            br.close();
	        }
	        catch (IOException e)
	        {
	            e.printStackTrace();
	        }
	        finally
	        {
	            if (null != br)
	            {
	                try
	                {
	                    br.close();
	                }
	                catch (IOException e)
	                {
	                    e.printStackTrace();
	                }
	            }
	        }
	        
	        return sb.toString();
	    }
	public static void main(String[] args) {
		String val="{\"messages\": [{\"profile\":{\"name\":\"Sky\"},\"wa_id\":\"85269930642\",\"from\":\"85269930642\",\"id\":\"ABGGhSaZMGQvAhBHll8_ZO8KMNmTRj8FFLxW\",\"video\":{\"sha256\":\"f451ca1cae78ffe6a43419136b0d728b6f18fe6b09563410ce28e92cb073a69b\",\"mime_type\":\"video\\/mp4\",\"id\":\"b91251b8-3dcb-43e4-ab63-a0559166ddcb\"},\"type\":\"video\",\"sequence\":\"417241\",\"timestamp\":\"1568772033\"}]}";
		
		JSONObject json=JSONObject.parseObject(val);
		JSONObject jsonMessage=JSONObject.parseObject(json.get("messages").toString().substring(1, json.get("messages").toString().length()-1));
		if(jsonMessage.get("text")!=null) {
			String body=jsonMessage.get("text").toString().substring(9,jsonMessage.get("text").toString().length()-2);
			System.out.println("body=="+body+"==");
		}else {
			System.out.println("非文本消息");
		}
	}
}
