package com.xiaoi.south.app.servlet.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import com.xiaoi.south.app.modules.utils.LdapUtils;
import com.xiaoi.south.app.service.WSServer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.xiaoi.south.app.cache.CacheSupport;
import com.xiaoi.south.app.constant.GlobalConstants;
import com.xiaoi.south.app.entity.Menu;
import com.xiaoi.south.app.entity.MenuMap;
import com.xiaoi.south.app.entity.RobotRequest;
import com.xiaoi.south.app.entity.RobotResponse;
import com.xiaoi.south.app.modules.utils.FaceBookUtils;
import com.xiaoi.south.app.service.impl.AskServiceProxy;
import com.xiaoi.south.app.service.impl.DefaultMessageInterfaceResourceProxy;

/**
 * <pre>
 * 对接问答
 * </pre>
 *
 * @porter ziQi
 * @email u36369@163.com
 * @copyright (c) 池塘上的树
 * @since 1.0
 */
@Controller
public class IBotController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(IBotController.class);



    @Autowired
    private DefaultMessageInterfaceResourceProxy defaultMessageInterfaceResourceProxy;

    @Autowired
    private AskServiceProxy askServiceProxy;

    @Autowired
    private LdapUtils ldap;


    /**
     * @param robotRequest
     * @param response
     * @param session
     * @param modelAndView void
     * @Description <b> #(二次开发问答接口入口)</b>
     * @since 1.0
     */
    @RequestMapping(value = "/irobot/ask4Json", produces = {"text/html;charset=UTF-8",
            "application/json;charset=UTF-8", "application/xml;charset=UTF-8"})
    public void ask4Json(@RequestBody RobotRequest robotRequest, HttpServletRequest request,
                         HttpServletResponse response, HttpSession session, ModelAndView modelAndView) {
        //判断消息类型，是否为转人工消息
           // 序号自动增长
           this.menu2Faq(robotRequest,defaultMessageInterfaceResourceProxy);
           RobotResponse robotResponse = new RobotResponse();
           robotResponse = this.askServiceProxy.ask4Robot(robotRequest);


           // <!- start ->
           /*
            * 统一约定注册到HttpServletRequest中，供handler使用；
            */
           request.setAttribute("robotRequest", robotRequest);
           request.setAttribute("robotResponse", robotResponse);

           // <!- end ->
           // write json
           // this.writeJson(response, robotResponse);
    }

    @RequestMapping(value = "/irobot/ask")
    public void askTemp(RobotRequest robotRequest, HttpServletRequest request, HttpServletResponse response,
                        HttpSession session, ModelAndView modelAndView) {
        // 序号自动增长
        this.menu2Faq(robotRequest,defaultMessageInterfaceResourceProxy);
        robotRequest.setLang("sc");
//        robotRequest.setUserId("testWelab");
//		robotRequest.setPlatform("web");
        RobotResponse robotResponse = this.askServiceProxy.ask4Robot(robotRequest);
        // <!- start ->
        /*
         * 统一约定注册到HttpServletRequest中，供handler使用；
         */
        request.setAttribute("robotRequest", robotRequest);
        request.setAttribute("robotResponse", robotResponse);
        // <!- end ->
        // write json
        this.writeJson(response, robotResponse);
    }

    @RequestMapping(value = "/irobot/ldap")
    public void LdapTemp(HttpServletRequest request, HttpServletResponse response,
                        HttpSession session, ModelAndView modelAndView) {
        Map<String,Object> map=getParams(request);
        String url=map.get("url").toString();
        String user=map.get("user").toString();
        String pw=map.get("pw").toString();
        int port=Integer.parseInt(map.get("port").toString());
        logger.info("打印请求参数，url="+url);
        logger.info("打印请求参数，user="+user);
        logger.info("打印请求参数，pw="+pw);
        logger.info("打印请求参数，port="+port);
        boolean flag=ldap.ldapConnection(url,port,user,pw);
        if(flag){
            this.writeJson(response, "连接成功");
        }
        this.writeJson(response, "连接error");
    }




    /**
     * <pre>
     * <b>刷新配置</b>
     * </pre>
     * <p>
     * void
     *
     * @since 1.0
     */
    @RequestMapping(value = "/irobot/reloadMessage")
    public void roadConfig() {
        this.defaultMessageInterfaceResourceProxy.reloadManagerMessage();
        ;
    }

    @RequestMapping(value = "/irobot/testMessage")
    public void testMessage(String key, String platform, HttpServletResponse response) {
//		String key="";
//		String platform="";
        String value = defaultMessageInterfaceResourceProxy.getString(key, platform);
        this.writeJson(response, key + "的值为=====" + value);
    }


    @RequestMapping(value = "/irobot/sms")
    public void sms(HttpServletRequest request, HttpServletResponse response) {
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("type", "1");
            jsonObj.put("userid", "93546");// 号码
            jsonObj.put("appid", "ac63196");
            jsonObj.put("secret", "30BBDFA16");
            jsonObj.put("destnumbers", "18825049225");// 电话号码
            jsonObj.put("content", "尊敬的用户，您正在使用手机验证码登录，验证码:191476，5分钟内输入有效，切勿泄露。您是第1351位获得账单优惠分期的用户，"
                    + "优惠码：KA3143，如非您本人操作，请联系24小时客服热线1010-1010。【小i智能银行】");
//		jsonObj.put("content", Configs.sms);
            jsonObj.put("sendtime", "");
            System.out.println(" 调用短信接口：" + jsonObj.toString());
//		System.out.println(" 调用短信接口返回结果："
//				+ ZxHttpUtil.httpPostWithJson(jsonObj,
//						"http://api.vatti.com.cn/sms/api/sendMsg/simple"));
            this.writeJson(response, "success");
        } catch (Exception e) {
            this.writeJson(response, "error");
        }
    }

    /**
     * <pre>
     * <b>上传图片</b>
     * </pre>
     * <p>
     * void
     *
     * @since 1.0
     */
    @RequestMapping(value = "/irobot/upImage")
    public void uploadimageFaceBook(HttpServletRequest request, HttpServletResponse response) {
        try {
            String url = request.getParameter("url");
            if (url != null & !"".equals(url)) {
                this.writeJson(response, FaceBookUtils.controlImage(url));
            } else {
                this.writeJson(response, "url异常");
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            this.writeJson(response, e);

        }
//        this.defaultMessageResourceProxy.reloadMessageResource();
    }




    public AskServiceProxy getAskServiceProxy() {
        return askServiceProxy;
    }

    public void setAskServiceProxy(AskServiceProxy askServiceProxy) {
        this.askServiceProxy = askServiceProxy;
    }

    public DefaultMessageInterfaceResourceProxy getDefaultMessageInterfaceResourceProxy() {
        return defaultMessageInterfaceResourceProxy;
    }

    public void setDefaultMessageInterfaceResourceProxy(
            DefaultMessageInterfaceResourceProxy defaultMessageInterfaceResourceProxy) {
        this.defaultMessageInterfaceResourceProxy = defaultMessageInterfaceResourceProxy;
    }

    public static void main(String[] args) {
//		Map<String, String> httpMap=new HashMap<String, String>();
//		httpMap.put("q", "香港創新科技與管理考察團--深圳大學mba海外與本土學習行動");
//		String val=HttpUtils.getPostContent("http://172.16.5.23:9993/semantic.action", httpMap,"utf-8");
//		System.out.println("val="+val.substring(val.indexOf("<Semantic>")+10,val.indexOf("</Semantic>")));
        String temp = "microsoftvba1231";
        System.out.println(temp.replaceAll("[\\[.\\]]", ""));
        if (temp.matches("^[a-zA-Z0-9]*")) {
            System.out.println(true);
        }

    }
}
