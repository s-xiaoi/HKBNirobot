/**
 * 文件名：BaseController.java
 * 
 * 版本信息： 日期：2016-9-20 create by ziQi 2016-9-20
 */
package com.xiaoi.south.app.servlet.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xiaoi.south.app.cache.CacheSupport;
import com.xiaoi.south.app.constant.GlobalConstants;
import com.xiaoi.south.app.entity.Menu;
import com.xiaoi.south.app.entity.MenuMap;
import com.xiaoi.south.app.entity.RobotRequest;
import com.xiaoi.south.app.service.impl.DefaultMessageInterfaceResourceProxy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

/**
 * @title BaseController
 * @description 基础Controller
 * @author ziQi
 * @version 下午8:21:30
 * @create_date 2016-9-20下午8:21:30
 * @copyright (c) jacky
 */
public abstract class BaseController{

    /**
     * 日志对象
     */
    protected Logger logger = LoggerFactory.getLogger( getClass() );
    
    protected String writeJsonp( HttpServletResponse response, Object obj, String callback ){
        if( obj != null ){
            String result = callback + "(" + JSON.toJSONString( obj )+ ")";
            return this.writeString( response, result, "text/json" );
        }
        return null;
    }

    /**
     * <pre>
     * <b>直接输出json(使用fastjons输出json字符串)</b>
     * </pre>
     * @param response
     * @param obj void 
     * @since 1.0    
    */
    protected String writeJson( HttpServletResponse response, Object obj ){
        if( obj != null ){
            String result = JSON.toJSONString( obj );
            return this.writeString( response, result, "text/json" );
        }
        return null;
    }

    /**
     * @Description <b>(直接输出字符串)</b></br>
     * @param response
     * @param str void
     * @return
     * @since 2016-9-20
     */
    protected String writeString( HttpServletResponse response, String str ){
        return this.writeString( response, str, "text/html" );
    }
    
    /**
     * @Description <b>(直接输出xml字符串)</b></br>
     * @param response
     * @return
     * @since 2016-9-20
     */
    protected String writeXml( HttpServletResponse response, String xml ){
        return this.writeString( response, xml, "text/xml" );
    }

    /**
     * @Description <b>(直接输出字符串)</b></br>
     * @param response
     * @param str void
     * @since 2016-9-20
     */
    protected String writeString( HttpServletResponse response, String str, String type ){
        try{
            response.reset();
            response.setContentType( type );
            response.setCharacterEncoding( "utf-8" );
            // 解决跨域问题
            response.setHeader( "Access-Control-Allow-Origin", "*" );
            response.getWriter().print( str );
            return null;
        }catch( IOException e ){
            return null;
        }
    }
    
    @SuppressWarnings("unchecked")
	protected Map<String, Object> getParams(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator it = request.getParameterMap().entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String key = String.valueOf(entry.getKey());
			Object[] values = (Object[]) entry.getValue();
			if (values.length <= 1) {
				map.put(key, values[0]);
			} else {
				map.put(key, values);
			}
		}
		return map;
	}

    /**
     * <b> #序号自动增长转换 </b>
     *
     * @param robotRequest
     * @return robotRequest
     * @since 1.0
     */
    public RobotRequest menu2Faq(RobotRequest robotRequest,DefaultMessageInterfaceResourceProxy defaultMessageInterfaceResourceProxy) {

        if(robotRequest.getQuestion().length()>100){//长度大于500说明是知识点调查
            String[] Customs=robotRequest.getQuestion().split(" ");
            robotRequest.setCustom3(Customs.length>=1?Customs[1]:null);
        }
        CacheSupport cacheSupport = new CacheSupport(GlobalConstants.APP_CACHE);

        String question = robotRequest.getQuestion();
        String userId = robotRequest.getUserId();
        // 序号自增长，取缓存
        String realQuestion = null;
        MenuMap menuMap = (MenuMap) cacheSupport.get(userId);
        if (menuMap != null && menuMap.getLastKey() > 0) {
            int key = -1;
            if (StringUtils.isNumeric(question)) {
                key = Integer.valueOf(question);
            }
            // 判断是否获取到菜单
            Menu menu = menuMap.get(key);
            if (menu != null) {
                realQuestion = menu.getKey();
                // 设置实际问题
                robotRequest.setQuestion(realQuestion);
                logger.info("Menu number {} for {}", question, realQuestion);
            }
        } else {
            menuMap = new MenuMap();
            Integer startKey = defaultMessageInterfaceResourceProxy
                    .getInteger("x.automatic.increase.menu.startKey", robotRequest.getPlatform());
            menuMap.setLastKey(startKey);
            cacheSupport.put(userId, menuMap);
        }
        return robotRequest;
    }
}
