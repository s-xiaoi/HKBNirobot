package com.xiaoi.south.app.servlet.controller;

import com.alibaba.fastjson.JSONObject;
import com.xiaoi.south.app.cache.CacheSupport;
import com.xiaoi.south.app.constant.GlobalConstants;
import com.xiaoi.south.app.entity.*;
import com.xiaoi.south.app.modules.utils.HttpUtils;
import com.xiaoi.south.app.modules.utils.LanguageUtils;
import com.xiaoi.south.app.modules.utils.RedisUtils;
import com.xiaoi.south.app.service.WSServer;
import com.xiaoi.south.app.service.impl.AskServiceProxy;
import com.xiaoi.south.app.service.impl.DefaultMessageInterfaceResourceProxy;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.io.IOException;
import java.util.*;

/**
 * @author brave.chen
 * @create 2019-11-04 10:49
 */
@Controller
public class LiveChatController extends BaseController {

    CacheSupport cacheSupport = new CacheSupport(GlobalConstants.APP_CACHE);

//    ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-redis.xml");
//    private RedisUtils redisUtil=(RedisUtils) context.getBean("redisUtil");

    @Autowired
    private RedisUtils redisUtil;
    @Resource(name="redisTemplate")
    private RedisTemplate redisTemplate;
    private Session socketSession;
    private WSServer ws = new WSServer();
    @Autowired
    private DefaultMessageInterfaceResourceProxy defaultMessageInterfaceResourceProxy;
    @Autowired
    private AskServiceProxy askServiceProxy;


    /**
     * @param robotRequest
     * @param response
     * @param session
     * @param modelAndView void
     * @Description <b> #(二次开发问答接口入口)</b>
     * @since 1.0
     */
    @RequestMapping(value = "/chat/ask4Json", produces = {"text/html;charset=UTF-8",
            "application/json;charset=UTF-8", "application/xml;charset=UTF-8"})
    public void ask4Json(@RequestBody RobotRequest robotRequest, HttpServletRequest request,
                         HttpServletResponse response, HttpSession session, ModelAndView modelAndView) {
        RobotResponse robotResponse = new RobotResponse();
        try {
            //判断消息类型，是否为转人工消息
            if (robotRequest.GetMan().equals("2")) {


                live800(robotRequest,request,response);

            } else {

                // 序号自动增长
                this.menu2Faq(robotRequest, defaultMessageInterfaceResourceProxy);
                robotResponse = this.askServiceProxy.ask4Robot(robotRequest);


                // <!- start ->
                /*
                 * 统一约定注册到HttpServletRequest中，供handler使用；
                 */
                request.setAttribute("robotRequest", robotRequest);
                request.setAttribute("robotResponse", robotResponse);

//                redisUtil.lSet(robotResponse.get)
                // <!- end ->
                // write json
                // this.writeJson(response, robotResponse);
            }
        }catch (Exception e){
            e.printStackTrace();

        }
        //记录日志


    }


    /**
     *
     * @param AitiRequest 请求类
     * @param request
     * @param response
     * 客服发消息给用户请求
     *
     */
    @RequestMapping(value = "/chat/Artificial")
    public void WebSocketSendArtificial(ArtificialRequest AitiRequest, HttpServletRequest request, HttpServletResponse response
                              ) {
        JSONObject jsonObj = new JSONObject();
        JSONObject jsonMessage = new JSONObject();
        try {
            logger.info("人工发送消息给用户，用户id==="+AitiRequest.getvisitorId()+",toString==="+AitiRequest.toString());
            WSServer ws = WSServer.getWebSocketMap().get(AitiRequest.getvisitorId());
            if(ws==null){
                jsonObj.put("code", "-1");
                jsonObj.put("msg","未获取到socket连接");
                logger.info("未获取到socket连接");
                this.writeJson(response, jsonObj);
                return;
            }

            try{

                if(StringUtils.equals(AitiRequest.getChatStatus(),"end")&&StringUtils.equals(AitiRequest.getCloseType(),"customer_close")){
                    jsonMessage.put("msg","关闭人工会话");
                    jsonMessage.put("code","-1");
                    logger.info("关闭人工会话="+AitiRequest.toString());
                    ws.sendMessage(jsonMessage);
//                    ws.onClose();
                }else if(AitiRequest.getMsg()==null&&"".equals(AitiRequest.getMsg())){
                    jsonObj.put("code", "-2");
                    jsonObj.put("msg","内容为null或者为空");
                    logger.info("内容为null或者为空="+AitiRequest.toString());
                    this.writeJson(response, jsonObj);
                    return;
                }else{
                    jsonMessage.put("msg",AitiRequest.getMsg());
                    ws.sendMessage(jsonMessage);
                }

            }catch (Exception e){
                e.printStackTrace();;
                jsonObj.put("code", "-2");
                jsonObj.put("msg","推送用户异常");
                logger.info("推送用户异常="+AitiRequest.toString());
                this.writeJson(response, jsonObj);
                return;
            }

            jsonObj.put("msg", AitiRequest.toString());
            jsonObj.put("code", "0");
            this.writeJson(response, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            jsonObj.put("code", "-1");
            jsonObj.put("msg","");
            this.writeJson(response, jsonObj);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * 通知方法
     *
     */
    @RequestMapping(value = "/chat/Notice")
    public void WebSocketnotice(NoticeRequest noticeRequest, HttpServletRequest request, HttpServletResponse response,
                                HttpSession session) {
        Map<String, Object> mapobj = getParams(request);
        socketSession = (Session) redisUtil.get("socketSession-" + mapobj.get("userId").toString());
        try {
            if (socketSession != null) {
                socketSession.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("socketClose=====" + socketSession.getId() + "");
    }


    @RequestMapping(value = "/chat/check")
    public void check(HttpServletRequest request, HttpServletResponse response,
                          HttpSession session, ModelAndView modelAndView) {
        Map<String, Object> maps = getParams(request);
        try {
            redisUtil.set("test001", "test1");
            logger.info("set success=" + redisUtil.get("test001"));
            this.writeJson(response, "success");
        } catch (Exception e) {
            e.printStackTrace();
            this.writeJson(response, "error");
        }
    }

    @RequestMapping(value = "/live/getToken")
    public void getLiveChatToken(HttpServletRequest request, HttpServletResponse response,
                                 HttpSession session) {
        Map<String, Object> maps = getParams(request);
        String userId = maps.get("userId").toString();
        if (StringUtils.isBlank(userId)) {
            logger.info("userID不能为空");
            this.writeJson(response, "userID不能为空");
        }

        String tokenId = "";
        if (redisUtil.get("liveChatTokenId" + userId) != null) {
            tokenId = redisUtil.get("liveChatTokenId" + userId).toString();
        }


        //判断是否获取到token，没有则重新获取
        if (tokenId != null && !"".equals(tokenId)) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("app_id", "0d962cbcc4694ce4905f481ca266e825");
            map.put("app_secret", "f2ee5a976b7c4ddca01b7b2269f4c9ec");
            String result = HttpUtils.doGet("http://show.any800.com/any800/resteasy/NewUcc2OtherSV/getToken", map, "utf-8");
            int i = result.indexOf("accesss_token");
            if (i != -1) {
                result = result.substring(i + 16, result.length());
                result = result.substring(0, result.indexOf("\""));
                tokenId = "liveChatTokenId" + userId;
                redisUtil.set(tokenId, result);

            }
        }
        String UserInfo = "";
        String context_id = "";
        //判断是否有用户信息，没有则重新获取
        if (redisUtil.get("liveChatUserInfo" + userId) != null && !"".equals(redisUtil.get("liveChatUserInfo") + userId)) {
            JSONObject json = new JSONObject();
            json.put("access_token", tokenId);
            JSONObject visitor_info = new JSONObject();
            visitor_info.put("visitor_id", "test001");//用户ID
            visitor_info.put("visitor_name", "");     //用户名
            visitor_info.put("mobile_phone", "电话");
            visitor_info.put("email", "xxx@163.com");
            json.put("visitor_info", visitor_info);
            json.put("visitor_origin", "PC");
            UserInfo = HttpUtils.doPost("http://show.any800.com/any800/resteasy/NewUcc2OtherSV/initDialogue", json.toJSONString());
            int i = UserInfo.indexOf("context_id");
            if (i != -1) {
                UserInfo = UserInfo.substring(i + 13, UserInfo.length());
                UserInfo = UserInfo.substring(0, UserInfo.indexOf("\""));
                context_id = "liveChatContextId" + userId;
                redisUtil.set(context_id, UserInfo);
            }
        }

    }

    @RequestMapping(value = "/chat/pingSocket")
    public void pingSocket(HttpServletRequest request, HttpServletResponse response,
                           HttpSession session, ModelAndView modelAndView) {

        try {
            /*socketSession = (Session) cacheSupport.get("socketSession-"+AitiRequest.getUserId());
            logger.info(socketSession.getId());
            ws.sendMessage(socketSession,AitiRequest.getMsg());
            this.writeJson(response,true);*/
            cacheSupport.put("","");


            this.writeJson(response, "");
        } catch (Exception e) {
            e.printStackTrace();
            this.writeJson(response, "error");
        }
    }

    public RobotResponse ErrorCode(String code, RobotResponse robotResponse, RobotRequest robotRequest){
        if(StringUtils.isBlank("sys_err_01")){
            logger.info(robotRequest.getUserId()+"=appid and appsecrete is invalid");
            robotResponse.setContent(robotRequest.getUserId()+"=appid and appsecrete is invalid");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_err_02")){
            logger.info(robotRequest.getUserId()+"=Accesstoken 失效，重新调用getToken接口再次生成token发送消息");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_err_03")){
            logger.info(robotRequest.getUserId()+"=access_token is empty");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_err_04")){
            logger.info(robotRequest.getUserId()+"=请求次数过多");
            robotResponse.setContent("请求次数过多");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_err_05")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_err_06")){
            logger.info(robotRequest.getUserId()+"=Context_id 不存在，无效，请调用initdialoge生成对应的context_id");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_01")){
            logger.info(robotRequest.getUserId()+"=对话接入失败，请稍后再试");
            robotResponse.setContent("对话接入失败，请稍后再试");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_02")){
            logger.info(robotRequest.getUserId()+"=参数有误,公司不存在");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_03")){
            logger.info(robotRequest.getUserId()+"=无法获取到相关的业务类型");
            robotResponse.setContent("无法获取到相关的业务类型");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_05")){
            logger.info(robotRequest.getUserId()+"=尊敬的客户，当前无坐席在线，给您带来不便，尽请谅解");
            robotResponse.setContent("尊敬的客户，当前无坐席在线，给您带来不便，尽请谅解");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_06")){
            logger.info(robotRequest.getUserId()+"=您已被封锁，无法接入对话");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_06")){
            logger.info(robotRequest.getUserId()+"=业务类型不存在");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_07")){
            logger.info(robotRequest.getUserId()+"=回调地址的参数有误");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_08")){
            logger.info(robotRequest.getUserId()+"=对话接入中，请稍后");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_09")){
            logger.info(robotRequest.getUserId()+"=温馨提示：尊敬的客户，我们的服务时间是【08:00:00-20:00:00】敬请谅解");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_10")){
            logger.info(robotRequest.getUserId()+"=对话关闭失败");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_11")){
            logger.info(robotRequest.getUserId()+"=暂时无法回答您的问题，若想进一步咨询，请输入“转人工”，接人工客服");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_12")){
            logger.info(robotRequest.getUserId()+"=访客已接入对话，无法再次接入对话");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_13")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_14")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_15")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_16")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_17")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_18")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_19")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_20")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_21")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }else if(StringUtils.isBlank("sys_22")){
            logger.info(robotRequest.getUserId()+"=存在请求必填参数为空");
            return robotResponse;
        }
        return null;
    }

    public void live800(RobotRequest robotRequest, HttpServletRequest request, HttpServletResponse response){
        //判断是否在客服工作时间
             /*   String manKey=robotRequest.getHkbnPlatform()+"-"+robotRequest.getLang();
                Any800Data dateObj= (Any800Data)redisUtil.get("manKey");//从缓存获取工作时间
                dateObj.setStartTime(0);
                dateObj.setStartTime(24);
                SimpleDateFormat sdf=new SimpleDateFormat("HH");
                int hours=new Date().getHours();*/
//                if(hours>dateObj.getStartTime()&&hours<dateObj.getEndTime()){//属于工作时间
        RobotResponse robotResponse = new RobotResponse();
        if(true){//属于工作时间
            String userId = robotRequest.getUserId();
            if (StringUtils.isBlank(userId)) {
                logger.info("userID不能为空");
                this.writeJson(response, "userID不能为空");
                return;
            }

            String tokenId = "";
            /*if (cacheSupport.get("liveChatTokenId-" + userId) != null) {
                tokenId = cacheSupport.get("liveChatTokenId-" + userId).toString();
            }*/
            if (redisUtil.get("liveChatTokenId-" + userId) != null) {
                tokenId = redisUtil.get("liveChatTokenId-" + userId).toString();
            }


            //判断是否获取到token，没有则重新获取
            if (tokenId == null || "".equals(tokenId)) {
                Map<String, String> map = new HashMap<String, String>();
                map.put("app_id", "0d962cbcc4694ce4905f481ca266e825");
                map.put("app_secret", "f2ee5a976b7c4ddca01b7b2269f4c9ec");
                String result = HttpUtils.doGet("http://show.any800.com/any800/resteasy/NewUcc2OtherSV/getToken", map, "utf-8");
                int i = result.indexOf("accesss_token");
                if (i != -1) {
                    result = result.substring(i + 16, result.length());
                    result = result.substring(0, result.indexOf("\""));
                    tokenId = result;
                    redisUtil.set("liveChatTokenId-" + userId, result,300);

                } else {
                    return;
                }
            }
            String UserInfo;
            String context_id = "";
//            byte[] UserInfo1;
            //判断是否有用户信息，没有则重新获取
            if (redisUtil.get("liveChatContextId-" + userId) == null || "".equals(redisUtil.get("liveChatContextId-") + userId)) {
                String visitor = "{\"mobile_phone\":\"\",\"visitor_id\":\"df5f7643-3571-dc26-d6d1-c81e2b19b32d\",\"visitor_name\":\"df5f7643-3571-dc26-d6d1-c81e2b19b32d\",\"email\":\"\"}";
                Map<String, String> paramsMap = new HashMap<String, String>();
                paramsMap.put("access_token", tokenId);
                JSONObject visitor_info = new JSONObject();
                visitor_info.put("visitor_id", userId);//用户ID
                visitor_info.put("visitor_name", userId);     //用户名
                visitor_info.put("mobile_phone", "");
                visitor_info.put("email", "");

                paramsMap.put("visitor_info", visitor_info.toString());
                paramsMap.put("visitor_origin", "PC");
                paramsMap.put("sessionId", robotRequest.getSessionId());
                UserInfo = HttpUtils.doPost("http://show.any800.com/any800/resteasy/NewUcc2OtherSV/initDialogue", paramsMap, "utf-8");
//                String UserInfo=new String(UserInfo1);
                int i = UserInfo.indexOf("context_id");
                if (i != -1) {
                    UserInfo = UserInfo.substring(i + 13, UserInfo.length());
                    UserInfo = UserInfo.substring(0, UserInfo.indexOf("\""));
                    context_id = UserInfo;
                    redisUtil.set("liveChatContextId-" + userId, UserInfo,300);
                } else {
                    return;
                }
            } else {
                context_id = redisUtil.get("liveChatContextId-" + userId).toString();
            }
            //发送用户问题给人工客户
            Map<String, String> paramsMap = new HashMap<String, String>();
            paramsMap.put("access_token", tokenId);
            paramsMap.put("context_id", context_id);
            JSONObject text = new JSONObject();
            text.put("msg_type", robotRequest.getMessageType());
            JSONObject content = new JSONObject();
            if("text".equals( robotRequest.getMessageType())) {
                content.put("content", robotRequest.getQuestion());
                content.put("lang_type", robotRequest.getHkbnPlatform());
                String business_type=null;
                String lang=QustionLanguagethree(robotRequest);
                if(robotRequest.getLang().equals("tc")){
                    business_type=robotRequest.getHkbnPlatform()+"_"+lang;
                }else if(robotRequest.getLang().equals("en")){
                    business_type=robotRequest.getHkbnPlatform()+"_"+lang;
                }else{
                    business_type=robotRequest.getHkbnPlatform()+"_"+lang;
                }
                content.put("business_type", business_type);
                text.put("text", content);
            }else{
                content.put("url", robotRequest.getQuestion());
                text.put(robotRequest.getMessageType(), content);
            }
            paramsMap.put("content", text.toString());
            System.out.println("sendResultrequest=userId=" + userId + ",token=" + tokenId + ",contentId=" + context_id + ",session=" + robotRequest.getSessionId()+",paramsMap="+paramsMap.toString());
            String sendResult = HttpUtils.doPost("http://show.any800.com/any800/resteasy/NewUcc2OtherSV/sendMessage", paramsMap, "utf-8");
            if(sendResult==null){
                robotResponse.setContent("请求sendMessage异常");
                robotResponse.setType(-1);
                return;
            }
            System.out.println("sendResultResponse="+sendResult.toString());
            JSONObject Result = JSONObject.parseObject(sendResult);
            if (Result!=null&&Result.get("error_code") != null) {
                JSONObject Data = JSONObject.parseObject(Result.get("data").toString());
                if ("ok".equals(Result.get("error_code"))) {
                    if (Data == null || "".equals(Data.get("content"))) {
                        if (robotRequest.getMessageType().equals("image") || robotRequest.getMessageType().equals("file")) {
                            robotResponse.setContent("上传文件成功");
                        } else {
                            robotResponse.setContent("发送成功");
                        }
                        robotResponse.setType(313);
                    } else {
                        robotResponse.setContent(Data.get("content").toString());
                        robotResponse.setType(303);
                    }
                } else if (Result.get("error_msg") != null && !"".equals(Result.get("error_msg"))) {
//                        robotResponse=ErrorCode(Result.get("error_msg").toString(),robotResponse,robotRequest);
//                            acsStatus
                    robotResponse.setAcsStatus(Result.get("error_code").toString());
                    robotResponse.setContent(Result.get("error_msg").toString());
                }

            } else {
                robotResponse.setContent("转人工失败");
                robotResponse.setAcsStatus("sys_10");
                robotResponse.setType(-1);
            }

            redisUtil.lSet(robotRequest.getUserId() + "-" + robotRequest.getSessionId(), robotRequest.getUserId() + "-" + robotRequest.getSessionId());
            List<String> list = new ArrayList<String>();
            list = (List<String>) (List) redisUtil.lGet("", 0, -1);
            List<Object> listObj = redisUtil.lGet("", 0, -1);
            request.setAttribute("robotRequest", robotRequest);
            request.setAttribute("robotResponse", robotResponse);
        }else{
            robotResponse.setContent("不在客服工作时间");
            request.setAttribute("robotRequest", robotRequest);
            request.setAttribute("robotResponse", robotResponse);
        }
    }

    /**
     * 前三句语言识别
     * default CN
     * @return
     */
    public String QustionLanguagethree(RobotRequest robotRequest){
        //从缓存中获取交互记录
        Object object=redisUtil.get(robotRequest.getUserId()+"-asklog");
        List<AskLogBean> lists=new ArrayList<AskLogBean>();
        if(object!=null){
            lists=(List<AskLogBean>)object;
        }
        boolean hasEnglish=false;
        boolean hasChinese=false;
        int i=0;
        if(lists.size()>=4) {
            i=4;
        }else{
            i=lists.size();
        }
       /* if(i==1){
            return robotRequest.getAgentLang();
        }*/
        for(int j=1;j<i;j++) {
            AskLogBean bean=lists.get(j);
            if (LanguageUtils.hasEnglish(bean.getQuestion())) {//是否包含英文
                hasEnglish = true;
            }
            if (LanguageUtils.hasChinese(bean.getQuestion())) {//是否包含中文
                hasChinese = true;
            }
        }
        if(hasEnglish&&hasChinese){
            return  "CN_EN";
        }else if(hasChinese){
            return "CN";
        }else{
            return  "EN";
        }


    }
}
