package com.xiaoi.south.app.servlet.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiaoi.south.app.constant.GlobalConstants;
import com.xiaoi.south.app.entity.RobotQuestion;
import com.xiaoi.south.app.modules.utils.StringTookits;
import com.xiaoi.south.app.service.impl.DefaultQuestionServiceProxy;
import com.xiaoi.south.app.service.impl.DefaultQuestionTCServiceProxy;

/**
 * @title QuestionController
 * @description 热点问题-相关问题-智能提示接口
 * @author ziQi
 * @version 下午12:56:35
 * @create_date 2016-3-18下午12:56:35
 * @copyright (c) jacky
 */
@Controller
public class QuestionController extends BaseController{

    private static final Logger logger = LoggerFactory.getLogger( QuestionController.class );

//    private DefaultQuestionServiceProxy questionServiceProxy = new DefaultQuestionServiceProxy();
    private DefaultQuestionTCServiceProxy tcProxy = new DefaultQuestionTCServiceProxy();

//    public DefaultQuestionServiceProxy getProxy(){
//        questionServiceProxy.init();
//        return questionServiceProxy;
//    }
    public DefaultQuestionServiceProxy getProxy(String lang) {
    	/*if (GlobalConstants.LANG[0].equals(lang)) {
    		tcProxy.init();
    		return tcProxy;
        } else if (GlobalConstants.LANG[1].equals(lang)) {
        	scProxy.init();
        	return scProxy;
        } else if (GlobalConstants.LANG[2].equals(lang)) {
        	enProxy.init();
        	return enProxy;
        } else {
    		tcProxy.init();
    		return tcProxy;
        }*/

        tcProxy.init();
        return tcProxy;
    }

    @RequestMapping( value = "/irobot/getHotQuestions" )
    @ResponseBody
    public List<String> getHotQuestions(String lang, int mode, String platform, String location, int top ){
        return this.getProxy(lang).getHotQuestions( mode, platform, location, top );
    }

    @RequestMapping( value = "/irobot/getHotQuestions0" )
    @ResponseBody
    public List<String> getHotQuestions0(String lang, int mode, String platform, String location, String brand, int top ){
        return this.getProxy(lang).getHotQuestions0( mode, platform, location, brand, top );
    }

    @RequestMapping( value = "/irobot/getRelatedQuestions" )
    @ResponseBody
    public List<String> getRelatedQuestions(String lang, String question, String platform, String location, int top ){
        return this.getProxy(lang).getRelatedQuestions( question, platform, location, top );
    }

    @RequestMapping( value = "/irobot/getRelatedQuestions0" )
    @ResponseBody
    public List<String> getRelatedQuestions0(String lang, String question, String platform, String location, String brand, int top ){
        return this.getProxy(lang).getRelatedQuestions0( question, platform, location, brand, top );
    }

    @RequestMapping( value = "/irobot/getSuggestedQuestions" )
    @ResponseBody
    public List<String> getSuggestedQuestions( @RequestParam( "input" )
    String input, String lang, String platform, String location, int top ){
        return this.getProxy(lang).getSuggestedQuestions( input, platform, location, top );
    }

    @RequestMapping( value = "/irobot/getSuggestedQuestions0" )
    @ResponseBody
    public List<String> getSuggestedQuestions0(String lang, String input, String platform, String location, String brand, int top ){
        return this.getProxy(lang).getSuggestedQuestions0( input, platform, location, brand, top );
    }

    @RequestMapping( value = "/irobot/getQuestions", method=RequestMethod.POST, produces = { "text/html;charset=UTF-8", "application/json;charset=UTF-8", "application/xml;charset=UTF-8" } )
    public void getQuestions(@RequestParam(required=false)String callback, HttpServletResponse response,@RequestBody RobotQuestion robotQuestion ){
        logger.info( "qtype={},question={},input={},mode={},platform={},location={},brand={},top={}", robotQuestion.getQtype(), robotQuestion.getQuestion(), robotQuestion.getInput(), robotQuestion.getMode(), robotQuestion.getPlatform(), robotQuestion.getLocation(), robotQuestion.getBrand(), robotQuestion.getTop() );
        List<String> list = new ArrayList<String>();
        // 产品定制热点问题模式
        int imode = StringTookits.parseInt( robotQuestion.getMode(), GlobalConstants.HOT_QUESTIONS_MODE[0] );
        // 返回的问题数量，默认10笔
        int itop = StringTookits.parseInt( robotQuestion.getTop(), 10 );
        if( StringUtils.equalsIgnoreCase( GlobalConstants.HOT_QUESTIONS, robotQuestion.getQtype() ) ){// 热点问题
            list = this.getProxy(robotQuestion.getLang()).getHotQuestions0( imode, robotQuestion.getPlatform(), robotQuestion.getLocation(), robotQuestion.getBrand(), itop );
        }else if( StringUtils.equalsIgnoreCase( GlobalConstants.RELATED_QUESTIONS, robotQuestion.getQtype() ) ){// 相关问题
            list = this.getProxy(robotQuestion.getLang()).getRelatedQuestions0( robotQuestion.getQuestion(), robotQuestion.getPlatform(), robotQuestion.getLocation(), robotQuestion.getBrand(), itop );
        }else if( StringUtils.equalsIgnoreCase( GlobalConstants.SUGGESTED_QUESTIONS, robotQuestion.getQtype() ) && StringUtils.isNoneBlank(robotQuestion.getInput()) ){// 智能提示
            list = this.getProxy(robotQuestion.getLang()).getSuggestedQuestions0( robotQuestion.getInput(), robotQuestion.getPlatform(), robotQuestion.getLocation(), robotQuestion.getBrand(), itop );
        }
        
        this.writeJson( response, list );
    }

    @RequestMapping( value = "/irobot/getQuestions4Json", method=RequestMethod.POST, produces = { "text/html;charset=UTF-8", "application/json;charset=UTF-8", "application/xml;charset=UTF-8" } )
    public void getQuestions4Json(@RequestParam(required=false)String callback, HttpServletResponse response,@RequestBody RobotQuestion robotQuestion ){
//        logger.info( "qtype={},question={},input={},mode={},platform={},location={},brand={},top={}", robotQuestion.getQtype(), robotQuestion.getQuestion(), robotQuestion.getInput(), robotQuestion.getMode(), robotQuestion.getPlatform(), robotQuestion.getLocation(), robotQuestion.getBrand(), robotQuestion.getTop() );
        List<String> list = new ArrayList<String>();
        // 产品定制热点问题模式
        int imode = StringTookits.parseInt( robotQuestion.getMode(), GlobalConstants.HOT_QUESTIONS_MODE[0] );
        // 返回的问题数量，默认10笔
        int itop = StringTookits.parseInt( robotQuestion.getTop(), 10 );
        if( StringUtils.equalsIgnoreCase( GlobalConstants.HOT_QUESTIONS, robotQuestion.getQtype() ) ){// 热点问题
            list = this.getProxy(robotQuestion.getLang()).getHotQuestions0( imode, robotQuestion.getPlatform(), robotQuestion.getLocation(), robotQuestion.getBrand(), itop );
        }else if( StringUtils.equalsIgnoreCase( GlobalConstants.RELATED_QUESTIONS, robotQuestion.getQtype() ) ){// 相关问题
            list = this.getProxy(robotQuestion.getLang()).getRelatedQuestions0( robotQuestion.getQuestion(), robotQuestion.getPlatform(), robotQuestion.getLocation(), robotQuestion.getBrand(), itop );
        }else if( StringUtils.equalsIgnoreCase( GlobalConstants.SUGGESTED_QUESTIONS, robotQuestion.getQtype() ) && StringUtils.isNoneBlank(robotQuestion.getInput()) ){// 智能提示
            list = this.getProxy(robotQuestion.getLang()).getSuggestedQuestions0( robotQuestion.getInput(), robotQuestion.getPlatform(), robotQuestion.getLocation(), robotQuestion.getBrand(), itop );
        }
        
        if (callback != null && callback.length() > 0) {
       	 	this.writeJsonp( response, list, callback );
        } else {
           // write json
           this.writeJson( response, list );
        }
    }

}
