/**    
 * 文件名：AccessRequired.java    
 *
 * 版本信息：    
 * 日期：2016年1月30日    
 * create by ziQi       
 * 2016年1月30日
 */
package com.xiaoi.south.app.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;

/**
 * @title AccessRequired
 * @description 定义的Access注解
 * @author ziQi
 * @version 上午11:42:28
 * @create_date 2016年1月30日上午11:42:28
 * @copyright (c) jacky
 */
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
public @interface AccessRequired{
    
}
