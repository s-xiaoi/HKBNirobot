/**    
 * 文件名：PaginatorRequired.java    
 * 版本信息：    
 * 日期：2016-9-21
 * create by ziQi       
 */
package com.xiaoi.south.app.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;

/** 
 * @title PaginatorRequired
 * @description 分页注解标识 
 * @author ziQi 
 * @version 下午10:19:44 
 * @create_date 2016-9-21下午10:19:44
 * @copyright (c) jacky
 */  
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
public @interface PaginatorRequired{
    
}
