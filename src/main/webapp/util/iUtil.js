var socketPINGPONG;
var iUtil = {
	// str是否以start字符串为开头
	startWith : function(str, start) {
		if (str == null || str == "" || str.length == 0
				|| str.length < start.length) {
			return false;
		}
		if (str.substr(0, start.length) == start) {
			return true;
		}
		return false;
	},
	initSocket:function(){//初始化WebSocket对象
        // var socket;
        var userId=$ibot.userId;
        if(typeof(WebSocket) == "undefined") {
            // alert("您的浏览器不支持WebSocket");
            $ibot.showMsg("您的浏览器不支持WebSocket连接，无法转接人工<br/>请更换版本更高的浏览器","left")
            return;
        }

            //实现化WebSocket对象，指定要连接的服务器地址与端口
		var location=window.document.location.origin.substring(window.document.location.origin.indexOf('://')+3,window.document.location.origin.length);;
        if(window.document.location.protocol.indexOf('s')>0){
            socket = new WebSocket("wss://"+location+"/HKBN/ws/"+userId);
		}else{
            socket = new WebSocket("ws://"+location+"/HKBN/ws/"+userId);
		}
            //打开事件
            socket.onopen = function() {
             //   alert("Socket 已打开");
                console.log('Socket 已打开'+iUtil.intDate()+' '+iUtil.intTime());
                $ibot.man="2";
                $ibot.postMsg('转人工');//主动发送转人工
                iUtil.socketPINGPONG();
                //socket.send("这是来自客户端的消息" + location.href + new Date());
            };
            //获得消息事件
            socket.onmessage = function(msg) {

                // alert("msgData:"+msg.data);
                console.log("msgData:"+msg.data+","+iUtil.intDate()+' '+iUtil.intTime());
				while (true){
					if(postStat==2){
                        var msg=JSON.parse(msg.data);
						if(msg.code!=null&&msg.code=='-1'){//人工结束
                            socket.onclose();
                            $ibot.man=1;
                            $ibot.sessionId=iUtil.createSessionId();
						}else if(msg.code!=null&&msg.code=='1024'){
                            console.log("PING====:"+msg.msg);
                        }else{
                            $ibot.showMsg(msg.msg,'left');
						}

                        break;

					}

				}

            };
            //关闭事件
            socket.onclose = function() {
                console.log("Socket已关闭"+iUtil.intDate()+' '+iUtil.intTime());
                $ibot.man=1;
                $ibot.sessionId=iUtil.createSessionId();//重新生成sessionId
                socket.send("closeSocket");
                clearInterval(socketPINGPONG);
                // alert("Socket已关闭");
            };

            //发生了错误事件
            socket.onerror = function() {
                console.log("soscket发生了错误"+iUtil.intDate()+' '+iUtil.intTime()+">>>>>开始重连");
                socket.onopen();

                // $ibot.showMsg("soscket连接发生了错误","left")
                // alert("发生了错误");
            }


        $("#btnSend").click(function() {
            socket.send("这是来自客户端的消息" + location.href + new Date());
        });

        $("#btnClose").click(function() {
            socket.close();
        });
	},
	intDate:function () {
        var dateStr = new Date().Format('yyyy:MM:dd');
        return dateStr;
    },

	intTime:function () {
        var dateStr = new Date().Format('HH:mm:ss');
        return dateStr;
    },

	// 把字符串str中的src字符串全部替换为reg
	replaceAll : function(str, src, reg) {
		return str.split(src).join(reg);
	},

	// 判断字符串是否为空
	isNotEmpty : function(str) {
		if (str == null || str == "undefined" || str.length < 1) {
			return false;
		}
		return true;
	},
	isEmpty : function(str) {
		if (str == null || str == "undefined" || str.length < 1) {
			return true;
		}
		return false;
	},

	// 返回的是对象形式的参数
	getUrlArgs : function() {
		var args = new Object();
		var query = location.search.substring(1);//获取查询串
		var pairs = query.split(",");//在逗号处断开
		for (var i = 0; i < pairs.length; i++) {
			var pos = pairs[i].indexOf('=');//查找name=value
			if (pos == -1) {//如果没有找到就跳过
				continue;
			}
			var argname = pairs[i].substring(0, pos);//提取name
			var value = pairs[i].substring(pos + 1);//提取value
			args[argname] = unescape(value);//存为属性
		}
		return args;//返回对象
	},
	// 返回url地址?后的参数对象
	getUrlParams : function() {
		var params = new Object();
		var query = location.search.substring(1);//获取查询串
		query = decodeURIComponent(query);	//对参数解码
		var pairs = query.split("&");
		for (var i = 0; i < pairs.length; i++) {
			var pos = pairs[i].indexOf('=');//查找name=value
			if (pos == -1) {//如果没有找到就跳过
				continue;
			}
			var argname = pairs[i].substring(0, pos);//提取name
			var value = pairs[i].substring(pos + 1);//提取value
			params[argname] = value;//存为属性
		}
		return params;//返回对象
	},
	// 返回url地址?后指定参数的值
	getUrlParam : function(variable) {
		var query = window.location.search.substring(1);
		query = decodeURIComponent(query);	//对参数解码
		var vars = query.split("&");
		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split("=");
			if (pair[0] == variable) {
				return pair[1];
			}
		}
		return null;
	},

	// 生成guid作为唯一用户
	guidSession : function() {
		return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	},

	// 生成sessionId
	createSessionId : function() {
		var nowT = new Date().getTime() + Math.random();
		return md5(nowT);
	},

	// 判断微信、企业微信浏览器
	isWX : function() {
		var flag = true;
		var ua = navigator.userAgent.toLowerCase();
	    if(ua.match(/MicroMessenger/i)=="micromessenger") {
	    	if (ua.match(/wxwork/i)=="wxwork") { //企业微信
	    		flag = false;
	    	} else { //微信
	    		flag = true;
	    	}
	    } else { //其他
	    	flag = true;
	    }
	    return flag;
	},

	// 判断ios
	isIOS : function() {
		var u = navigator.userAgent;
	    var ios = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
	    var android = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
	    return ios;
	},

	parseImgmsg2Msg:function(data,obj){//图片
		var content='';
		var image=obj.args[2];
		content+='<div class="swiper-slide"><a href="'+image+'"  target="_blank"><img src="'+image+'" alt=""/></a></div>';
		return content;
	},
	parseImgtxt2Msg:function(data,obj){//图文
		var content='';
		var url=obj.args[3].substring(obj.args[3].indexOf("<Url><![CDATA[")+14,obj.args[3].indexOf("]]></Url>"));
		var image=obj.args[3].substring(obj.args[3].indexOf("<PicUrl><![CDATA[")+17,obj.args[3].indexOf("]]></PicUrl>"));
		var title=obj.args[3].substring(obj.args[3].indexOf("<Title><![CDATA[")+16,obj.args[3].indexOf("]]></Title>"));
		var Description=obj.args[3].substring(obj.args[3].indexOf("<Description><![CDATA[")+22,obj.args[3].indexOf("]]></Description>"));
		if(title.indexOf('mediaTag')!=-1){
			title=title.replace('mediaTag','');
			url=Description;
		}
		content+='<div class="swiper-slide"><a href="'+url+'"  target="_blank"><img src="'+image+'" alt="""/></a>';
		content+='<p>'+title+'</p></div>';
		return content;
	},
	ControllerFileUpdate:function (flag) {
		if(flag) {
            $(".ToggleBtn").css("display", "");
        }else{
            $(".ToggleBtn").css("display","none");
		}
    },
	TaskCheck:function () {//定时检查及请求后台做检查（socket是否异常断开）
    },
    socketPINGPONG:function () {
        socketPINGPONG=setInterval(function () {
            console.log("PING");
            socket.send("PING");
        },5000);
    },
	clearTerval:function (terval) {//清楚定时器

        clearInterval(terval);
    },
	getHKBNProfile:function () {//请求HKBN profile API
        $.ajax({
            type : "get",
            async: true,
            dataType : "json",
            contentType: 'application/json',
            url : "https://future-uat.hkbn.com.hk/chat/api/profile",
            success: function(data){
                //    "Platform": "MyAcct",    "Section": "app",    "LoginStatus": true,
                //  "Location": "/app",    "PPS": "712759380",    "Language": "Chi",
                // "CustIP": "",    "aio_session": ""}
                if(data){
                    if(data.Platform=='MyAcct'){
                        $ibot.hkbnPlatform="CS";
                    }else if(data.Section=="renew"){
                        $ibot.hkbnPlatform="ORET";
                    }else if(data.Platform=="Spec"&&data.LoginStatus==false){
                        $ibot.hkbnPlatform="OACQ";
                    }
                    if(data.PPS){
                    	$ibot.userId=data.PPS;
					}
					if(data.Language){
                        $ibot.cfg.lang=langHKBN[data.Language];
					}
                }
                if(data.xxx&&socket){
                    iUtil.initSocket();
                }
            },error:function(e){

            }
        });
    }

}