/**
 * xUtils 工具集引用说明
 * 组装自定义工具集,提升开发效率
 */
var jsIgnoreVersionPackages = [
    				"../util/layui/layui.js"								//	layui模块化js, 需在页面引用 layui.css
					,"../util/md5.js"										//	md5加密工具
					,"../util/Browser.js"									//	浏览器检测工具
					,"../util/js.cookie.js"								//	cookie使用工具
					,"../util/i18n/jquery.i18n.properties.min.js"			//	javascript提供国际化
					,"../util/autocomplete/jquery.autocomplete.min.js" 	//	智能提示组件, 需在页面引用 automcomplete.css
];
for (var i = 0; i < jsIgnoreVersionPackages.length; i++) {
    document.write('<script type="text/javascript" src="' + jsIgnoreVersionPackages[i] + '"></script>');
}

var jsVersionPackages = [
    				"../util/layui/layui_use.js"
    				,"../util/layui/lay/modules/upload.js"
					,"../util/iUtil.js"									//	自定义常用工具, 依赖：md5.js
					,"../util/robot.js"//	定制化的：robot.js 和 app.js
					,"../web/js/app.js"
                  ];
var _VERSION =  new Date().getTime();
//document.write('<script type="text/javascript" src="irobot/version"></script>');
for (var i = 0; i < jsVersionPackages.length; i++) {
    document.write('<script type="text/javascript" src="' + jsVersionPackages[i] + "?v=" + _VERSION +  '"></script>');
}


