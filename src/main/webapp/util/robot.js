var imgtxtmsg='';
var type='';
var dataType='';
var contentSegmentation='contentSegmentation';
var socket;
var manType='text';
var postStat=2;
String.prototype.disPx = function(){
	if(this.indexOf('px', 0) > 0) {
		return parseInt(this.replace(/px/g, ''));
	}
	return parseInt(this);
};

Date.prototype.Format = function(fmt)   
{ //author: meizz   
  var o = {   
    "M+" : this.getMonth()+1,                 //月份   
    "d+" : this.getDate(),                    //日   
    "H+" : this.getHours(),                   //小时   
    "m+" : this.getMinutes(),                 //分   
    "s+" : this.getSeconds(),                 //秒   
    "q+" : Math.floor((this.getMonth()+3)/3), //季度   
    "S"  : this.getMilliseconds()             //毫秒   
  };   
  if(/(y+)/.test(fmt))   
    fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
  for(var k in o)   
    if(new RegExp("("+ k +")").test(fmt))   
  fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
  return fmt;   
};
function UUID() {
	function S4() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    }
  /*  var userId=localStorage.getItem("ibot-userId");
	if(userId!=null&&userId!=''){
		return userId;
	}else{
        return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
	}*/
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());

}

var datetime = '';
var lastDatetime = '';
var last_time;
var start = false;
var i;

var $ibot = {
	userId : UUID(),
	welcomeMsg : 'system_welcome',
    sessionId:iUtil.createSessionId(),
	urlPattern1:[/\[link\s+url=[\'\"]+([^\[\]\'\"]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,'<a href="javascript:aClick(\'$1\');" >$2</a>'],
	urlPattern2:[/\[link\s+url=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,'<a href="javascript:aClick(\'$1\');">$2</a>'],
//	submitPattern1:[/\[link\s+submit=\s*[\'\"]+([^\[\]\'\"]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,'<a id="submitLink" href="#" rel="$1" onclick="getRel(this.rel,this);return false;" >$2</a>'],
// 	submitPattern1:[/\[link\s+submit=[\'\"]+([^\[\]]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,'<div class="hot-list" id="submitLink" onclick="getRel(\'$1\',this);return false;">$2</div>'],
// 	submitPattern2:[/\[link\s+submit=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,'<div class="hot-list" id="submitLink" onclick="getRel(\'$1\',this);return false;">$2</div>'],
	submitPattern3:[/\[link\s+submit=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,'<a id="submitLink" href="#" rel="$1" onclick="getRel(this.rel,this);return false;" >$2</a>'],
	submitPattern4:[/\[link\s+submit=[\'\"]+([^\[\]]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,'<a id="submitLink" href="#" rel="$1" onclick="getRel(this.rel,this);return false;" >$2</a>'],
	faqVotePattern:[/\[link\s+submit=[\'\"]+([^\[\]]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,''],
    defalutPattern:[/\[link\](.*?)\[\/link\]/gi,'<a id="submitLink" href="#"  rel="$1" onclick="getRel(this.rel,this);return false;" >$1</a>'],
	inputting : '正在输入<img src = "../web/images/1.gif"/>',
    messageType:'text',//发送消息类型 text：文本  image:图片  file:文件
    man:'1',//2：人工消息， 1：机器人消息
    hkbnPlatform:'CS',//从官网获取的语言，默认繁体
	// 外部调用run方法即可启动机器人
	run : function(sendBtn,inputBox,outputDiv,_cfg){
		this.els.sendBtn = $(sendBtn);
		this.els.inputBox = $(inputBox);
		this.els.outputDiv = $(outputDiv);
		this.init(_cfg);
	},
	els : {
		'sendBtn' : null,
		'outputDiv' : null,
		'inputBox' : null
	},
	cfg : {
	},
	init : function(_cfg){
        // localStorage.setItem("ibot-userId", $ibot.userId);
		for(var item in _cfg) {
			this.cfg[item] = _cfg[item];
		}
		var _this = this;
		this.els.sendBtn.click(function(){
			_this.sendMsg($.trim($ibot.els.inputBox.val()));
			//新增点击完发送让输入框获取焦点
			_this.els.inputBox.focus();
		});
		//this.threadDate();
		//展示欢迎语
		//$ibot.showMsg($ibot.inputting, 'left');
		//$ibot.showMsgEx($ibot.welcomeMsg, 'left');
		if (_this.cfg.welcomeMsgPower) {
			$ibot.sendMsgEx($ibot.welcomeMsg,$ibot.welcomeMsg,false);
		}
		//初始化智能提示
		if (_this.cfg.autoCompletePower) {
			initAutoComplete();
		}
		$ibot.els.inputBox.val($.trim($ibot.els.inputBox.val()));
		//初始化webSocket
		// iUtil.initSocket();
	},
	threadDate : function(){
		// 每隔一分钟更新一下日期,如果下一次显示问日期时间有变化则显示
		datetime = new Date().Format('HH:mm');
		window.setInterval(function(){
			datetime = new Date().Format('HH:mm');
		}, 1000*60);
	},
	getObjById : function(eleId) {
		var divObj = $('#'+eleId);
		if(divObj && divObj.attr('id')) {
			return divObj;
		}
		return false;
	},
	showMsg : function(msg, pos){
        $ibot.messageType="text";
        console.log(iUtil.intDate()+' '+iUtil.intTime()+"===showMsg");
		if(msg==null||msg.indexOf('<div class="swiper-container chat-txt">')!=-1){
			return ;
		}
		msg=$.trim(msg);
        //截取FAQ解决未解决
        var faqVote;
        if(msg.indexOf('[faq]')!=-1){
            faqVote=msg.substring(msg.indexOf('[faq]')+5,msg.indexOf('[/faq]'));
            msg=msg.replace(msg.substr(msg.indexOf('[faq]'),msg.indexOf('[/faq]')+6),'');
        }


		//自定义标签转换
		msg=replaceLink(msg);
		var div = '';
		if(dataType==11&&msg.indexOf('suggestedTag')!=-1){
			imgtxtmsg=msg.replace('suggestedTag','</div><div class="swiper-button-prev swiper-button"></div><div class="swiper-button-next swiper-button"></div></div> ');
			type='imgtxtmsg';
		}
		
		if(!pos && pos !='left' && pos != 'right'){
			alert(msg);
			return;
		}
		if(pos == 'left') {
			if(msg==$ibot.inputting){
                console.log(iUtil.intDate()+' '+iUtil.intTime()+"===showMsg.inputting");
				div = $('<div id="inputtingDiv" class="chat-txt clearfix"><div class="chat-area robot"><div class="robot_icon"></div>  <div class="dialog"><div class="cont"><div class="cont-situation"> </div> </div></div></div>  </div> </div>');
			}else{
                console.log(iUtil.intDate()+' '+iUtil.intTime()+"===showMsg.remove");
                $('#inputtingDiv').remove();
				div = $('<div class="chat-txt clearfix"><div class="chat-area robot"> <p class="robot-time"><span class="int-date">'+iUtil.intDate()+'</span><span class="int-time"> '+iUtil.intTime()+'</span></p><div class="dialog"><div class="text-message"><div class="cont-situation"> </div></div><!--<div class="comment"><a href="#" class="yes"></a><a href="#" class="no cur"></a></div>--></div></div>  </div>');
			}
			
			last_time = new Date().getTime();
		} else if(pos == 'right'){
			if(datetime != lastDatetime) {
				var dateStr = new Date().Format('HH:mm');
				lastDatetime = dateStr;
				var dateDiv = $('<div class="dateDiv radius-box"></div>');
				this.els.outputDiv.append(dateDiv);
				dateDiv.html(dateStr);
			}
			div = $('<div class="chat-txt clearfix"><div class="chat-area user"><p class="user-time"><span class="int-date">'+iUtil.intDate()+'</span><span class="int-time"> '+iUtil.intTime()+'</span></p><div class="dialog"> <div class="text-message"></div></div></div></div>');
			msg=msg.replace("<br/>","");
			// 增加xss过滤
			msg=xssFilter(msg);
		}
		if((type=='imgtxtmsg'||type=='imgmsg')&&imgtxtmsg!=''&&msg.indexOf("web/images/1.gif")==-1){
			div=$(imgtxtmsg);
			//div.html();
		}else{
			if(pos=='right'){
				div.find('.text-message').html(msg);
			}else{

				div.find('.cont-situation').html(msg);
			}
			// 转换解决未解决样式
		//	div = replaceFaqvote(div);
//			div.attr('title', new Date().Format('yyyy-MM-dd HH:mm:ss'));
			
		}
        console.log(iUtil.intDate()+' '+iUtil.intTime()+"===showMsg.append");
		this.els.outputDiv.append(div);

		if(faqVote!=null&&faqVote!=''){
            faqVote=faqVote.replace(/\[link\s+submit=[\'\"]+([^\[\]]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/i,'<a href="#" onclick="getRel(\'$1\',this);return false;"  id="submitLink" class="yes"></a>');
            faqVote=faqVote.replace(/\[link\s+submit=[\'\"]+([^\[\]]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/i,'<a href="#" onclick="getRel(\'$1\',this);return false;"  id="submitLink" class="no"></a>');
            faqVote='<div class="comment">'+faqVote+'</div>';
            div.find('.dialog').append($(faqVote));
            faqVote='';//清空faqVote值
        }
		if((type=='imgtxtmsg'||type=='imgmsg')&&imgtxtmsg!=''&&msg.indexOf("web/images/1.gif")==-1){
			var centeredSlide=true;
			if(imgtxtmsg.indexOf('navfixed')!=-1){
				centeredSlide=false;
			}
			var swiper = new Swiper('.swiper-container', {
			      slidesPerView: 'auto',
			      freeMode:true,
			      centeredSlides:centeredSlide,
			      spaceBetween: 10,
				 // 如果需要前进后退按钮
				    navigation: {
				      nextEl: '.swiper-button-next',
				      prevEl: '.swiper-button-prev',
				    },     
			    });
			imgtxtmsg='';//清空
			type='';
		}else if(msg.indexOf('onclick="previewImg(this)"')!=-1){
			imgtxtmsg='';//清空
			type='';
		}else{
			//加上淡入淡出的效果
//			div.hide();
//			div.fadeIn('slow');	
			
		} 
		
		var htmlDiv = this.els.outputDiv.get(0);
		htmlDiv.scrollTop = htmlDiv.scrollHeight;
        console.log(iUtil.intDate()+' '+iUtil.intTime()+"===showMsg.scrollTop");
		// 显示超出显示框长度的答案头部&收起超出答案框的内容
		if(pos == 'left') {
			var child_idx = this.els.outputDiv.children().length;
			var childEle = this.els.outputDiv.children().get(child_idx-1);
			var childEle2 = this.els.outputDiv.children().get(child_idx-2);
			var tmpHeight = childEle.offsetHeight - parseInt(htmlDiv.offsetHeight);
            var tmpHeight = childEle.offsetHeight - 160 - 16;//parseInt(htmlDiv.offsetHeight)
        /*    if( tmpHeight>0 ){
                $(".showall").css("display","");

                var collapseHeight = htmlDiv.offsetHeight-childEle2.offsetHeight-65;
                $(childEle).find('.cont-situation').height('160');
                $(childEle).find('.cont-situation').next().attr({'cHeight':collapseHeight});
                $(childEle).find('.cont-situation').next().show();
            }else{
                $(childEle).find('.showallParent').remove();
            }*/
            htmlDiv.scrollTop = childEle2.offsetTop;
			
		}
	},
	showMsgEx : function(msg, pos, type){
		
		if(pos=='left' && msg != $ibot.inputting){
            setTimeout(function(){
                $ibot.showMsg(msg,pos,0);
            },1500);

		}else{
			$ibot.showMsg(msg,pos);
		}
	},
	fireEx:function(data){
		var content = data.content;
		
		var imgmsgCount=0;
		var tag='';
		var contentImg='';
			if(data.commands && data.commands.length>0){
			imgtxtmsg='<div class="swiper-container chat-txt swiper02"> <div class="swiper-wrapper">';
			for ( var int = 0; int <  data.commands.length; int++) {
				var obj = data.commands[int];
				if (obj.name=='videomsg') {
				} else if (obj.name=='imgtxtmsg') {
					type='imgtxtmsg';
					imgtxtmsg += iUtil.parseImgtxt2Msg(data,obj);
				} else if (obj.name=='p4') {
					break;
				} else if (obj.name=='imgmsg') {
					imgmsgCount++;
					type='imgmsg';
					imgtxtmsg += iUtil.parseImgmsg2Msg(data,obj);
					contentImg= "<img style=\"width:150px\" src="+obj.args[2]+"   onclick=\"previewImg(this)\">";
				} else if(obj.name=='txtmsg') {
					content=data.content;
				}
			}
			imgtxtmsg+=' </div><div class="swiper-button-prev"></div><div class="swiper-button-next"></div>';
		}
			if(type!=''&&content!=null&&tag!='1'){
				for ( var int = 0; int <  data.commands.length; int++) {
					var obj = data.commands[int];
					if (obj.name=='imgtxtmsg') {
						$ibot.showMsg($ibot.inputting ,'left');
						type='imgtxtmsg';
						imgtxtmsg='<div class="swiper-container chat-txt"><div class="swiper-wrapper">';
						imgtxtmsg += iUtil.parseImgtxt2Msg(data,obj);
						imgtxtmsg+=' </div><div class="swiper-button-prev"></div><div class="swiper-button-next"></div>';
						$ibot.showMsgEx('','left','');
					}else if (obj.name=='imgmsg') {
						$ibot.showMsg($ibot.inputting ,'left');
						type='imgmsg';
						contentImg= "<img src="+obj.args[2]+"   onclick=\"previewImg(this)\">";
						$ibot.showMsgEx(contentImg,'left','');
					}else if(obj.name=='txtmsg') {
						$ibot.showMsg($ibot.inputting ,'left');
						$ibot.showMsgEx(obj.args[0],'left','');
					}
				}
				imgtxtmsg='';type='';
				return false;
			}else if(type=='imgmsg'&&imgmsgCount<=1){//如果只有一个图片就返回可预览图片跟答案，有多个就返回轮滑
				imgtxtmsg='';
				return contentImg;
			}else{
				return content;
			}
		
	},
	
	messagereceived:function(data){

        console.log(iUtil.intDate()+' '+iUtil.intTime()+"===messagereceived");
		//处理type
		var content = $ibot.fireEx(data);

        $ibot.sessionId=data.sessionId;
		if(data.type==101){
			$ibot.messageType=true;
			//初始化webSocket
            iUtil.initSocket();

		}
		if(imgtxtmsg!=''&&!content){
			content='';
		}else if(!content){
			return false;
		}
        $ibot.showMsg($ibot.inputting ,'left');
		if(content && content.indexOf(contentSegmentation) != -1){
			var cs = content.split(contentSegmentation);
			for(var k = 0; k < cs.length; k++){
				$ibot.showMsg($ibot.inputting ,'left');
				
				$ibot.showMsgEx(cs[k] ,'left');
				
			}
		}else{

			$ibot.showMsgEx(content,'left');
		}
	},
	sendMsg : function(str){
		$ibot.sendMsgEx(str,str,true);
	},
	sendMsgEx : function(msg,str,isShow){
        console.log(iUtil.intDate()+' '+iUtil.intTime()+"===sendMsgEx");
        //var msg = '';
		if(!msg && msg == '') {
			msg = this.els.inputBox.val();
		}
		this.els.inputBox.val('');
        console.log(iUtil.intDate()+' '+iUtil.intTime()+"===clear Val");
		msg=$.trim(msg);
		
		if(msg && msg != ''){
			if(isShow) this.showMsg(str, 'right');
			
			this.postMsg(msg);
		}
	},
	getCommand : function(commands,name) {
        console.log(iUtil.intDate()+' '+iUtil.intTime()+"getCommand");
		var command = undefined;
		if(commands && commands.length>0){
			for ( var int = 0; int < commands.length; int++) {
				var obj = commands[int];
				if (obj.name==name) {
					command = obj;
					break;
				}
			}
		}
		return command;
	},
	sendCommand : function(){
		
	},
	clearAll : function(){
		this.els.inputBox.val('');
		this.els.outputDiv.html('');
	},
	postMsg : function(msg){
        console.log(iUtil.intDate()+' '+iUtil.intTime()+"===postMsg");
        postStat=1;
		var userId = this.userId;
		var platform = this.cfg.platform;
		var question = msg;
		var lang = this.cfg.lang;
		var askUrl = this.cfg.robotAskUrl;
		var messageType=manType;
		var sessionId=this.sessionId;
		var _this = this;
		var man=this.man;
		var hkbnPlatform=this.hkbnPlatform;
		var robotRequest = {
				'sessionId' : sessionId,
				'userId' : userId,
				'platform' : platform,
				'lang' : lang,
				'question' : question,
				'messageType':messageType,
				'man':man,
				'hkbnPlatform':hkbnPlatform
			};
		$.ajax({
			type : "POST",
			async: false,
			dataType : "json",
			contentType: 'application/json',
			url : askUrl,
			data : JSON.stringify(robotRequest),
		  success: function(data){
              console.log(iUtil.intDate()+' '+iUtil.intTime()+"===ajax");
			  if(data){
                  manType="text";//默认为text
				  // 问题语言识别功能处理
				  var qlrCommand = $ibot.getCommand(data.commands,'qlrinfo');
				  dataType=data.type;
				  if(dataType==313){
				  	return ;
				  }
				  if(data.acsStatus!=null&&data.acsStatus!=""){
				  	if(data.acsStatus=='sys_10'||data.acsStatus=='sys_05'||data.acsStatus=='sys_06'){
				  		socket.onclose();
					}
				  }
				  if (qlrCommand) {
					  parseQlrinfoMsg(data,qlrCommand);
				  } else {
					  $ibot.messagereceived(data);
				  }

                  postStat=2;
			  }else{
				  _this.showMsg('与后端通讯失败，请重新试一下', 'left');
                  postStat=2;
			  }
          },
          error: function(data){
               _this.showMsg('与后端通讯失败，请重试', 'left');
              postStat=2;
          }
		});
	},transitionPost:function (msg,linkNode) {
        $(linkNode).parent().css("pointer-events","none");
		$ibot.postMsg(msg);
    }
};

//增加可以接收用户传入userId值
function getUrlParam(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
	var r = window.location.search.substr(1).match(reg); //匹配目标参数
	if (r != null) return unescape(r[2]);
		return null; //返回参数值
}
//增加把link submit标签的submit后面的内容放到a标签的rel字段中，用于提交
function getRel(msg,linkNode){
    console.log(iUtil.intDate()+' '+iUtil.intTime()+"===getRel");
	if (msg.indexOf('faqvote:')==0) {
		$(linkNode).removeAttr('onclick');
		$(linkNode).css('cursor', 'auto').css('color', '#999999');
		if (linkNode.className==solveValue) {
			$(linkNode).next().removeAttr('onclick');
			// $(linkNode).next().css('cursor', 'auto').css('color', '#999999');
            linkNode.classList.add('cur');
			$ibot.postMsg(msg);
		} else if (linkNode.className==unsolveValue) {
			$(linkNode).prev().removeAttr('onclick');
			// $(linkNode).prev().css('cursor', 'auto').css('color', '#999999');
            linkNode.classList.add('cur');
            var div='<div class="chat-txt clearfix"><div class="chat-area robot"><p class="robot-time">' +
				'<span class="int-date">'+iUtil.intDate()+'</span><span class="int-time">'+iUtil.intTime()+'</span></p>' +
				'<div class="dialog"><div class="text-message"><div class="feedback"><h4>不滿意原因</h4>' +
				'<ul class="PopupWindow"><li class="cur" onclick="$ibot.transitionPost(\''+msg+' 答非所問\',this)">答非所問</li><li onclick="$ibot.transitionPost(\''+msg+' 過於專業\',this)">過於專業</li><li onclick="$ibot.transitionPost(\''+msg+' 內容過簡\',this)">內容過簡</li><li  onclick="faqNo(\''+msg+'\',this)">其他</li></ul>' +
				'</div></div></div></div></div>';

            $("#outputDiv").append($(div));
            $("#outputDiv").get(0).scrollTop = $("#outputDiv").get(0).scrollHeight;

		}
	} else {
		$ibot.sendMsgEx(msg,linkNode.innerHTML,true);

		//相关问、菜单只能点击一次，点击后禁用并改变背景色
//		$(linkNode).css('background', '#999');
//		$(linkNode).prevAll().css('background', '#999');
//		$(linkNode).nextAll().css('background', '#999');
//		$(linkNode).removeAttr('onclick');
//		$(linkNode).prevAll().removeAttr('onclick');
//		$(linkNode).nextAll().removeAttr('onclick');
		
	}
}
/**
 * 增加 xss过滤
 * @param msg
 * @returns
 */
function xssFilter(msg){
	msg=msg.replace(/</g, '&lt;').replace(/>/g, '&gt;');
	return msg;
}
function replaceLink(html){
	var urlPattern1=$ibot.urlPattern1;
	var urlPattern2=$ibot.urlPattern2;
	var submitPattern3=$ibot.submitPattern3;
	var submitPattern4=$ibot.submitPattern4;
	var defalutPattern=$ibot.defalutPattern;
	if(html.indexOf('suggestedTag')!=-1){
		html = html.replace(/\[link\s+url=[\'\"]+([^\[\]\'\"]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/i,'<div class="swiper-container chat-txt swiper02"> <div class="swiper-wrapper"><div class="swiper-slide" onclick="getRel(\'$1\',this);return false;">$2<\/div>');
		html = html.replace(/\[link\s+url=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/i,'<div class="swiper-container "> <div class="swiper-wrapper"><div class="swiper-slide" onclick="getRel(\'$1\',this);return false;">$2<\/div>');
		html = html.replace(/\[link\s+submit=[\'\"]+([^\[\]]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/i,'<div class="navfixed swiper-container chat-txt swiper02 "> <div class="swiper-wrapper"><div class="swiper-slide" onclick="getRel(\'$1\',this);return false;">$2<\/div>');
		html = html.replace(/\[link\s+url=[\'\"]+([^\[\]\'\"]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,'<div class="swiper-slide navfixed" onclick="getRel(\'$1\',this);return false;">$2<\/div>');
		html = html.replace(/\[link\s+url=([^\s\[\]\'\"]+)\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,'<div class="swiper-slide" onclick="getRel(\'$1\',this);return false;">$2<\/div>');
		html = html.replace(/\[link\s+submit=[\'\"]+([^\[\]]+)[\'\"]+\s*[^\[\]]*\]([^\[\]]+)\[\/link\]/gi,'<div class="swiper-slide" onclick="getRel(\'$1\',this);return false;">$2<\/div>');
		
		dataType=11;
	}
	html = html.replace(urlPattern1[0],urlPattern1[1]);
	html = html.replace(urlPattern2[0],urlPattern2[1]);
	html = html.replace(submitPattern3[0],submitPattern3[1]);
	html = html.replace(submitPattern4[0],submitPattern4[1]);
	html = html.replace(defalutPattern[0],defalutPattern[1]);
	html = html.replace(/((\+852\s)\d{4}\s?\d{4})/gi,'<a href="tel:$1">$1</a>');
	
	html = html.replace(/[\n]/gi, "<br/>");
	html = html.replace(/<\/div><br\/>/gi, "</div>");
	
	return html;
}
function replaceFaqvote(html) {
	if (solveValue && unsolveValue) {
		var arr = html.find('#submitLink');
		if (arr && arr.length>0) {
			arr.each(function() {
				var val = $.trim($(this).html());
				var val2 = solveValue;
				var val3 = unsolveValue;
				if (val2 == val) {
					$(this).addClass('yes');
				} else if (val3 == val) {
					$(this).addClass('no');
				}
			});
		}
	}
	return html;
}

/**
 * 智能提示等初始化信息事件
 * 1.智能提示
 * 2.取消回车事件并发送消息
 * @returns
 */
function initAutoComplete() {
    var robotQuestion = {"qtype":"suggestedQuestions","lang":"","platform":"web","top":"10"};
    $ibot.els.inputBox.devbridgeAutocomplete({
        type: 'POST',
        ajaxSettings:{'contentType': 'application/json'},
        serviceUrl: $ibot.cfg.robotAutoCompleteUrl,
        paramName: 'input',
        dataType: 'json',
        params: robotQuestion,
        orientation: 'auto',
        onSelect: function(suggestion) {
            $ibot.sendMsg(suggestion.value);
//			if( $ibot.els.inputBox ){
//				$ibot.els.inputBox.blur();
//        	}
        },
        transformResult: function(response, originalQuery) {
            var s = new Object();
            s.query = originalQuery;
            var arrData = [];
            var datas = [];
            if ('text' == this.dataType && iUtil.isNotEmpty(response)) {
                datas = JSON.parse(response);
            } else {
                datas = response;
            }
            for( var i=0; i< datas.length ;i++ ) {
                var d = new Object();
                d.value = datas[i];
                d.data = datas[i];
                arrData[i] = d;
            }
            s.suggestions = arrData
            return s;
        }
    });

	/**
	 * 取消回车事件并发送消息
	 * @param e
	 * @returns
	 */
	$ibot.els.inputBox.bind('keydown', function(e) {
		if(e.keyCode == 13){
			e.preventDefault();
			$ibot.els.inputBox.val($.trim($ibot.els.inputBox.val()));
			$ibot.sendMsg($ibot.els.inputBox.val());
	        if( $ibot.els.inputBox ){
	        	$ibot.els.inputBox.focus();
        	}
	    }
	});
	
}

/**
 * 展开和收起长答案
 * @param ecObj
 * @returns
 */
function expanAndCollapse(ecObj) {
	var cHeight = $(ecObj).parent().attr('cHeight');
	if ($(ecObj).text() == answerExpand) {
		$(ecObj).text(answerCollapse);
		$(ecObj).parent().siblings(".cont-situation").css("height","auto");
	} else {
		$(ecObj).text(answerExpand);
		$(ecObj).parent().siblings(".cont-situation").css("height",cHeight);
	}
}
/**
 * 图片预览
 * @param obj
 * @returns
 */
function previewImg(obj) {
    var img = new Image();  
    img.src = obj.src;
    var imgHtml = "<img src='" + obj.src + "' width='500px' height='500px'/>";  
    //弹出层
    layer.open({  
        type: 1,  
        shade: 0.8,
        offset: 'auto',
        //area: [500 + 'px',550+'px'],  '100%', '100%'
        area: ['100%', '100%'],
        shadeClose:true,
        scrollbar: false,
        title: "图片预览", //不显示标题  
        content: imgHtml, //捕获的元素，注意：最好该指定的元素要存放在body最外层，否则可能被其它的相对元素所影响  
        cancel: function () {  
            //layer.msg('捕获就是从页面已经存在的元素上，包裹layer的结构', { time: 5000, icon: 6 });  
        }  
    }); 
}

/**
 * 未解决其他原因
 */
function faqNo(msg,linkNode){
    $(linkNode).parent().css("pointer-events","none");
    layer.prompt({
            btn: [qlrInfoConfirm, qlrInfoCancel]
            ,closeBtn: 0
            ,formType: 2
            ,title: unsolveContentValue
            ,btn2: function(index, layero){
                $ibot.postMsg(msg);
            }
        },function(value, index, elem){
            if ($.trim(value)=='') {
                layer.open({
                    title: qlrInfoTip
                    ,content: unsolveContentValue
                });
            } else {
                $ibot.postMsg(msg+" "+value);
                layer.close(index);
            }
        }
    );
}