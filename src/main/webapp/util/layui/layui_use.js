/**
 * layui 
 * 模块化使用方式：使用到什么组件，则需要use加载组件; layui.js
 * 非模块化使用方式：则全部组件都已加载，直接使用即可; layui.all.js
 */

//模块化方式, 页面引用layui.js
layui.use(['layer'], function() {
	var layer = layui.layer;
});


