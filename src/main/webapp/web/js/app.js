
var currentLang = 'zh_HK';
var langObj = {'繁':'zh_HK','簡':'zh_CN','EN':'en_HK'};
var apiLangObj = {'zh_HK':'tc','zh_CN':'sc','en_HK':'en'};
var langApiObj = {'tc':'zh_HK','sc':'zh_CN','en':'en_HK'};
var langHKBN={'Chi':'tc','Eng':'en'};
var platform = 'web';
var sendBtn = '#sendBtn';
var inputBox = '#inputBox';
var outputDiv = '#outputDiv';
var appUrl = '..';
var inputMaxLength = 100;
var forJson = '4Json';
/**
 * 获取url参数
 * @param variable
 * @returns
 */
function getVariable(variable)
{
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }
    return(false);
}

$(function() {
    // 初始化前端页面信息
    initWebParams(true);

    // 当前语言：PreferredLocale=zh_CN、zh_HK、en_HK
//	var clientLang = Cookies.get('HSBC_CLIENT_COOKIE'); //根据汇丰语言设置：PreferredLocale=zh_CN\zh_HK\en_HK
    var clientLang = Cookies.get('HSBC_CLIENT_LANG'); //自定义语言设置：PreferredLocale=zh_CN\zh_HK\en_HK
    if (clientLang) {
        var arr = clientLang.split("=");
        currentLang = arr[1];
    }

    // 语言配置文件
    changeLanguageInfo();

    // 判断PC端和移动端
    var info = new Browser();
//	if ('PC' == info.device) {
    sendBtn = '#sendBtn';
    inputBox = '#inputBox';
//	} else {
//		platform = 'app';
//		sendBtn = '#sendBtn2';
//		inputBox = '#inputBox2';
//	}

    // app端语言选择
    /*$('.drop-down li').click(function() {
        var key = $(this).html();
        $(this).text($('.tc-mobile').text());
        $('.tc-mobile').text(key);
        $('.tc-mobile').removeClass('current');
        $('.drop-down').removeClass('current');

        currentLang = langObj[key];
        $ibot.cfg.lang = apiLangObj[currentLang];
        initAutoComplete();
        initWebParams(false);
        changeLanguageInfo();
    });
    $('.typeface span').click(function(){
        var key = $(this).html();

        currentLang = langObj[key];
        $ibot.cfg.lang = apiLangObj[currentLang];
        initAutoComplete();
        initWebParams(false);
        changeLanguageInfo();
    });*/

    var tempLang=getVariable("lang");
    if(tempLang==null||tempLang==''||tempLang=='null'||tempLang=='false'){
        tempLang=apiLangObj[currentLang];
    }
    $ibot.cfg.lang=tempLang;
    // 初始化机器人&智能提示
    $ibot.run(sendBtn,inputBox,outputDiv,{
        'autoCompletePower' : true,
        'welcomeMsgPower' : false,
        'lang' : tempLang,
        'platform' : platform,
        'robotAskUrl' : appUrl+'/chat/ask'+forJson+'?&t='+new Date().getTime(),
        'robotAutoCompleteUrl' : appUrl+'/irobot/getQuestions'+forJson+'?t='+new Date().getTime()
    });

    // 展示欢迎语
    $ibot.sendMsgEx($ibot.welcomeMsg,$ibot.welcomeMsg,false);

    // 限制输入长度事件
    $ibot.els.inputBox.bind('keyup', function(e) {
        setInputLengthTip();
    });
    $ibot.els.inputBox.bind('keydown', function(e) {
        setInputLengthTip();
    });
    $ibot.els.inputBox.bind('blur', function(e) {
        setInputLengthTip();
    });
    $ibot.els.sendBtn.bind('click', function(e) {
        setInputLengthTip();
    });


});

function setInputLengthTip() {
    var totalLength = inputMaxLength;
    var inputLength = $ibot.els.inputBox.val().length;
    var valueLength = totalLength - inputLength;
    $('.txt_num').text(valueLength);
}

function changeLanguageInfo() {
    $('.typeface span').each(function(){
        var key = $(this).html();
        if (currentLang == langObj[key]) {
            $(this).addClass('cur').siblings().removeClass('cur');
            $('.tc-mobile').text(key);
            var i = 0;
            for(var l in langObj){
                if (l != key) {
                    if (i == 0) {
                        $('.sc-mobile').text(l);
                    } else {
                        $('.en-mobile').text(l);
                    }
                    i++;
                }
            }
        }
    });

    jQuery.i18n.properties({
        path:'../util/i18n/bundle/',
        name: 'Messages',
        language: currentLang,
        callback: function(){
//  			$('.header-left img').attr('src','images/hongkong-hsbc-logo-'+apiLangObj[currentLang]+'.png');
            //$('.header-mob h1').text(header_title);
            $('.htitle').text(header_title);
            $('#buttonLang').text(buttonNext);
            $('.disclaimertxt').text(disclaimertxt);
            $('.inputArea').attr('placeholder',inputbox_placeholder);
            $('.input-send').attr('placeholder',inputbox_placeholder);
            $('.input-tips').html(inputbox_limit_tips('<span class="txt_num">'+inputMaxLength+'</span>'));
            $('.sendButton').attr('value',inputbtn_value);
            $('.btn-send').text(inputbtn_value);
            $ibot.inputting = ibotInputting_tips + '<img src = "../web/images/1.gif"/>';
            document.title = header_title;
        }
    });
}

/**
 * 初始化前端页面信息
 * @returns
 */
function initWebParams(isWebsiteOpen) {
    //'zh_HK','sc':'zh_CN','en':'en_HK
    if(isWebsiteOpen){
        if(getVariable("lang")!=''){
            currentLang=langApiObj[getVariable("lang")];
            /*if(getVariable("lang")=='en'){
                $('.disclaimertxt').text(header_title);
            }else if(getVariable("lang")=='sc'){

            }else{

            }*/
        }else{
            currentLang=langApiObj['tc']
        }
    }
}

/**
 * 问题语言识别功能消息解析
 * @param data
 * @param qlrinfoObj
 * @returns
 */
function parseQlrinfoMsg(data,qlrinfoObj) {
    debugger;
    var content = data.content;
    var args = qlrinfoObj.args[0];
    var qlr = JSON.parse(args);
    var qlrLang = qlr.qlrLang;
    //data.content = qlrContent;
    //直接切换语言
    currentLang = langApiObj[qlrLang];
    $ibot.cfg.lang = qlrLang;
    initAutoComplete();
    initWebParams(false);
    changeLanguageInfo();
    $ibot.messagereceived(data);
}
/**
 * a标签只打开一个页面
 * @param url
 * @returns
 */
function aClick(url){
    window.open(url,"myFrame");
}
