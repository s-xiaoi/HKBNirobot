
$(function(){

    //返回顶部
    $("#outputDiv").scroll(function(e) {
        var secT=$("#outputDiv").scrollTop();
        if(secT>300){
            $('.backtop').fadeIn();
        }else{
            $('.backtop').fadeOut();
        }
    });
    $('.backtop').click(function(e) {
        $('#outputDiv').animate({scrollTop:0},300);
    });
    //全文展示和收縮
    $(".showall").click(function(){
        var con = $(this).html();
        if(con == "展開"){
            $(this).parent().siblings(".cont-situation").css("height","auto");
            $(this).html("收起");
            $('.showshadow').hide();
            $(this).addClass('showall-active');
        }else{
            $(this).parent().siblings(".cont-situation").css("height","150px");
            $(this).html("展開");
            $('.showshadow').show();
            $(this).removeClass('showall-active');
        }
        
    });
   // 底部菜单显示隐藏
   $('.ToggleBtn').click(function(event) {
      var clssname = $(this).prop("className");
      if(clssname == "ToggleBtn"){
         $('.container_in').css('bottom','168px');
         $('.chat-menu').removeClass('hd')
         $(this).addClass('ToggleBtn-active');
      }else{
         $('.container_in').css('bottom','55px');
         $('.chat-menu').addClass('hd')
         $(this).removeClass('ToggleBtn-active')         
      }
     
   });
   // 不满意评价选择
   $('.PopupWindow li').click(function(event) {
       $(this).toggleClass('cur').siblings().removeClass('cur')
   });
    // 客服展示
    $(".kefu").click(function(){
        $('.wrap').removeClass('hidden')
    $(this).hide();
    });    
    $('.hclose').click(function(event) {
     $(".kefu").show();
     $('.wrap').addClass('hidden')
    });   

});



